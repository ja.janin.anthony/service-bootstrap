package com.ajja.service.bootstrap.generator.builder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;

/**
 * SourceCodeBuilder
 */
public final class SourceCodeBuilder {

    private final Collection<String> imports = new HashSet<>();

    private final Collection<String> methods = new ArrayList<>();

    private final String template;

    /**
     * SourceCodeBuilder
     *
     * @param template String
     */
    public SourceCodeBuilder(final String template) {
        final String[] lines = template.split("\n");

        this.template = Arrays.stream(lines).filter(line -> !(line.startsWith("import ") && line.endsWith(";"))).collect(Collectors.joining("\n")).replaceAll("(\n{2,})|(\r{2,})|((\r\n){2,})", "\n\n") + "\n";
        this.imports.addAll(Arrays.stream(lines).filter(line -> line.startsWith("import ") && line.endsWith(";")).collect(Collectors.toList()));
    }

    /**
     * addImport
     *
     * @param importClass Class<?>
     */
    public void addImport(final Class<?> importClass) {
        if (!importClass.getPackageName().equals("java.lang")) {
            imports.add(String.format("import %s;", importClass.getName().replace("$", ".")));
        }
    }

    /**
     * addMethod
     *
     * @param methodCodeBuilder MethodCodeBuilder
     */
    public void addMethod(final MethodCodeBuilder methodCodeBuilder) {
        methods.add(methodCodeBuilder.build());
    }

    /**
     * build
     *
     * @return String
     */
    public String build() {
        String result = template;

        String previousStrImportFirstPackageName = null;

        final StringBuilder sbImports = new StringBuilder();
        for (final String strImport : imports.stream().sorted().collect(Collectors.toList())) {
            final String strImportFirstPackageName = strImport.substring(0, strImport.indexOf('.'));
            if (previousStrImportFirstPackageName != null && !previousStrImportFirstPackageName.equals(strImportFirstPackageName)) {
                sbImports.append("\n");
            }
            previousStrImportFirstPackageName = strImportFirstPackageName;
            sbImports.append(strImport).append("\n");
        }
        if (sbImports.length() > 0) {
            final int index = result.indexOf(/* package ...; <-- */";") + 2;
            result = result.substring(0, index) + "\n" + sbImports + result.substring(index);
        }

        final StringBuilder sbContent = new StringBuilder();
        for (final String strMethod : methods) {
            sbContent.append(strMethod);
        }
        if (sbContent.length() > 0) {
            result = result.substring(0, result.lastIndexOf('}')) + sbContent + "}\n";
        }

        return result;
    }
}
