package com.ajja.service.bootstrap.generator.utils;

import com.ajja.service.bootstrap.generator.context.GeneratorContext;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;

/**
 * FileUtils
 */
public final class FileUtils {

    /**
     * FileUtils
     */
    private FileUtils() {}

    /**
     * forEachFile
     *
     * @param directoryName String
     * @param consumer Consumer<Class<?>>
     * @param classLoader ClassLoader
     */
    public static void forEachFile(final String directoryName, final Consumer<Class<?>> consumer, final ClassLoader classLoader) {
        final File[] listFiles = new File(directoryName).listFiles();

        if (listFiles != null) {
            for (final File file : listFiles) {
                if (file.isFile()) {
                    final String fileAbsolutePath = file.getAbsolutePath();

                    if (fileAbsolutePath.contains(GeneratorContext.SRC_MAIN_JAVA) && fileAbsolutePath.endsWith(GeneratorContext.JAVA_FILE_EXTENSION)) {
                        final String className = fileAbsolutePath.substring(fileAbsolutePath.indexOf(GeneratorContext.SRC_MAIN_JAVA) + GeneratorContext.SRC_MAIN_JAVA.length() + 1).replace(File.separator, ".").replace(GeneratorContext.JAVA_FILE_EXTENSION, StringUtils.EMPTY);

                        try {
                            consumer.accept(classLoader.loadClass(className));
                        } catch (ClassNotFoundException e) {
                            throw new RuntimeException(e);
                        }
                    }
                } else if (file.isDirectory()) {
                    forEachFile(file.getAbsolutePath(), consumer, classLoader);
                }
            }
        }
    }

    /**
     * getContent
     *
     * @param pathName String
     * @return String
     */
    public static String getContent(final String pathName) {
        try {
            return Files.readString(Paths.get(pathName));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
