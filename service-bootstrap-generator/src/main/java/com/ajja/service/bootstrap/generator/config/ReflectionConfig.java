package com.ajja.service.bootstrap.generator.config;

import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.generator.context.GeneratorContext;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * ReflectionConfig
 */
public final class ReflectionConfig {

    /**
     * ReflectionConfig
     */
    private ReflectionConfig() {}

    /**
     * generate
     *
     * @param aClass Class<?>
     * @param classLoader ClassLoader
     * @param projectPathImpl String
     * @throws ClassNotFoundException exception
     */
    public static void generate(final Class<?> aClass, final ClassLoader classLoader, final String projectPathImpl) throws ClassNotFoundException {
        final File directory = new File(projectPathImpl + File.separator + GeneratorContext.SRC_MAIN_RESOURCES + File.separator);
        directory.mkdirs();

        try {
            final File file = new File(directory, GeneratorContext.REFLECTION_CONFIG_FILE);
            final Path fileName = file.toPath();

            final StringBuilder sb = new StringBuilder();
            if (file.exists()) {
                final String content = Files.readString(fileName)
                    .replace("[\n", "")
                    .replace("\n]\n", "");
                sb.append(content);
            } else {
                addClass(sb, User.class);
            }
            addClass(sb, aClass);

            Files.writeString(fileName, String.format("[\n%s\n]\n", sb.toString()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * addClass
     *
     * @param sb StringBuilder
     * @param aClass Class<?>
     */
    private static void addClass(final StringBuilder sb, final Class<?> aClass) {
        if (sb.length() > 0) {
            sb.append(",\n");
        }

        sb.append("  {\n");
        sb.append("    \"name\" : \"").append(aClass.getCanonicalName()).append("\",\n");
        sb.append("    \"allDeclaredConstructors\" : true,\n");
        sb.append("    \"allPublicConstructors\" : true,\n");
        sb.append("    \"allDeclaredMethods\" : true,\n");
        sb.append("    \"allPublicMethods\" : true,\n");
        sb.append("    \"allDeclaredFields\" : true,\n");
        sb.append("    \"allPublicFields\" : true\n");
        sb.append("  }");
    }
}
