package com.ajja.service.bootstrap.generator;

import com.ajja.service.bootstrap.common.application.annotation.Controller;
import com.ajja.service.bootstrap.common.business.annotation.Entity;
import com.ajja.service.bootstrap.common.business.annotation.Repository;
import com.ajja.service.bootstrap.common.business.annotation.Service;
import com.ajja.service.bootstrap.generator.config.ReflectionConfig;
import com.ajja.service.bootstrap.generator.context.GeneratorContext;
import com.ajja.service.bootstrap.generator.proxy.*;
import com.ajja.service.bootstrap.generator.utils.ClassLoaderUtils;
import com.ajja.service.bootstrap.generator.utils.FileUtils;
import com.ajja.service.bootstrap.generator.utils.FolderUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

import java.io.File;

@Mojo(name = "generator", requiresDependencyResolution = ResolutionScope.COMPILE, defaultPhase = LifecyclePhase.COMPILE)
public class Application extends AbstractMojo {

    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    private MavenProject project;

    @Parameter(defaultValue = "${outputClassJava}", readonly = true, required = true)
    private boolean outputClassJava;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        final ClassLoader classLoader = ClassLoaderUtils.getClassLoader(project, getClass().getClassLoader());

        final String projectName = project.getName();
        getLog().info("-- Project name: " + projectName);

        final String projectImplName = projectName.substring(projectName.indexOf("-impl-") + "-impl-".length());
        getLog().info("-- Project impl name: " + projectImplName);

        final String projectPathImpl = project.getBasedir().getPath();
        getLog().info("-- Project path impl: " + projectPathImpl);

        final String projectPathBusiness = projectPathImpl.substring(0, projectPathImpl.lastIndexOf(File.separator)).replace("-impl", "-business");
        getLog().info("-- Project path business: " + projectPathBusiness);

        final String projectPathApplication = projectPathBusiness.replace("-business", "-application");
        getLog().info("-- Project path application: " + projectPathApplication);

        // Remove reflection config fie if exists (for quarkus)
        final File file = new File(projectPathImpl + File.separator + GeneratorContext.SRC_MAIN_RESOURCES + File.separator + GeneratorContext.REFLECTION_CONFIG_FILE);
        if (file.exists()) {
            file.delete();
        }

        FileUtils.forEachFile(projectPathBusiness, (aClass) -> {
            try {
                if (aClass.getAnnotation(Entity.class) != null) {
                    if ("quarkus".equals(projectImplName)) {
                        ReflectionConfig.generate(aClass, classLoader, projectPathImpl);
                    }
                }
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }, classLoader);

        FileUtils.forEachFile(projectPathBusiness, (aClass) -> {
            try {
                if (aClass.getAnnotation(Repository.class) != null) {
                    if ("quarkus".equals(projectImplName)) {
                        QuarkusRepositoryProxy.generate(aClass, classLoader, projectPathBusiness, projectPathImpl);
                    } else if ("spring-boot".equals(projectImplName)) {
                        SpringBootRepositoryProxy.generate(aClass, classLoader, projectPathBusiness, projectPathImpl);
                    }
                } else if (aClass.getAnnotation(Service.class) != null) {
                    if ("quarkus".equals(projectImplName)) {
                        QuarkusServiceProxy.generate(aClass, classLoader, projectPathBusiness, projectPathImpl);
                    } else if ("spring-boot".equals(projectImplName)) {
                        SpringBootServiceProxy.generate(aClass, classLoader, projectPathBusiness, projectPathImpl);
                    }
                }
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }, classLoader);

        FileUtils.forEachFile(projectPathApplication, (aClass) -> {
            try {
                if (aClass.getAnnotation(Controller.class) != null) {
                    if ("quarkus".equals(projectImplName)) {
                        QuarkusControllerProxy.generate(aClass, classLoader, projectPathApplication, projectPathImpl);
                    } else if ("spring-boot".equals(projectImplName)) {
                        SpringBootControllerProxy.generate(aClass, classLoader, projectPathApplication, projectPathImpl);
                    }
                }
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }, classLoader);

        if (!outputClassJava) {
            FolderUtils.removeProxyFolder(projectPathImpl + File.separator + GeneratorContext.SRC_MAIN_JAVA);
        }
    }
}
