package com.ajja.service.bootstrap.generator.utils;

import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * ClassLoaderUtils
 */
public final class ClassLoaderUtils {

    /**
     * ClassLoaderUtils
     */
    private ClassLoaderUtils() {}

    /**
     * getClassLoader
     *
     * @param project MavenProject
     * @param parent ClassLoader
     * @return ClassLoader
     */
    public static ClassLoader getClassLoader(final MavenProject project, final ClassLoader parent) {
        try {
            return new URLClassLoader(project.getCompileClasspathElements().stream()
                .map(classPath -> {
                    try {
                        return new File(classPath).toURI().toURL();
                    } catch (MalformedURLException e) {
                        return new RuntimeException(e);
                    }
                }).toArray(URL[]::new), parent);
        } catch (DependencyResolutionRequiredException e) {
            throw new RuntimeException();
        }
    }
}
