package com.ajja.service.bootstrap.generator.builder;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * TemplateBuilder
 */
public final class TemplateBuilder {

    private final Map<String, String> placeholders = new HashMap<>();

    private final String fileName;

    /**
     * TemplateBuilder
     */
    public TemplateBuilder(final String fileName) {
        this.fileName = fileName;
    }

    /**
     * setPlaceholder
     *
     * @param key String
     * @param value String
     */
    public void setPlaceholder(final String key, final String value) {
        placeholders.put(String.format("{{%s}}", key), value);
    }

    /**
     * build
     *
     * @return String
     */
    public String build() {
        String template = getTemplate(fileName);
        if (template == null) {
            return StringUtils.EMPTY;
        }

        for (final Map.Entry<String, String> entry : placeholders.entrySet()) {
            template = template.replace(entry.getKey(), entry.getValue());
        }

        return template;
    }

    /**
     * getTemplate
     *
     * @param fileName String
     * @return String
     */
    private String getTemplate(final String fileName) {
        final ClassLoader classLoader = TemplateBuilder.class.getClassLoader();
        final InputStream inputStream = classLoader.getResourceAsStream(fileName);

        try {
            return readFromInputStream(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * readFromInputStream
     *
     * @param inputStream InputStream
     * @return String
     * @throws IOException exception
     */
    private String readFromInputStream(final InputStream inputStream) throws IOException {
        final StringBuffer sb = new StringBuffer();

        try (final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line = bufferedReader.readLine();
            if (line != null) {
                do {
                    sb.append(line).append("\n");
                    line = bufferedReader.readLine();
                } while (line != null);
            }
        }

        return sb.toString();
    }
}
