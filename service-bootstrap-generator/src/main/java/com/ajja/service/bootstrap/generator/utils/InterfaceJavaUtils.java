package com.ajja.service.bootstrap.generator.utils;

import com.ajja.service.bootstrap.generator.context.GeneratorContext;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * InterfaceJavaUtils
 */
public final class InterfaceJavaUtils {

    /**
     * InterfaceJavaUtils
     */
    private InterfaceJavaUtils() {}

    /**
     * getSignatures
     *
     * @param anInterface Class<?>
     * @param projectPath String
     * @return String[]
     */
    public static List<String> getSignatures(final Class<?> anInterface, final String projectPath) {
        final String pathName = projectPath + File.separator + GeneratorContext.SRC_MAIN_JAVA + File.separator + anInterface.getName().replace(".", File.separator) + GeneratorContext.JAVA_FILE_EXTENSION;
        final String content = FileUtils.getContent(pathName);

        return Arrays.stream(content.substring(content.indexOf('{') + 1, content.lastIndexOf('}')).split("\n"))
            .map(line -> line.trim())
            .filter(line -> line.endsWith(");"))
            .map(line -> line.substring(0, line.lastIndexOf(");") + 1))
            .collect(Collectors.toList());
    }
}
