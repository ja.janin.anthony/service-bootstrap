package com.ajja.service.bootstrap.generator.builder;

import java.util.ArrayList;
import java.util.Collection;

/**
 * MethodCodeBuilder
 */
public final class MethodCodeBuilder {

    private final Collection<String> annotations = new ArrayList<>();

    private String visibility;

    private String signature;

    private String body;

    /**
     * addAnnotation
     *
     * @param annotationClass Class<?>
     */
    public void addAnnotation(final Class<?> annotationClass) {
        annotations.add("@" + annotationClass.getSimpleName());
    }

    /**
     * addAnnotation
     *
     * @param strAnnotation String
     */
    public void addAnnotation(final String strAnnotation) {
        annotations.add(strAnnotation);
    }

    /**
     * setVisibility
     *
     * @param visibility String
     */
    public void setVisibility(final String visibility) {
        this.visibility = visibility;
    }

    /**
     * setSignature
     *
     * @param signature String
     */
    public void setSignature(final String signature) {
        this.signature = signature;
    }

    /**
     * setBody
     *
     * @param body String
     */
    public void setBody(final String body) {
        this.body = String.format("\t\t%s\n", body);
    }

    /**
     * build
     *
     * @return String
     */
    public String build() {
        final StringBuilder sb = new StringBuilder();

        sb.append("\n");
        for (final String annotation : annotations) {
            sb.append("\t").append(annotation).append("\n");
        }
        sb.append("\t").append(visibility).append(" ").append(signature).append(" {\n").append(body).append("\t}\n");

        return sb.toString();
    }
}
