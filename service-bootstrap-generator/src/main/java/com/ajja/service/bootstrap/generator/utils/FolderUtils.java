package com.ajja.service.bootstrap.generator.utils;

import com.ajja.service.bootstrap.generator.context.GeneratorContext;

import java.io.File;

/**
 * FolderUtils
 */
public final class FolderUtils {

    /**
     * FolderUtils
     */
    private FolderUtils() {}

    /**
     * removeProxyFolder
     *
     * @param pathName String
     */
    public static void removeProxyFolder(final String pathName) {
        final File[] listFiles = new File(pathName).listFiles();

        if (listFiles != null) {
            for (final File file : listFiles) {
                if (file.isDirectory()) {
                    final String directoryName = file.getName();
                    if (GeneratorContext.PROXY_FOLDER.equals(directoryName)) {
                        removeProxiesFiles(file);
                    } else {
                        removeProxyFolder(file.getAbsolutePath());
                    }
                }
            }
        }
    }

    /**
     * removeProxiesFiles
     *
     * @param folder File
     */
    private static void removeProxiesFiles(final File folder) {
        if (folder.isDirectory()) {
            final File[] listFiles = folder.listFiles();

            if (listFiles != null) {
                for (final File file : listFiles) {
                    if (file.isDirectory()) {
                        removeProxiesFiles(file);
                    }

                    final String fileName = file.getName();

                    if (fileName.endsWith(GeneratorContext.PROXY + GeneratorContext.JAVA_FILE_EXTENSION)) {
                        file.delete();
                    }
                }
            }

            folder.delete();
        }
    }
}
