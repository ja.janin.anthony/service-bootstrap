package com.ajja.service.bootstrap.generator.utils;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * MethodUtils
 */
public final class MethodUtils {

    /**
     * MethodUtils
     */
    private MethodUtils() {}

    /**
     * getParameterTypes
     *
     * @param classLoader ClassLoader
     * @param method Method
     * @return List<Class>
     */
    public static List<Class> getParameterTypes(final ClassLoader classLoader, final Method method) {
        final List<Class> types = new ArrayList<>();

        for (final Parameter parameter : method.getParameters()) {
            if (parameter.getParameterizedType() instanceof ParameterizedType) {
                try {
                    final ParameterizedType parameterizedType = (ParameterizedType) parameter.getParameterizedType();
                    types.add(classLoader.loadClass(parameterizedType.getRawType().getTypeName()));

                    for (final Type actualTypeArgument : parameterizedType.getActualTypeArguments()) {
                        types.add(classLoader.loadClass(actualTypeArgument.getTypeName()));
                    }
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
            } else {
                types.add(parameter.getType());
            }
        }

        return types;
    }

    /**
     * getReturnTypes
     *
     * @param classLoader ClassLoader
     * @param method Method
     * @return List<Class>
     */
    public static List<Class> getReturnTypes(final ClassLoader classLoader, final Method method) {
        final List<Class> types = new ArrayList<>();

        if (method.getGenericReturnType() instanceof ParameterizedType) {
            try {
                final ParameterizedType parameterizedType = (ParameterizedType) method.getGenericReturnType();
                types.add(classLoader.loadClass(parameterizedType.getRawType().getTypeName()));

                for (final Type actualTypeArgument : parameterizedType.getActualTypeArguments()) {
                    types.add(classLoader.loadClass(actualTypeArgument.getTypeName()));
                }
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        } else {
            types.add(method.getReturnType());
        }

        return types;
    }
}
