package com.ajja.service.bootstrap.generator.utils;

/**
 * ParameterUtils
 */
public final class ParameterUtils {

    /**
     * ParameterUtils
     */
    private ParameterUtils() {}

    /**
     * getParameterName
     *
     * @param parameter String
     * @return String
     */
    public static String getParameterName(final String parameter) {
        final String[] args = parameter.split(" ");
        if (args.length != 2) {
            return "";
        }

        final String type = args[0];
        final String name = args[1];

        if ("Request".equals(type)) {
            return "new RequestImpl(" + name + ")";
        }

        return name;
    }
}
