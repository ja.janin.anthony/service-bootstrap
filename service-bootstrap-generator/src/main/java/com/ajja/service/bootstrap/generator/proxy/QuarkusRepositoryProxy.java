package com.ajja.service.bootstrap.generator.proxy;

import com.ajja.service.bootstrap.common.business.repository.IRepository;
import com.ajja.service.bootstrap.generator.builder.MethodCodeBuilder;
import com.ajja.service.bootstrap.generator.builder.SourceCodeBuilder;
import com.ajja.service.bootstrap.generator.builder.TemplateBuilder;
import com.ajja.service.bootstrap.generator.context.GeneratorContext;
import com.ajja.service.bootstrap.generator.utils.InterfaceJavaUtils;
import com.ajja.service.bootstrap.generator.utils.JavaCompilerUtils;
import com.ajja.service.bootstrap.generator.utils.MethodUtils;
import com.ajja.service.bootstrap.generator.utils.ParameterUtils;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * QuarkusRepositoryProxy
 */
public final class QuarkusRepositoryProxy {

    /**
     * QuarkusRepositoryProxy
     */
    private QuarkusRepositoryProxy() {}

    /**
     * generate
     *
     * @param anInterface Class<?>
     * @param classLoader ClassLoader
     * @param projectPathBusiness String
     * @param projectPathImpl String
     * @throws ClassNotFoundException exception
     */
    public static void generate(final Class<?> anInterface, final ClassLoader classLoader, final String projectPathBusiness, final String projectPathImpl) throws ClassNotFoundException {
        final String interfaceName = anInterface.getSimpleName();
        if (interfaceName.charAt(0) != 'I') {
            throw new RuntimeException(String.format("%s must begin by letter 'I'", interfaceName));
        }

        if (Arrays.stream(anInterface.getInterfaces()).filter(currentInterface -> IRepository.class.getSimpleName().equals(currentInterface.getSimpleName())).findFirst().isEmpty()) {
            throw new RuntimeException(String.format("%s must implement 'IRepository'.", interfaceName));
        }

        final String repositoryName = interfaceName.substring(1);
        final Class<?> aClass = classLoader.loadClass(anInterface.getPackageName().replace(".business.", ".application.") + "." + repositoryName + "Impl");

        final String packageName = anInterface.getPackageName().replace(".business.", ".impl.quarkus.proxy.");
        final String className = repositoryName + GeneratorContext.PROXY;

        final TemplateBuilder templateBuilder = new TemplateBuilder("generator/quarkus/repository_proxy_template.txt");
        templateBuilder.setPlaceholder("packageName", packageName);
        templateBuilder.setPlaceholder("repositoryName", repositoryName);

        final SourceCodeBuilder sourceCodeBuilder = new SourceCodeBuilder(templateBuilder.build());
        sourceCodeBuilder.addImport(anInterface);
        sourceCodeBuilder.addImport(aClass);

        for (final String signature : InterfaceJavaUtils.getSignatures(anInterface, projectPathBusiness)) {
            final String returnType = signature.substring(0, signature.indexOf(' '));
            final String methodName = signature.substring(signature.indexOf(' ') + 1, signature.indexOf('('));
            final String[] parameters = signature.substring(signature.indexOf('(') + 1, signature.lastIndexOf(')')).split("\\s*,\\s*");

            final Method method = Arrays.stream(aClass.getMethods()).filter(currentMethodImpl -> currentMethodImpl.getName().equals(methodName)).findFirst().get();
            MethodUtils.getParameterTypes(classLoader, method).stream().forEach(currentType -> sourceCodeBuilder.addImport(currentType));
            MethodUtils.getReturnTypes(classLoader, method).stream().forEach(currentType -> sourceCodeBuilder.addImport(currentType));

            final MethodCodeBuilder methodCodeBuilder = new MethodCodeBuilder();
            methodCodeBuilder.addAnnotation(Override.class);
            methodCodeBuilder.setVisibility("public");
            methodCodeBuilder.setSignature(signature);

            final String parameterNames = Arrays.stream(parameters).filter(currentParameter -> currentParameter != null && !currentParameter.isEmpty()).map(currentParameter -> ParameterUtils.getParameterName(currentParameter)).collect(Collectors.joining(", "));
            if ("void".equals(returnType) || "Void".equals(returnType)) {
                methodCodeBuilder.setBody(String.format("repository.%s(%s);", methodName, parameterNames));
            } else {
                methodCodeBuilder.setBody(String.format("return repository.%s(%s);", methodName, parameterNames));
            }

            sourceCodeBuilder.addMethod(methodCodeBuilder);
        }

        final String sourceCode = sourceCodeBuilder.build();
        JavaCompilerUtils.compile(classLoader, projectPathImpl, packageName, className, sourceCode);
    }
}
