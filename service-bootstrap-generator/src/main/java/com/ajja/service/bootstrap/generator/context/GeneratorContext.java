package com.ajja.service.bootstrap.generator.context;

import java.io.File;

/**
 * GeneratorContext
 */
public final class GeneratorContext {

    public static final String SRC_MAIN_JAVA = "src" + File.separator + "main" + File.separator + "java";

    public static final String SRC_MAIN_RESOURCES = "src" + File.separator + "main" + File.separator + "resources";

    public static final String JAVA_FILE_EXTENSION = ".java";

    public static final String TARGET_CLASSES = "target" + File.separator + "classes";

    public static final String REFLECTION_CONFIG_FILE = "reflection-config.json";

    public static final String PROXY_FOLDER = "proxy";

    public static final String PROXY = "Proxy";
}
