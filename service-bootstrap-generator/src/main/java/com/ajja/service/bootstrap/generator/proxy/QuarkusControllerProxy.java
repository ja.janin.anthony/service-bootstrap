package com.ajja.service.bootstrap.generator.proxy;

import com.ajja.service.bootstrap.common.application.annotation.*;
import com.ajja.service.bootstrap.common.application.controller.IController;
import com.ajja.service.bootstrap.common.application.controller.request.IRequest;
import com.ajja.service.bootstrap.common.application.controller.response.IResponse;
import com.ajja.service.bootstrap.common.impl.quarkus.controller.request.RequestImpl;
import com.ajja.service.bootstrap.common.impl.quarkus.controller.response.ResponseFactory;
import com.ajja.service.bootstrap.generator.builder.MethodCodeBuilder;
import com.ajja.service.bootstrap.generator.builder.SourceCodeBuilder;
import com.ajja.service.bootstrap.generator.builder.TemplateBuilder;
import com.ajja.service.bootstrap.generator.context.GeneratorContext;
import com.ajja.service.bootstrap.generator.utils.*;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.core.Context;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * QuarkusControllerProxy
 */
public final class QuarkusControllerProxy {

    /**
     * QuarkusControllerProxy
     */
    private QuarkusControllerProxy() {}

    /**
     * generate
     *
     * @param anInterface Class<?>
     * @param classLoader ClassLoader
     * @param projectPathApplication String
     * @param projectPathImpl String
     * @throws ClassNotFoundException exception
     */
    public static void generate(final Class<?> anInterface, final ClassLoader classLoader, final String projectPathApplication, final String projectPathImpl) throws ClassNotFoundException {
        final String interfaceName = anInterface.getSimpleName();
        if (interfaceName.charAt(0) != 'I') {
            throw new RuntimeException(String.format("%s must begin by letter 'I'", interfaceName));
        }

        if (Arrays.stream(anInterface.getInterfaces()).filter(currentInterface -> IController.class.getSimpleName().equals(currentInterface.getSimpleName())).findFirst().isEmpty()) {
            throw new RuntimeException(String.format("%s must implement 'IController'.", interfaceName));
        }

        final String controllerName = interfaceName.substring(1);
        final Class<?> aClass = classLoader.loadClass(anInterface.getPackageName() + "." + controllerName + "Impl");

        final RequestMapping requestMappingAnnotation = aClass.getAnnotation(RequestMapping.class);
        if (requestMappingAnnotation == null) {
            throw new RuntimeException(String.format("%s must have annotation 'RequestMapping'.", aClass.getSimpleName()));
        }
        final String route = requestMappingAnnotation.value();

        final String packageName = anInterface.getPackageName().replace(".application.", ".impl.quarkus.proxy.");
        final String className = controllerName + GeneratorContext.PROXY;

        final TemplateBuilder templateBuilder = new TemplateBuilder("generator/quarkus/controller_proxy_template.txt");
        templateBuilder.setPlaceholder("packageName", packageName);
        templateBuilder.setPlaceholder("route", route);
        templateBuilder.setPlaceholder("controllerName", controllerName);

        final SourceCodeBuilder sourceCodeBuilder = new SourceCodeBuilder(templateBuilder.build());
        sourceCodeBuilder.addImport(anInterface);
        sourceCodeBuilder.addImport(aClass);
        sourceCodeBuilder.addImport(Context.class);

        for (final String signature : ClassJavaUtils.getSignatures(aClass, projectPathApplication)) {
            final String methodName = signature.substring(signature.indexOf(' ') + 1, signature.indexOf('('));
            final String[] parameters = signature.substring(signature.indexOf('(') + 1, signature.lastIndexOf(')')).split("\\s*,\\s*");

            final Method method = Arrays.stream(aClass.getMethods()).filter(currentMethodImpl -> currentMethodImpl.getName().equals(methodName)).findFirst().get();
            MethodUtils.getParameterTypes(classLoader, method).stream().forEach(currentType -> sourceCodeBuilder.addImport(currentType));

            sourceCodeBuilder.addImport(Inject.class);

            final MethodCodeBuilder methodCodeBuilder = new MethodCodeBuilder();
            methodCodeBuilder.addAnnotation(Inject.class);
            methodCodeBuilder.setVisibility("public");
            methodCodeBuilder.setSignature(signature);

            final String parameterNames = Arrays.stream(parameters).map(currentParameter -> ParameterUtils.getParameterName(currentParameter)).collect(Collectors.joining(", "));
            methodCodeBuilder.setBody(String.format("controller.%s(%s);", methodName, parameterNames));

            sourceCodeBuilder.addMethod(methodCodeBuilder);
        }

        for (String signature : InterfaceJavaUtils.getSignatures(anInterface, projectPathApplication)) {
            final String returnType = signature.substring(0, signature.indexOf(' '));
            final String methodName = signature.substring(signature.indexOf(' ') + 1, signature.indexOf('('));
            final String[] parameters = signature.substring(signature.indexOf('(') + 1, signature.lastIndexOf(')')).split("\\s*,\\s*");

            final Method method = Arrays.stream(aClass.getMethods()).filter(currentMethodImpl -> currentMethodImpl.getName().equals(methodName)).findFirst().get();
            MethodUtils.getParameterTypes(classLoader, method).stream().filter(currentType -> !IRequest.class.equals(currentType)).forEach(currentType -> sourceCodeBuilder.addImport(currentType));
            if (signature.indexOf("Request ") != -1) {
                sourceCodeBuilder.addImport(RequestImpl.class);
            }
            MethodUtils.getReturnTypes(classLoader, method).stream().filter(currentType -> !IResponse.class.equals(currentType)).forEach(currentType -> sourceCodeBuilder.addImport(currentType));
            if ("Response".equals(returnType)) {
                sourceCodeBuilder.addImport(ResponseFactory.class);
            }

            final MethodCodeBuilder methodCodeBuilder = new MethodCodeBuilder();
            for (final Annotation annotation : method.getAnnotations()) {
                if (annotation.annotationType().equals(PostMapping.class)) {
                    final PostMapping postMappingAnnotation = (PostMapping) annotation;
                    methodCodeBuilder.addAnnotation(POST.class);

                    final String value = postMappingAnnotation.value();
                    if (value != null && !value.isEmpty()) {
                        methodCodeBuilder.addAnnotation(String.format("@Path(\"%s\")", value));
                    }
                } else if (annotation.annotationType().equals(GetMapping.class)) {
                    final GetMapping getMappingAnnotation = (GetMapping) annotation;
                    methodCodeBuilder.addAnnotation(GET.class);

                    final String value = getMappingAnnotation.value();
                    if (value != null && !value.isEmpty()) {
                        methodCodeBuilder.addAnnotation(String.format("@Path(\"%s\")", value));
                    }
                } else if (annotation.annotationType().equals(PutMapping.class)) {
                    final PutMapping putMappingAnnotation = (PutMapping) annotation;
                    methodCodeBuilder.addAnnotation(PUT.class);

                    final String value = putMappingAnnotation.value();
                    if (value != null && !value.isEmpty()) {
                        methodCodeBuilder.addAnnotation(String.format("@Path(\"%s\")", value));
                    }
                } else if (annotation.annotationType().equals(DeleteMapping.class)) {
                    final DeleteMapping deleteMappingAnnotation = (DeleteMapping) annotation;
                    methodCodeBuilder.addAnnotation(DELETE.class);

                    final String value = deleteMappingAnnotation.value();
                    if (value != null && !value.isEmpty()) {
                        methodCodeBuilder.addAnnotation(String.format("@Path(\"%s\")", value));
                    }
                } else if (annotation.annotationType().equals(RolesAllowed.class)) {
                    final RolesAllowed rolesAllowedAnnotation = (RolesAllowed) annotation;

                    final String[] value = rolesAllowedAnnotation.value();
                    if (value != null && value.length != 0) {
                        sourceCodeBuilder.addImport(javax.annotation.security.RolesAllowed.class);
                        methodCodeBuilder.addAnnotation(String.format("@RolesAllowed({\"%s\"})", String.join("\", \"", value)));
                    }
                }
            }
            for (final Parameter parameter : method.getParameters()) {
                final int index = Integer.valueOf(parameter.getName().replace("arg", StringUtils.EMPTY));

                for (final Annotation annotation : parameter.getAnnotations()) {
                    if (annotation.annotationType().equals(PathVariable.class)) {
                        final PathVariable pathVariableAnnotation = (PathVariable) annotation;

                        final String value = pathVariableAnnotation.value();
                        if (value != null && !value.isEmpty()) {
                            signature = signature.replace(parameters[index], String.format("@PathParam(\"%s\") %s", value, parameters[index]));
                        }
                    }
                }
            }
            signature = signature.replace(", Request request)", ", @Context HttpServerRequest request)").replace("(Request request)", "(@Context HttpServerRequest request)");

            methodCodeBuilder.addAnnotation(Override.class);
            methodCodeBuilder.setVisibility("public");
            methodCodeBuilder.setSignature(signature);

            final String parameterNames = Arrays.stream(parameters).map(currentParameter -> ParameterUtils.getParameterName(currentParameter)).collect(Collectors.joining(", "));
            if ("Response".equals(returnType)) {
                methodCodeBuilder.setBody(String.format("return ResponseFactory.build(controller.%s(%s));", methodName, parameterNames));
            } else if ("void".equals(returnType) || "Void".equals(returnType)) {
                methodCodeBuilder.setBody(String.format("controller.%s(%s);", methodName, parameterNames));
            } else {
                methodCodeBuilder.setBody(String.format("return controller.%s(%s);", methodName, parameterNames));
            }

            sourceCodeBuilder.addMethod(methodCodeBuilder);
        }

        final String sourceCode = sourceCodeBuilder.build();
        JavaCompilerUtils.compile(classLoader, projectPathImpl, packageName, className, sourceCode);
    }
}
