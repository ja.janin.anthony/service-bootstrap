package com.ajja.service.bootstrap.generator.utils;

import com.ajja.service.bootstrap.generator.context.GeneratorContext;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ClassJavaUtils
 */
public final class ClassJavaUtils {

    /**
     * ClassJavaUtils
     */
    private ClassJavaUtils() {}

    /**
     * getSignatures
     *
     * @param aClass Class<?>
     * @param projectPath String
     * @return List<String>
     */
    public static List<String> getSignatures(final Class<?> aClass, final String projectPath) {
        final String pathName = projectPath + File.separator + GeneratorContext.SRC_MAIN_JAVA + File.separator + aClass.getName().replace(".", File.separator) + GeneratorContext.JAVA_FILE_EXTENSION;
        final String content = FileUtils.getContent(pathName);

        return Arrays.stream(content.substring(content.indexOf('{') + 1, content.lastIndexOf('}')).split("\n"))
            .map(line -> line.trim())
            .filter(line -> line.startsWith("public void set") && line.endsWith(") {"))
            .map(line -> line.substring("public ".length(), line.lastIndexOf(") {") + 1))
            .collect(Collectors.toList());
    }
}
