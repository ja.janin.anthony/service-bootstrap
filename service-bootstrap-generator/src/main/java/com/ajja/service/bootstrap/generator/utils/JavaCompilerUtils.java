package com.ajja.service.bootstrap.generator.utils;

import com.ajja.service.bootstrap.generator.context.GeneratorContext;

import javax.tools.JavaCompiler;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

/**
 * JavaCompilerUtils
 */
public final class JavaCompilerUtils {

    /**
     * JavaCompilerUtils
     */
    private JavaCompilerUtils() {}

    /**
     * compile
     *
     * @param classLoader ClassLoader
     * @param projectPath String
     * @param packageName String
     * @param className String
     * @param sourceCode String
     */
    public static void compile(final ClassLoader classLoader, final String projectPath, final String packageName, final String className, final String sourceCode) {
        final String osName = System.getProperty("os.name");

        try {
            final File sourceFile = createFile(projectPath, packageName, className, sourceCode);
            final File targetDirectory = new File(projectPath + File.separator + GeneratorContext.TARGET_CLASSES);

            final JavaCompiler javaCompiler = ToolProvider.getSystemJavaCompiler();

            try (final StandardJavaFileManager standardJavaFileManager = javaCompiler.getStandardFileManager(null, null, null)) {
                standardJavaFileManager.setLocation(StandardLocation.CLASS_OUTPUT, Collections.singletonList(targetDirectory));

                final Collection<String> files = Arrays.stream(((URLClassLoader) classLoader).getURLs()).map(url -> osName.startsWith("Windows") && url.getFile().startsWith("/") ? url.getFile().substring(1) : url.getFile()).collect(Collectors.toList());
                final String classpath = System.getProperty("java.class.path") + File.pathSeparator + String.join(File.pathSeparator, files);

                final Collection<String> options = new ArrayList<>();
                options.addAll(Arrays.asList("-classpath", classpath));
                options.add("-Xlint:unchecked");
                options.add("-deprecation");

                final JavaCompiler.CompilationTask compilationTask = javaCompiler.getTask(null, standardJavaFileManager, null, options, null, standardJavaFileManager.getJavaFileObjectsFromFiles(Collections.singletonList(sourceFile)));
                compilationTask.call();

                classLoader.loadClass(packageName + "." + className);
            }
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * createFile
     *
     * @param projectPath String
     * @param packageName String
     * @param className String
     * @param sourceCode String
     * @return File
     */
    private static File createFile(final String projectPath, final String packageName, final String className, final String sourceCode) {
        final File sourceDirectory = new File(projectPath + File.separator + GeneratorContext.SRC_MAIN_JAVA + File.separator + packageName.replace(".", File.separator));
        sourceDirectory.mkdirs();

        final File sourceFile = new File(sourceDirectory, className + GeneratorContext.JAVA_FILE_EXTENSION);
        try {
            try (final FileWriter fileWriter = new FileWriter(sourceFile)) {
                fileWriter.write(sourceCode);
                return sourceFile;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
