package {{packageName}};

import com.ajja.service.bootstrap.common.business.repository.IRepositoryLocator;
import com.ajja.service.bootstrap.common.business.service.IServiceLocator;
import io.quarkus.arc.Unremovable;
import io.quarkus.runtime.annotations.RegisterForReflection;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
@Unremovable
@RegisterForReflection
@Named("I{{serviceName}}")
public class {{serviceName}}Proxy implements I{{serviceName}} {

    private {{serviceName}}Impl service = new {{serviceName}}Impl();

    @Inject
    public void setRepositoryLocator(IRepositoryLocator repositoryLocator) {
        service.setRepositoryLocator(repositoryLocator);
    }

    @Inject
    public void setServiceLocator(IServiceLocator serviceLocator) {
        service.setServiceLocator(serviceLocator);
    }
}
