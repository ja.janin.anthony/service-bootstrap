package {{packageName}};

import com.ajja.service.bootstrap.common.application.sql.ISQLConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("I{{repositoryName}}")
public class {{repositoryName}}Proxy implements I{{repositoryName}} {

    private {{repositoryName}}Impl repository = new {{repositoryName}}Impl();

    @Autowired
    public void setSQLConnection(ISQLConnection sqlConnection) {
        repository.setSQLConnection(sqlConnection);
    }
}
