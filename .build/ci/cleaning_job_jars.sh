#!/bin/bash

set -e

echo "=========================================================================================="
echo "Begin cleaning_job_jars.sh"

docker rmi -f $(docker images | grep none | awk '{print $3}') &> /dev/null || true

docker rmi -f $(docker images maven -aq) || true

docker ps -a
docker images
docker network ls
docker volume ls

echo "End cleaning_job_jars.sh"
echo "=========================================================================================="
