#!/bin/bash

set -e

echo "=========================================================================================="
echo "Begin push_jar.sh"

VERSION=`cat pom.xml | grep "^    <version>.*</version>$" | awk -F'[><]' '{print $3}'`
echo "Version : ${VERSION}"

IS_SNAPSHOT="yes"
echo ${VERSION} | grep "SNAPSHOT" &> /dev/null || IS_SNAPSHOT="no"
echo "Is snapshot : ${IS_SNAPSHOT}"

sed -i "s#</project>#<distributionManagement><snapshotRepository><id>nexus-snapshots</id><url>${CI_REGISTRY_SNAPSHOTS}</url></snapshotRepository><repository><id>nexus-releases</id><url>${CI_REGISTRY_RELEASES}</url></repository></distributionManagement></project>#" pom.xml

sed -i "s|{{CI_REGISTRY_SNAPSHOTS}}|${CI_REGISTRY_SNAPSHOTS}|g" .build/maven/settings.xml
sed -i "s|{{CI_REGISTRY_RELEASES}}|${CI_REGISTRY_RELEASES}|g" .build/maven/settings.xml
sed -i "s|{{CI_REGISTRY_USER}}|${CI_REGISTRY_USER}|g" .build/maven/settings.xml
sed -i "s|{{CI_REGISTRY_PASSWORD}}|${CI_REGISTRY_PASSWORD}|g" .build/maven/settings.xml

echo "Pushing to registry (it will take a few minutes...)"
docker run --rm \
  -v "$PWD":/usr/service-bootstrap \
  -w /usr/service-bootstrap \
  maven:3.6.0-jdk-11 \
  mvn -s .build/maven/settings.xml deploy -DskipTests &> /dev/null

echo "End push_jar.sh"
echo "=========================================================================================="
