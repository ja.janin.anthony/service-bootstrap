package com.ajja.service.bootstrap.common.application.sql.exception;

/**
 * SQLException
 */
public final class SQLException extends Exception {

    /**
     * SQLException
     *
     * @param cause Throwable
     */
    public SQLException(final Throwable cause) {
        super(cause);
    }

    /**
     * SQLException
     *
     * @param message String
     */
    public SQLException(final String message) {
        super(message);
    }
}
