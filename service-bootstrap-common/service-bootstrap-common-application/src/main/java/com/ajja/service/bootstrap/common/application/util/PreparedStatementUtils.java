package com.ajja.service.bootstrap.common.application.util;

import com.ajja.service.bootstrap.common.application.sql.IPreparedStatement;
import com.ajja.service.bootstrap.common.application.sql.exception.SQLException;
import com.ajja.service.bootstrap.core.utils.ReflectionUtils;
import com.ajja.service.bootstrap.core.utils.StringUtils;
import org.apache.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * PreparedStatementUtils
 */
public final class PreparedStatementUtils {

    private static final Logger logger = Logger.getLogger(PreparedStatementUtils.class);

    /**
     * PreparedStatementUtils
     */
    private PreparedStatementUtils() {}

    /**
     * populate
     *
     * @param preparedStatement IPreparedStatement
     * @param entity T
     * @param column String
     * @param index int
     * @param <T>
     */
    public static <T> void populate(final IPreparedStatement preparedStatement, final T entity, final String column, final int index) {
        try {
            final String columnCamelCase = StringUtils.toCamelCase(column);

            final Class<?> entityClass = entity.getClass();

            final String methodName;
            if (columnCamelCase.startsWith("Is")) {
                methodName = columnCamelCase.substring(0, 1).toLowerCase() + columnCamelCase.substring(1);;
            } else {
                methodName = "get" + columnCamelCase;
            }

            final Method method = entityClass.getMethod(methodName);
            final Class<?> type = ReflectionUtils.getType(method, entityClass);

            final Object value = method.invoke(entity);
            setValue(preparedStatement, type, index, value);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | SQLException e) {
            logger.error(e);
        }
    }

    /**
     * setValue
     *
     * @param preparedStatement IPreparedStatement
     * @param type Class<?>
     * @param index int
     * @param value Object
     * @throws SQLException exception
     */
    public static void setValue(final IPreparedStatement preparedStatement, final Class<?> type, final int index, final Object value) throws SQLException {
        if (String.class.equals(type)) {
            preparedStatement.setString(index, (String) value);
        } else if (int.class.equals(type) || Integer.class.equals(type)) {
            preparedStatement.setInt(index, (Integer) value);
        } else if (long.class.equals(type) || Long.class.equals(type)) {
            preparedStatement.setLong(index, (Long) value);
        } else if (float.class.equals(type) || Float.class.equals(type)) {
            preparedStatement.setFloat(index, (Long) value);
        } else if (double.class.equals(type) || Double.class.equals(type)) {
            preparedStatement.setDouble(index, (Double) value);
        } else if (boolean.class.equals(type) || Boolean.class.equals(type)) {
            preparedStatement.setBoolean(index, (Boolean) value);
        } else if (Date.class.equals(type)) {
            preparedStatement.setDate(index, (Date) value);
        } else if (List.class.equals(type) || Set.class.equals(type)) {
            preparedStatement.setString(index, (String) ((Collection) value).stream().map(v -> v.toString()).collect(Collectors.joining(", ")));
        } else if (type.isEnum()) {
            preparedStatement.setString(index, value.toString());
        } else {
            preparedStatement.setObject(index, value);
        }
    }
}
