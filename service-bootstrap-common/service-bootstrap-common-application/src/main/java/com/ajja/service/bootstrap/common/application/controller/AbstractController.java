package com.ajja.service.bootstrap.common.application.controller;

import com.ajja.service.bootstrap.common.business.service.IService;
import com.ajja.service.bootstrap.common.business.service.IServiceLocator;

/**
 * AbstractController
 */
public abstract class AbstractController {

    private IServiceLocator serviceLocator;

    /**
     * setServiceLocator
     *
     * @param serviceLocator IServiceLocator
     */
    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    /**
     * getService
     *
     * @param aClass Class<T>
     * @param <T extends IService
     * @return T
     */
    protected <T extends IService> T getService(final Class<T> aClass) {
        return serviceLocator.get(aClass);
    }
}
