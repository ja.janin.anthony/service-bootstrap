package com.ajja.service.bootstrap.common.application.sql;

import com.ajja.service.bootstrap.common.application.sql.exception.SQLException;

import java.util.Date;

/**
 * IPreparedStatement
 */
public interface IPreparedStatement extends AutoCloseable {

    /**
     * RETURN_GENERATED_KEYS
     */
    int RETURN_GENERATED_KEYS = 1;

    /**
     * close
     *
     * @throws SQLException exception
     */
    void close() throws SQLException;

    /**
     * executeQuery
     *
     * @return IResultSet
     * @throws SQLException exception
     */
    IResultSet executeQuery() throws SQLException;

    /**
     * executeUpdate
     *
     * @return int
     * @throws SQLException exception
     */
    int executeUpdate() throws SQLException;

    /**
     * execute
     *
     * @return boolean
     * @throws SQLException exception
     */
    boolean execute() throws SQLException;

    /**
     * getGeneratedKeys
     *
     * @return IResultSet
     * @throws SQLException exception
     */
    IResultSet getGeneratedKeys() throws SQLException;

    /**
     * setString
     *
     * @param parameterIndex int
     * @param value Object
     * @throws SQLException exception
     */
    void setString(int parameterIndex, String value) throws SQLException;

    /**
     * setInt
     *
     * @param parameterIndex int
     * @param value Object
     * @throws SQLException exception
     */
    void setInt(int parameterIndex, int value) throws SQLException;

    /**
     * setLong
     *
     * @param parameterIndex int
     * @param value Object
     * @throws SQLException exception
     */
    void setLong(int parameterIndex, long value) throws SQLException;

    /**
     * setFloat
     *
     * @param parameterIndex int
     * @param value Object
     * @throws SQLException exception
     */
    void setFloat(int parameterIndex, float value) throws SQLException;

    /**
     * setDouble
     *
     * @param parameterIndex int
     * @param value Object
     * @throws SQLException exception
     */
    void setDouble(int parameterIndex, double value) throws SQLException;

    /**
     * setBoolean
     *
     * @param parameterIndex int
     * @param value Object
     * @throws SQLException exception
     */
    void setBoolean(int parameterIndex, boolean value) throws SQLException;

    /**
     * setDate
     *
     * @param parameterIndex int
     * @param value Date
     * @throws SQLException exception
     */
    void setDate(int parameterIndex, Date value) throws SQLException;

    /**
     * setObject
     *
     * @param parameterIndex int
     * @param value Object
     * @throws SQLException exception
     */
    void setObject(int parameterIndex, Object value) throws SQLException;
}
