package com.ajja.service.bootstrap.common.application.sql;

import com.ajja.service.bootstrap.common.application.sql.exception.SQLException;

/**
 * IResultSetMetaData
 */
public interface IResultSetMetaData {

    /**
     * getColumnCount
     *
     * @return int
     * @throws SQLException exception
     */
    int getColumnCount() throws SQLException;

    /**
     * getColumnName
     *
     * @param column
     * @return String
     * @throws SQLException exception
     */
    String getColumnName(int column) throws SQLException;
}
