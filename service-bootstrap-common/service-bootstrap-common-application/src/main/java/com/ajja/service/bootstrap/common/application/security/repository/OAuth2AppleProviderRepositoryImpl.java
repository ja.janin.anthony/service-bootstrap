package com.ajja.service.bootstrap.common.application.security.repository;

import com.ajja.service.bootstrap.common.application.util.HttpRequestUtils;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.RedirectUrlResponse;
import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;
import com.ajja.service.bootstrap.common.business.security.entity.type.Role;
import com.ajja.service.bootstrap.common.business.security.repository.IOAuth2AppleProviderRepository;
import com.ajja.service.bootstrap.common.business.util.PasswordUtils;
import org.json.JSONObject;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * OAuth2AppleProviderRepositoryImpl
 */
public class OAuth2AppleProviderRepositoryImpl implements IOAuth2AppleProviderRepository {

    private String clientId;

    private String clientSecret;

    private String redirectUri;

    private String authorizationUri;

    private String tokenUri;

    private String userInfoUri;

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public void setAuthorizationUri(String authorizationUri) {
        this.authorizationUri = authorizationUri;
    }

    public void setTokenUri(String tokenUri) {
        this.tokenUri = tokenUri;
    }

    public void setUserInfoUri(String userInfoUri) {
        this.userInfoUri = userInfoUri;
    }

    @Override
    public RedirectUrlResponse getRedirectUrl(String state) {
        final String url = authorizationUri + "?" +
            "response_type=code" +
            "&scope=openid" +
            "&client_id=" + clientId +
            "&redirect_uri=" + redirectUri +
            "&state=" + state;

        final RedirectUrlResponse redirectUrlResponse = new RedirectUrlResponse();
        redirectUrlResponse.setRedirectUrl(url);

        return redirectUrlResponse;
    }

    @Override
    public User getUser(String code) {
        final String idToken = getIdToken(code);
        final String payload = new String(Base64.getDecoder().decode(idToken.split("\\.")[1]));

        final JSONObject result = new JSONObject(payload);
        final String id = result.getString("sub");
        final String name = "";
        final String email = result.has("email") ? result.getString("email") : "";

        final User user = new User();
        user.setName(name);
        user.setEmail(email);
        user.setEmailVerified(!email.isEmpty());
        user.setPassword(PasswordUtils.generate(8));
        user.setProvider(Provider.PROVIDER_APPLE);
        user.setProviderId(id);
        user.setRoles(Set.of(Role.ROLE_USER));

        return user;
    }

    /**
     * getIdToken
     *
     * @param code String
     * @return String
     */
    private String getIdToken(final String code) {
        final Map<String, String> body = new HashMap<>();
        body.put("grant_type", "authorization_code");
        body.put("client_id", clientId);
        body.put("client_secret", clientSecret);
        body.put("redirect_uri", redirectUri);
        body.put("code", code);

        final JSONObject result = HttpRequestUtils.doPostForm(tokenUri, body);
        final String idToken = result.getString("id_token");

        return idToken;
    }
}
