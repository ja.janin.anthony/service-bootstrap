package com.ajja.service.bootstrap.common.application.util;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * HttpRequestUtils
 */
public final class HttpRequestUtils {

    private static final Logger logger = Logger.getLogger(HttpRequestUtils.class);

    /**
     * HttpRequestUtils
     */
    private HttpRequestUtils() {}

    /**
     * doGetJSON
     *
     * @param url String
     * @return JSONObject
     */
    public static JSONObject doGetJSON(final String url) {
        try {
            final HttpClient client = HttpClient.newHttpClient();
            final HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Accept", "application/json")
                .build();

            final HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == 200) {
                return new JSONObject(response.body());
            }
        } catch (IOException | InterruptedException e) {
            logger.error(e);
        }

        return null;
    }

    /**
     * doGetJSON
     *
     * @param url String
     * @param bearer String
     * @return JSONObject
     */
    public static JSONObject doGetJSON(final String url, final String bearer) {
        try {
            final HttpClient client = HttpClient.newHttpClient();
            final HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Accept", "application/json")
                .header("Authorization", String.format("Bearer %s", bearer))
                .build();

            final HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == 200) {
                return new JSONObject(response.body());
            }
        } catch (IOException | InterruptedException e) {
            logger.error(e);
        }

        return null;
    }

    /**
     * doPostJSON
     *
     * @param url String
     * @param body JSONObject
     * @return JSONObject
     */
    public static JSONObject doPostJSON(final String url, final JSONObject body) {
        try {
            final HttpClient client = HttpClient.newHttpClient();
            final HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(body.toString()))
                .build();

            final HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == 200) {
                return new JSONObject(response.body());
            }
        } catch (IOException | InterruptedException e) {
            logger.error(e);
        }

        return null;
    }

    /**
     * doPostForm
     *
     * @param url String
     * @param body Map<String, String>
     * @return JSONObject
     */
    public static JSONObject doPostForm(final String url, final Map<String, String> body) {
        try {
            final HttpClient client = HttpClient.newHttpClient();
            final HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Content-Type", "application/x-www-form-urlencoded")
                .POST(HttpRequest.BodyPublishers.ofString(body.entrySet().stream().map(entry -> entry.getKey() + "=" + entry.getValue()).collect(Collectors.joining("&"))))
                .build();

            final HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == 200) {
                return new JSONObject(response.body());
            }
        } catch (IOException | InterruptedException e) {
            logger.error(e);
        }

        return null;
    }
}
