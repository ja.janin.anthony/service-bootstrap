package com.ajja.service.bootstrap.common.application.sql;

/**
 * AbstractSQLRepository
 */
public abstract class AbstractSQLRepository {

    private ISQLConnection sqlConnection;

    /**
     * setSQLConnection
     *
     * @param sqlConnection ISQLConnection
     */
    public void setSQLConnection(final ISQLConnection sqlConnection) {
        this.sqlConnection = sqlConnection;
    }

    /**
     * getSQLQueryFactory
     *
     * @return ISQLQueryFactory
     */
    protected ISQLQueryFactory getSQLQueryFactory() {
        return new SQLQueryFactoryImpl(sqlConnection);
    }
}
