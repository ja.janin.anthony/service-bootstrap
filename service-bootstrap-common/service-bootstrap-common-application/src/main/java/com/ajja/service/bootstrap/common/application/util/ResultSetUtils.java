package com.ajja.service.bootstrap.common.application.util;

import com.ajja.service.bootstrap.common.application.sql.IResultSet;
import com.ajja.service.bootstrap.common.application.sql.IResultSetMetaData;
import com.ajja.service.bootstrap.common.application.sql.exception.SQLException;
import com.ajja.service.bootstrap.core.utils.ReflectionUtils;
import com.ajja.service.bootstrap.core.utils.StringUtils;
import org.apache.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * ResultSetUtils
 */
public final class ResultSetUtils {

    private static final Logger logger = Logger.getLogger(ResultSetUtils.class);

    /**
     * ResultSetUtils
     */
    private ResultSetUtils() {}

    /**
     * getColumns
     *
     * @param resultSet IResultSet
     * @return List<String>
     */
    public static List<String> getColumns(final IResultSet resultSet) {
        final List<String> columns = new ArrayList<>();
        try {
            final IResultSetMetaData metaData = resultSet.getMetaData();
            final int numColumns = metaData.getColumnCount();
            for (int i = 1; i <= numColumns; i++) {
                columns.add(metaData.getColumnName(i));
            }
        } catch (SQLException e) {
            logger.error(e);
        }

        return columns;
    }

    /**
     * populate
     *
     * @param resultSet IResultSet
     * @param entity T
     * @param column String
     * @param <T>
     */
    public static <T> void populate(final IResultSet resultSet, final T entity, final String column) {
        try {
            final String columnCamelCase = StringUtils.toCamelCase(column);

            final Class<?> entityClass = entity.getClass();

            final String methodName;
            if (columnCamelCase.startsWith("Is")) {
                methodName = columnCamelCase.substring(0, 1).toLowerCase() + columnCamelCase.substring(1);;
            } else {
                methodName = "get" + columnCamelCase;
            }

            final Method method = entityClass.getMethod(methodName);
            final Class<?> type = ReflectionUtils.getType(method, entityClass);

            final T value = getValue(resultSet, column, method, type);
            entityClass.getMethod("set" + columnCamelCase, method.getReturnType()).invoke(entity, value);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | SQLException e) {
            logger.error(e);
        }
    }

    /**
     * getValue
     *
     * @param resultSet IResultSet
     * @param column String
     * @param method Method
     * @param type Class<?>
     * @param <T>
     * @return T
     * @throws SQLException exception
     */
    private static <T> T getValue(final IResultSet resultSet, final String column, final Method method, final Class<?> type) throws SQLException {
        if (String.class.equals(type)) {
            return (T) resultSet.getString(column);
        } else if (int.class.equals(type) || Integer.class.equals(type)) {
            return (T) Integer.valueOf(resultSet.getInt(column));
        } else if (long.class.equals(type) || Long.class.equals(type)) {
            return (T) Long.valueOf(resultSet.getLong(column));
        } else if (float.class.equals(type) || Float.class.equals(type)) {
            return (T) Float.valueOf(resultSet.getFloat(column));
        } else if (double.class.equals(type) || Double.class.equals(type)) {
            return (T) Double.valueOf(resultSet.getDouble(column));
        } else if (boolean.class.equals(type) || Boolean.class.equals(type)) {
            return (T) Boolean.valueOf(resultSet.getBoolean(column));
        } else if (Date.class.equals(type)) {
            return (T) resultSet.getDate(column);
        } else if (List.class.equals(type) || Set.class.equals(type)) {
            final Type genericReturnType = method.getGenericReturnType();
            if (genericReturnType instanceof ParameterizedType) {
                final Class<?> genericType = (Class<?>) ((ParameterizedType) genericReturnType).getActualTypeArguments()[0];

                if (genericType.isEnum()) {
                    final Stream<Enum> stream = Arrays.stream(resultSet.getString(column).split("\\s*,\\s*"))
                            .map(value -> Enum.valueOf((Class<Enum>) genericType, value));

                    if (List.class.equals(type)) {
                        return (T) stream.collect(Collectors.toList());
                    } else if (Set.class.equals(type)) {
                        return (T) stream.collect(Collectors.toSet());
                    }
                } else {
                    final Stream<String> stream = Arrays.stream(resultSet.getString(column).split("\\s*,\\s*"));

                    if (List.class.equals(type)) {
                        return (T) stream.collect(Collectors.toList());
                    } else if (Set.class.equals(type)) {
                        return (T) stream.collect(Collectors.toSet());
                    }
                }
            }
        } else if (type.isEnum()) {
            return (T) Enum.valueOf((Class<Enum>) type, resultSet.getString(column));
        }

        return (T) resultSet.getObject(column);
    }
}
