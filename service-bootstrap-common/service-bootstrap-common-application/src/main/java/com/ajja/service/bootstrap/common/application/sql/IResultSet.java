package com.ajja.service.bootstrap.common.application.sql;

import com.ajja.service.bootstrap.common.application.sql.exception.SQLException;

import java.util.Date;

/**
 * IResultSet
 */
public interface IResultSet {

    /**
     * next
     *
     * @return boolean
     * @throws SQLException exception
     */
    boolean next() throws SQLException;

    /**
     * getMetaData
     *
     * @return IResultSetMetaData
     * @throws SQLException exception
     */
    IResultSetMetaData getMetaData() throws SQLException;

    /**
     * getString
     *
     * @param columnLabel String
     * @return String
     * @throws SQLException exception
     */
    String getString(String columnLabel) throws SQLException;

    /**
     * getInt
     *
     * @param columnLabel String
     * @return int
     * @throws SQLException exception
     */
    int getInt(String columnLabel) throws SQLException;

    /**
     * getLong
     *
     * @param columnIndex int
     * @return long
     * @throws SQLException exception
     */
    long getLong(int columnIndex) throws SQLException;

    /**
     * getLong
     *
     * @param columnLabel String
     * @return long
     * @throws SQLException exception
     */
    long getLong(String columnLabel) throws SQLException;

    /**
     * getFloat
     *
     * @param columnLabel String
     * @return float
     * @throws SQLException exception
     */
    float getFloat(String columnLabel) throws SQLException;

    /**
     * getDouble
     *
     * @param columnLabel String
     * @return double
     * @throws SQLException exception
     */
    double getDouble(String columnLabel) throws SQLException;

    /**
     * getBoolean
     *
     * @param columnLabel String
     * @return boolean
     * @throws SQLException exception
     */
    boolean getBoolean(String columnLabel) throws SQLException;

    /**
     * getDate
     *
     * @param columnLabel String
     * @return Date
     * @throws SQLException exception
     */
    Date getDate(String columnLabel) throws SQLException;

    /**
     * getObject
     *
     * @param columnLabel String
     * @return Object
     * @throws SQLException exception
     */
    Object getObject(String columnLabel) throws SQLException;
}
