package com.ajja.service.bootstrap.common.application.security.repository;

import com.ajja.service.bootstrap.common.application.util.HttpRequestUtils;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.RedirectUrlResponse;
import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;
import com.ajja.service.bootstrap.common.business.security.entity.type.Role;
import com.ajja.service.bootstrap.common.business.security.repository.IOAuth2FacebookProviderRepository;
import com.ajja.service.bootstrap.common.business.util.PasswordUtils;
import org.json.JSONObject;

import java.util.Set;

/**
 * OAuth2FacebookProviderRepositoryImpl
 */
public class OAuth2FacebookProviderRepositoryImpl implements IOAuth2FacebookProviderRepository {

    private String clientId;

    private String clientSecret;

    private String redirectUri;

    private String authorizationUri;

    private String tokenUri;

    private String userInfoUri;

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public void setAuthorizationUri(String authorizationUri) {
        this.authorizationUri = authorizationUri;
    }

    public void setTokenUri(String tokenUri) {
        this.tokenUri = tokenUri;
    }

    public void setUserInfoUri(String userInfoUri) {
        this.userInfoUri = userInfoUri;
    }

    @Override
    public RedirectUrlResponse getRedirectUrl(String state) {
        final String url = authorizationUri + "?" +
            "client_id=" + clientId +
            "&redirect_uri=" + redirectUri +
            "&state=" + state;

        final RedirectUrlResponse redirectUrlResponse = new RedirectUrlResponse();
        redirectUrlResponse.setRedirectUrl(url);

        return redirectUrlResponse;
    }

    @Override
    public User getUser(String code) {
        final String accessToken = getAccessToken(code);

        final String url = userInfoUri +
            "&access_token=" + accessToken;

        final JSONObject result = HttpRequestUtils.doGetJSON(url);
        final String id = result.getString("id");
        final String name = result.getString("name");
        final String email = result.has("email") ? result.getString("email") : "";

        final User user = new User();
        user.setName(name);
        user.setEmail(email);
        user.setEmailVerified(!email.isEmpty());
        user.setPassword(PasswordUtils.generate(8));
        user.setProvider(Provider.PROVIDER_FACEBOOK);
        user.setProviderId(id);
        user.setRoles(Set.of(Role.ROLE_USER));

        return user;
    }

    /**
     * getAccessToken
     *
     * @param code String
     * @return String
     */
    private String getAccessToken(final String code) {
        final String url = tokenUri + "?" +
            "client_id=" + clientId +
            "&client_secret=" + clientSecret +
            "&redirect_uri=" + redirectUri +
            "&code=" + code;

        final JSONObject result = HttpRequestUtils.doGetJSON(url);
        final String accessToken = result.getString("access_token");

        return accessToken;
    }
}
