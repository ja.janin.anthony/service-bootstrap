package com.ajja.service.bootstrap.common.application.security.repository;

import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.type.Role;
import com.ajja.service.bootstrap.common.business.security.repository.IJWTRepository;
import com.ajja.service.bootstrap.common.business.service.AbstractService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;

/**
 * JWTRepositoryImpl
 */
public class JWTRepositoryImpl extends AbstractService implements IJWTRepository {

    private final Logger logger = Logger.getLogger(JWTRepositoryImpl.class);

    private String secret;

    private long accessTokenDuration;

    private long refreshTokenDuration;

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public void setAccessTokenDuration(long accessTokenDuration) {
        this.accessTokenDuration = accessTokenDuration;
    }

    public void setRefreshTokenDuration(long refreshTokenDuration) {
        this.refreshTokenDuration = refreshTokenDuration;
    }

    @Override
    public String createAccessToken(String clientId, User user, String issuer, Date issuedAt, UUID userUUID) {
        final Map<String, Object> claims = new HashMap<>();
        claims.put("user-uuid", userUUID);
        claims.put("scopes", user.getRoles());

        final Date expiration = new Date(issuedAt.getTime() + accessTokenDuration * 1000);
        return createToken(clientId, user, claims, issuer, issuedAt, expiration, secret);
    }

    @Override
    public String createRefreshToken(String clientId, User user, String issuer, Date issuedAt) {
        final Map<String, Object> claims = new HashMap<>();
        claims.put("jti", UUID.randomUUID());
        claims.put("scopes", Set.of(Role.ROLE_REFRESH_TOKEN));

        final Date expiration = new Date(issuedAt.getTime() + refreshTokenDuration * 1000);
        return createToken(clientId, user, claims, issuer, issuedAt, expiration, secret);
    }

    @Override
    public boolean isRefreshToken(String token) {
        try {
            final Claims claims = getClaims(token, secret);
            if (claims == null) {
                return false;
            }

            final List<String> scopes = claims.get("scopes", List.class);
            final Set<Role> roles = scopes.stream()
                .map(role -> Role.valueOf(role))
                .collect(Collectors.toSet());

            if (roles.size() != 1) {
                return false;
            }
            final Role role = roles.iterator().next();

            return Role.ROLE_REFRESH_TOKEN.equals(role);
        } catch (Exception e) {
            logger.error(e);
        }

        return false;
    }

    @Override
    public User getUser(String token) {
        try {
            final Claims claims = getClaims(token, secret);
            if (claims == null) {
                return null;
            }

            final String subject = claims.getSubject();
            final String[] args = subject.split(":");
            final Long id = Long.parseLong(args[1]);
            final String email = args[2];

            final List<String> scopes = claims.get("scopes", List.class);
            final Set<Role> roles = scopes.stream()
                .map(role -> Role.valueOf(role))
                .collect(Collectors.toSet());

            final User user = new User();
            user.setId(id);
            user.setEmail(email);
            user.setRoles(roles);

            return user;
        } catch (Exception e) {
            logger.error(e);
        }

        return null;
    }

    /**
     * createToken
     *
     * @param clientId String
     * @param user User
     * @param claims Map<String, Object>
     * @param issuer String
     * @param issuedAt Date
     * @param expiration Date
     * @param secret String
     * @return String
     */
    private String createToken(final String clientId, final User user, final Map<String, Object> claims, final String issuer, final Date issuedAt, final Date expiration, final String secret) {
        return Jwts.builder()
            .setClaims(claims)
            .setSubject(String.format("%s:%d:%s", clientId, user.getId(), !user.getEmail().isEmpty() ? user.getEmail() : "anonymous"))
            .setIssuer(issuer)
            .setIssuedAt(issuedAt)
            .setExpiration(expiration)
            .signWith(SignatureAlgorithm.HS512, Base64.getEncoder().encodeToString(secret.getBytes()))
            .compact();
    }

    /**
     * getClaims
     *
     * @param token String
     * @param secret String
     * @return Claims
     */
    private Claims getClaims(final String token, final String secret) {
        try {
            final Claims claims = Jwts.parser()
                .setSigningKey(Base64.getEncoder().encodeToString(secret.getBytes()))
                .parseClaimsJws(token)
                .getBody();

            final Date expirationDateFromToken = claims.getExpiration();
            if (expirationDateFromToken.before(new Date())) {
                return null;
            }

            return claims;
        } catch (Exception e) {
            logger.error(e);
        }

        return null;
    }
}
