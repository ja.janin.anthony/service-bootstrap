package com.ajja.service.bootstrap.common.application.controller.factory;

import com.ajja.service.bootstrap.common.application.controller.response.IResponse;
import com.ajja.service.bootstrap.common.application.controller.response.ResponseImpl;

import java.net.URI;

/**
 * ResponseFactory
 */
public final class ResponseFactory {

    /**
     * ResponseFactory
     */
    private ResponseFactory() {}

    /**
     * ok
     *
     * @return IResponse
     */
    public static IResponse ok() {
        return new ResponseImpl(200);
    }

    /**
     * created
     *
     * @param location URI
     * @return IResponse
     */
    public static IResponse created(final URI location) {
        final IResponse response = new ResponseImpl(201);
        response.setLocation(location);
        return response;
    }

    /**
     * noContent
     *
     * @return IResponse
     */
    public static IResponse noContent() {
        return new ResponseImpl(204);
    }

    /**
     * notFound
     *
     * @return IResponse
     */
    public static IResponse notFound() {
        return new ResponseImpl(404);
    }
}
