package com.ajja.service.bootstrap.common.application.sql;

import com.ajja.service.bootstrap.common.application.sql.exception.SQLException;
import com.ajja.service.bootstrap.common.application.util.PreparedStatementUtils;
import com.ajja.service.bootstrap.common.application.util.ResultSetUtils;
import com.ajja.service.bootstrap.common.business.entity.AbstractEntity;
import com.ajja.service.bootstrap.common.business.entity.AbstractEntityAuditable;
import com.ajja.service.bootstrap.common.business.entity.IEntity;
import com.ajja.service.bootstrap.core.utils.ReflectionUtils;
import com.ajja.service.bootstrap.core.utils.StringUtils;
import org.apache.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * SQLQueryFactoryImpl
 */
public final class SQLQueryFactoryImpl implements ISQLQueryFactory {

    private final Logger logger = Logger.getLogger(SQLQueryFactoryImpl.class);

    private final ISQLConnection sqlConnection;

    private List<Object> params = new ArrayList<>();

    private String whereClause;

    /**
     * SQLQueryFactoryImpl
     *
     * @param sqlConnection ISQLConnection
     */
    public SQLQueryFactoryImpl(final ISQLConnection sqlConnection) {
        this.sqlConnection = sqlConnection;
    }

    @Override
    public <T extends IEntity> T insert(T entity) {
        if (entity instanceof AbstractEntityAuditable) {
            final Date now = new Date();

            ((AbstractEntityAuditable) entity).setCreationDate(now);
            ((AbstractEntityAuditable) entity).setUpdatingDate(now);
            ((AbstractEntityAuditable) entity).setVersion(1L);
        }

        final Class<?> entityClass = entity.getClass();

        final Collection<String> columns = ReflectionUtils.getColumns(entityClass);
        columns.removeIf(column -> column.equals("id"));

        final String sql = String.format("INSERT INTO %s (%s) VALUES (%s)", StringUtils.toSnakeCase(entityClass.getSimpleName()), columns.stream().collect(Collectors.joining(", ")), columns.stream().map(column -> "?").collect(Collectors.joining(", ")));
        try (final IPreparedStatement preparedStatement = sqlConnection.prepareStatement(sql, IPreparedStatement.RETURN_GENERATED_KEYS)) {
            int index = 1;

            for (final String column : columns) {
                PreparedStatementUtils.populate(preparedStatement, entity, column, index++);
            }

            preparedStatement.execute();

            final IResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                final Long id = resultSet.getLong(1);
                if (id == null) {
                    return null;
                }

                if (entity instanceof AbstractEntity) {
                    ((AbstractEntity) entity).setId(id);
                }

                return entity;
            }
        } catch (SQLException e) {
            logger.error(e);
        }

        params.clear();
        return null;
    }

    @Override
    public <T extends IEntity> List<T> select(String sql, Class<T> entityClass) {
        try (final IPreparedStatement preparedStatement = sqlConnection.prepareStatement(sql)) {
            final int numParams = params.size();
            if (numParams > 0) {
                int index = 1;
                for (final Object param : params) {
                    PreparedStatementUtils.setValue(preparedStatement, param.getClass(), index++, param);
                }
                params.clear();
            }

            final IResultSet resultSet = preparedStatement.executeQuery();
            return extractData(entityClass, resultSet);
        } catch (SQLException e) {
            logger.error(e);
        }

        params.clear();
        return null;
    }

    @Override
    public <T extends IEntity> List<T> select(Class<T> entityClass) {
        final Collection<String> columns = ReflectionUtils.getColumns(entityClass);

        final String sql = String.format("SELECT %s FROM %s", columns.stream().collect(Collectors.joining(", ")), StringUtils.toSnakeCase(entityClass.getSimpleName())) + getWhereClause();
        try (final IPreparedStatement preparedStatement = sqlConnection.prepareStatement(sql)) {
            final int numParams = params.size();
            if (numParams > 0) {
                int index = 1;
                for (final Object param : params) {
                    PreparedStatementUtils.setValue(preparedStatement, param.getClass(), index++, param);
                }
                params.clear();
            }

            final IResultSet resultSet = preparedStatement.executeQuery();
            return extractData(entityClass, resultSet);
        } catch (SQLException e) {
            logger.error(e);
        }

        params.clear();
        return null;
    }

    @Override
    public <T extends IEntity> T selectOne(Class<T> entityClass) {
        final List<T> entities = select(entityClass);
        if (entities != null && entities.size() > 0) {
            return entities.get(0);
        }

        return null;
    }

    @Override
    public <T extends IEntity> Integer update(T entity) {
        if (entity instanceof AbstractEntityAuditable) {
            final Date now = new Date();

            ((AbstractEntityAuditable) entity).setUpdatingDate(now);
            ((AbstractEntityAuditable) entity).setVersion(((AbstractEntityAuditable) entity).getVersion() + 1L);
        }

        final Class<?> entityClass = entity.getClass();

        final Collection<String> columns = ReflectionUtils.getColumns(entityClass);
        columns.removeIf(column -> column.equals("id"));

        final String sql = String.format("UPDATE %s SET %s", StringUtils.toSnakeCase(entityClass.getSimpleName()), columns.stream().map(column -> String.format("%s = ?", column)).collect(Collectors.joining(", "))) + getWhereClause();
        try (final IPreparedStatement preparedStatement = sqlConnection.prepareStatement(sql)) {
            int index = 1;

            for (final String column : columns) {
                PreparedStatementUtils.populate(preparedStatement, entity, column, index++);
            }

            final int numParams = params.size();
            if (numParams > 0) {
                for (final Object param : params) {
                    PreparedStatementUtils.setValue(preparedStatement, param.getClass(), index++, param);
                }
                params.clear();
            }

            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        }

        params.clear();
        return null;
    }

    @Override
    public <T extends IEntity> Integer delete(Class<T> entityClass) {
        final String sql = String.format("DELETE FROM %s", StringUtils.toSnakeCase(entityClass.getSimpleName())) + getWhereClause();
        try (final IPreparedStatement preparedStatement = sqlConnection.prepareStatement(sql)) {
            final int numParams = params.size();
            if (numParams > 0) {
                int index = 1;
                for (final Object param : params) {
                    PreparedStatementUtils.setValue(preparedStatement, param.getClass(), index++, param);
                }
                params.clear();
            }

            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
        }

        params.clear();
        return null;
    }

    /**
     * extractData
     *
     * @param entityClass Class<T>
     * @param resultSet IResultSet
     * @param <T>
     * @return T
     * @throws SQLException exception
     */
    private <T extends IEntity> List<T> extractData(final Class<T> entityClass, final IResultSet resultSet) throws SQLException {
        final List<T> entities = new ArrayList<>();

        final List<String> columns = ResultSetUtils.getColumns(resultSet);
        final int numColumns = columns.size();

        while (resultSet.next()) {
            try {
                final T entity = entityClass.getConstructor().newInstance();
                if (numColumns > 0) {
                    for (final String column : columns) {
                        ResultSetUtils.populate(resultSet, entity, column);
                    }
                }

                entities.add(entity);
            } catch (InstantiationException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                logger.error(e);
            }
        }

        return entities;
    }

    @Override
    public ISQLQueryFactory where(String clause) {
        whereClause = String.format(" WHERE %s", clause);
        return this;
    }

    @Override
    public ISQLQueryFactory setParam(Object value) {
        params.add(value);
        return this;
    }

    /**
     * getWhereClause
     *
     * @return String
     */
    private String getWhereClause() {
        return (whereClause == null || whereClause.isEmpty()) ? "" : whereClause;
    }
}
