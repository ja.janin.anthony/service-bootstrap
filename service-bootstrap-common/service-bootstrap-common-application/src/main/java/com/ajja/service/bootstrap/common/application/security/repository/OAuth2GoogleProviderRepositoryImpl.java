package com.ajja.service.bootstrap.common.application.security.repository;

import com.ajja.service.bootstrap.common.application.util.HttpRequestUtils;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.RedirectUrlResponse;
import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;
import com.ajja.service.bootstrap.common.business.security.entity.type.Role;
import com.ajja.service.bootstrap.common.business.security.repository.IOAuth2GoogleProviderRepository;
import com.ajja.service.bootstrap.common.business.util.PasswordUtils;
import org.json.JSONObject;

import java.util.Set;

/**
 * OAuth2GoogleProviderRepositoryImpl
 */
public class OAuth2GoogleProviderRepositoryImpl implements IOAuth2GoogleProviderRepository {

    private String clientId;

    private String clientSecret;

    private String redirectUri;

    private String authorizationUri;

    private String tokenUri;

    private String userInfoUri;

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public void setAuthorizationUri(String authorizationUri) {
        this.authorizationUri = authorizationUri;
    }

    public void setTokenUri(String tokenUri) {
        this.tokenUri = tokenUri;
    }

    public void setUserInfoUri(String userInfoUri) {
        this.userInfoUri = userInfoUri;
    }

    @Override
    public RedirectUrlResponse getRedirectUrl(String state) {
        final String url = authorizationUri + "?" +
            "response_type=code" +
            "&scope=openid%20profile%20email" +
            "&client_id=" + clientId +
            "&redirect_uri=" + redirectUri +
            "&state=" + state;

        final RedirectUrlResponse redirectUrlResponse = new RedirectUrlResponse();
        redirectUrlResponse.setRedirectUrl(url);

        return redirectUrlResponse;
    }

    @Override
    public User getUser(String code) {
        final String accessToken = getAccessToken(code);

        final JSONObject result = HttpRequestUtils.doGetJSON(userInfoUri, accessToken);
        final String id = result.getString("sub");
        final String name = result.getString("name");
        final String email = result.has("email") ? result.getString("email") : "";

        final User user = new User();
        user.setName(name);
        user.setEmail(email);
        user.setEmailVerified(!email.isEmpty());
        user.setPassword(PasswordUtils.generate(8));
        user.setProvider(Provider.PROVIDER_GOOGLE);
        user.setProviderId(id);
        user.setRoles(Set.of(Role.ROLE_USER));

        return user;
    }

    /**
     * getAccessToken
     *
     * @param code String
     * @return String
     */
    private String getAccessToken(final String code) {
        final JSONObject body = new JSONObject();
        body.put("grant_type", "authorization_code");
        body.put("client_id", clientId);
        body.put("client_secret", clientSecret);
        body.put("redirect_uri", redirectUri);
        body.put("code", code);

        final JSONObject result = HttpRequestUtils.doPostJSON(tokenUri, body);
        final String accessToken = result.getString("access_token");

        return accessToken;
    }
}
