package com.ajja.service.bootstrap.common.application.controller.response;

import java.net.URI;

/**
 * IResponse
 */
public interface IResponse {

    /**
     * getStatus
     *
     * @return int
     */
    int getStatus();

    /**
     * getLocation
     *
     * @return URI
     */
    URI getLocation();

    /**
     * setLocation
     *
     * @param location URI
     */
    void setLocation(URI location);
}
