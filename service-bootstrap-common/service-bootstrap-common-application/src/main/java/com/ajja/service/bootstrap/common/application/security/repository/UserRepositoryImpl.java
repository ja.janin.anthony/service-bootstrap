package com.ajja.service.bootstrap.common.application.security.repository;

import com.ajja.service.bootstrap.common.application.sql.AbstractSQLRepository;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;
import com.ajja.service.bootstrap.common.business.security.repository.IUserRepository;

/**
 * UserRepositoryImpl
 */
public final class UserRepositoryImpl extends AbstractSQLRepository implements IUserRepository {

    @Override
    public User create(User user) {
        return getSQLQueryFactory()
            .insert(user);
    }

    @Override
    public User findById(Long id) {
        return getSQLQueryFactory()
            .where("id = ?")
            .setParam(id)
            .selectOne(User.class);
    }

    @Override
    public User findByProvider(Provider provider, String providerId) {
        return getSQLQueryFactory()
            .where("provider = ? AND provider_id = ?")
            .setParam(provider.name())
            .setParam(providerId)
            .selectOne(User.class);
    }
}
