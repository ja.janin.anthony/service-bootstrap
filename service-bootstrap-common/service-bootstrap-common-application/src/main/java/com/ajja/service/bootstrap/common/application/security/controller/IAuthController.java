package com.ajja.service.bootstrap.common.application.security.controller;

import com.ajja.service.bootstrap.common.application.annotation.Controller;
import com.ajja.service.bootstrap.common.application.controller.IController;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.AuthorizationCodeRequest;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.RedirectUrlResponse;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.UserLoginInfoRequest;
import com.ajja.service.bootstrap.common.business.security.entity.token.AccessTokenResponse;
import com.ajja.service.bootstrap.common.business.security.entity.token.ResfreshTokenRequest;

@Controller
public interface IAuthController<Request, Response> extends IController<Request, Response> {

	/**
	 * facebookRedirectUrl
	 *
	 * @param request Request
	 * @return RedirectUrlResponse
	 */
	RedirectUrlResponse facebookRedirectUrl(Request request);

	/**
	 * facebookToken
	 *
	 * @param authorizationCodeRequest AuthorizationCodeRequest
	 * @param request Request
	 * @return AccessTokenResponse
	 */
	AccessTokenResponse facebookToken(AuthorizationCodeRequest authorizationCodeRequest, Request request);

	/**
	 * facebookUser
	 *
	 * @param userLoginInfoRequest UserLoginInfoRequest
	 * @param request Request
	 * @return AccessTokenResponse
	 */
	AccessTokenResponse facebookUser(UserLoginInfoRequest userLoginInfoRequest, Request request);

	/**
	 * googleRedirectUrl
	 *
	 * @param request Request
	 * @return RedirectUrlResponse
	 */
	RedirectUrlResponse googleRedirectUrl(Request request);

	/**
	 * googleToken
	 *
	 * @param authorizationCodeRequest AuthorizationCodeRequest
	 * @param request Request
	 * @return AccessTokenResponse
	 */
	AccessTokenResponse googleToken(AuthorizationCodeRequest authorizationCodeRequest, Request request);

	/**
	 * googleUser
	 *
	 * @param userLoginInfoRequest UserLoginInfoRequest
	 * @param request Request
	 * @return AccessTokenResponse
	 */
	AccessTokenResponse googleUser(UserLoginInfoRequest userLoginInfoRequest, Request request);

	/**
	 * appleRedirectUrl
	 *
	 * @param request Request
	 * @return RedirectUrlResponse
	 */
	RedirectUrlResponse appleRedirectUrl(Request request);

	/**
	 * appleToken
	 *
	 * @param authorizationCodeRequest AuthorizationCodeRequest
	 * @param request Request
	 * @return AccessTokenResponse
	 */
	AccessTokenResponse appleToken(AuthorizationCodeRequest authorizationCodeRequest, Request request);

	/**
	 * appleUser
	 *
	 * @param userLoginInfoRequest UserLoginInfoRequest
	 * @param request Request
	 * @return AccessTokenResponse
	 */
	AccessTokenResponse appleUser(UserLoginInfoRequest userLoginInfoRequest, Request request);

	/**
	 * refreshToken
	 *
	 * @param resfreshTokenRequest ResfreshTokenRequest
	 * @param request Request
	 * @return AccessTokenResponse
	 */
	AccessTokenResponse refreshToken(ResfreshTokenRequest resfreshTokenRequest, Request request);

	/**
	 * logout
	 *
	 * @return Response
	 */
	Response logout();
}
