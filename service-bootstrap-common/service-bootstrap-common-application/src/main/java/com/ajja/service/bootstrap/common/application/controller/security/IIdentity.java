package com.ajja.service.bootstrap.common.application.controller.security;

/**
 * IIdentity
 */
public interface IIdentity {

    /**
     * getUserId
     *
     * @return Long
     */
    Long getUserId();
}
