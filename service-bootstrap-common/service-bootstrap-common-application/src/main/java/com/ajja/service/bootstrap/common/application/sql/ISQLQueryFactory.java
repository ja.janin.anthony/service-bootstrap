package com.ajja.service.bootstrap.common.application.sql;

import com.ajja.service.bootstrap.common.business.entity.IEntity;

import java.util.List;

/**
 * ISQLQueryFactory
 */
public interface ISQLQueryFactory {

    /**
     * insert
     *
     * @param entity T
     * @param <T>
     * @return T
     */
    <T extends IEntity> T insert(T entity);

    /**
     * select
     *
     * @param sql String
     * @param entityClass Class<T>
     * @param <T>
     * @return List<T>
     */
    <T extends IEntity> List<T> select(String sql, Class<T> entityClass);

    /**
     * select
     *
     * @param entityClass Class<T>
     * @param <T>
     * @return List<T>
     */
    <T extends IEntity> List<T> select(Class<T> entityClass);

    /**
     * selectOne
     *
     * @param entityClass Class<T>
     * @param <T>
     * @return T
     */
    <T extends IEntity> T selectOne(Class<T> entityClass);

    /**
     * update
     *
     * @param entity T
     * @param <T>
     * @return Integer
     */
    <T extends IEntity> Integer update(T entity);

    /**
     * delete
     *
     * @param entityClass Class<T>
     * @param <T>
     * @return Integer
     */
    <T extends IEntity> Integer delete(Class<T> entityClass);

    /**
     * where
     *
     * @param clause String
     * @return ISQLQueryFactory
     */
    ISQLQueryFactory where(String clause);

    /**
     * setParam
     *
     * @param value Object
     * @return ISQLQueryFactory
     */
    ISQLQueryFactory setParam(Object value);
}
