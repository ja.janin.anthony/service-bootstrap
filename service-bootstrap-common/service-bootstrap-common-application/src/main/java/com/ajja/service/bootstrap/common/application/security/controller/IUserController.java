package com.ajja.service.bootstrap.common.application.security.controller;

import com.ajja.service.bootstrap.common.application.annotation.Controller;
import com.ajja.service.bootstrap.common.application.controller.IController;
import com.ajja.service.bootstrap.common.business.security.entity.User;

@Controller
public interface IUserController<Request, Response> extends IController<Request, Response> {

	/**
     * me
	 *
	 * @param request Request
	 * @return User
	 */
    User me(Request request);
}
