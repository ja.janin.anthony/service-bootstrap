package com.ajja.service.bootstrap.common.application.controller.security;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.util.Base64;

/**
 * IdentityImpl
 */
public final class IdentityImpl implements IIdentity {

    private final Logger logger = Logger.getLogger(IdentityImpl.class);

    private String token;

    /**
     * IdentityImpl
     *
     * @param token String
     */
    public IdentityImpl(final String token) {
        this.token = token;
    }

    @Override
    public Long getUserId() {
        if (token == null || token.isEmpty()) {
            return 0L;
        }

        final String[] args = token.split("\\.");
        if (args.length != 3) {
            return 0L;
        }

        try {
            final String payload = new String(Base64.getDecoder().decode(args[1]));

            final JSONObject jsonObject = new JSONObject(payload);
            final String subject = jsonObject.getString("sub");

            return Long.parseLong(subject.split(":")[1]);
        } catch (Exception e) {
            logger.error(e);
        }

        return 0L;
    }
}
