package com.ajja.service.bootstrap.common.application.controller.builder;

import org.apache.log4j.Logger;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * URIBuilderImpl
 */
public final class URIBuilderImpl implements IURIBuilder {

    private final Logger logger = Logger.getLogger(URIBuilderImpl.class);

    private final StringBuffer uri;

    /**
     * URIBuilderImpl
     *
     * @param uri StringBuffer
     */
    public URIBuilderImpl(final StringBuffer uri) {
        this.uri = uri;
    }

    @Override
    public IURIBuilder append(String value) {
        uri.append(value);
        return this;
    }

    @Override
    public IURIBuilder append(Long value) {
        uri.append(value);
        return this;
    }

    @Override
    public URI build() {
        try {
            return new URI(uri.toString());
        } catch (URISyntaxException e) {
            logger.error(e);
        }

        return null;
    }
}
