package com.ajja.service.bootstrap.common.application.security.controller;

import com.ajja.service.bootstrap.common.application.annotation.GetMapping;
import com.ajja.service.bootstrap.common.application.annotation.PostMapping;
import com.ajja.service.bootstrap.common.application.annotation.RequestMapping;
import com.ajja.service.bootstrap.common.application.controller.AbstractController;
import com.ajja.service.bootstrap.common.application.controller.factory.ResponseFactory;
import com.ajja.service.bootstrap.common.application.controller.request.IRequest;
import com.ajja.service.bootstrap.common.application.controller.response.IResponse;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.AuthorizationCodeRequest;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.OAuth2Client;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.RedirectUrlResponse;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.UserLoginInfoRequest;
import com.ajja.service.bootstrap.common.business.security.entity.token.AccessTokenResponse;
import com.ajja.service.bootstrap.common.business.security.entity.token.ResfreshTokenRequest;
import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;
import com.ajja.service.bootstrap.common.business.security.service.IJWTService;
import com.ajja.service.bootstrap.common.business.security.service.IOAuth2Service;
import com.ajja.service.bootstrap.common.business.security.service.IUserService;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.UUID;

@RequestMapping("/auth")
public class AuthControllerImpl extends AbstractController implements IAuthController<IRequest, IResponse> {

	private final Logger logger = Logger.getLogger(AuthControllerImpl.class);

	private long accessTokenDuration;

	public void setAccessTokenDuration(long accessTokenDuration) {
		this.accessTokenDuration = accessTokenDuration;
	}

	@GetMapping("/facebook/redirect_url")
	@Override
	public RedirectUrlResponse facebookRedirectUrl(IRequest request) {
		final String clientId = request.getParameter("client_id");
		if (clientId == null || clientId.isEmpty()) {
			logger.error("Client id is empty.");
			return null;
		}

		final IOAuth2Service oauth2Service = getService(IOAuth2Service.class);

		final OAuth2Client oauth2Client = oauth2Service.getOAuth2Client(clientId);
		if (oauth2Client == null) {
			logger.error(String.format("OAuth2 client not found for this client id : %s.", clientId));
			return null;
		}

		final String redirectUri = request.getParameter("redirect_uri");
		if (redirectUri == null || redirectUri.isEmpty()) {
			logger.error("Redirect uri is empty.");
			return null;
		}

		if (!oauth2Client.getRedirectUri().equals(redirectUri)) {
			logger.error("Redirect uri doesn't match.");
			return null;
		}

		final String state = request.getParameter("state");
		if (state == null || state.isEmpty()) {
			logger.error("State is empty.");
			return null;
		}

		final RedirectUrlResponse redirectUrl = oauth2Service.getRedirectUrl(Provider.PROVIDER_FACEBOOK, state);
		if (redirectUrl == null) {
			logger.error("Redirect url is empty.");
			return null;
		}

		return redirectUrl;
	}

	@PostMapping("/facebook/token")
	@Override
	public AccessTokenResponse facebookToken(AuthorizationCodeRequest authorizationCodeRequest, IRequest request) {
		final String issuer = request.getIssuer();
		if (issuer == null || issuer.isEmpty()) {
			logger.error("Issuer is empty.");
			return null;
		}

		final String clientId = authorizationCodeRequest.getClientId();
		if (clientId == null || clientId.isEmpty()) {
			logger.error("Client id is empty.");
			return null;
		}

		final IOAuth2Service oauth2Service = getService(IOAuth2Service.class);

		final OAuth2Client oauth2Client = oauth2Service.getOAuth2Client(clientId);
		if (oauth2Client == null) {
			logger.error(String.format("OAuth2 client not found for this client id : %s.", clientId));
			return null;
		}

		final String redirectUri = authorizationCodeRequest.getRedirectUri();
		if (redirectUri == null || redirectUri.isEmpty()) {
			logger.error("Redirect uri is empty.");
			return null;
		}

		final String code = authorizationCodeRequest.getCode();
		if (code == null || code.isEmpty()) {
			logger.error("Code is empty.");
			return null;
		}

		final User user = oauth2Service.getUser(Provider.PROVIDER_FACEBOOK, code);
		if (user == null) {
			logger.error("User not found.");
			return null;
		}

		return getAccessTokenResponse(clientId, user, issuer, accessTokenDuration);
	}

	@PostMapping("/facebook/user")
	@Override
	public AccessTokenResponse facebookUser(UserLoginInfoRequest userLoginInfoRequest, IRequest request) {
		final String issuer = request.getIssuer();
		if (issuer == null || issuer.isEmpty()) {
			logger.error("Issuer is empty.");
			return null;
		}

		final String clientId = userLoginInfoRequest.getClientId();
		if (clientId == null || clientId.isEmpty()) {
			logger.error("Client id is empty.");
			return null;
		}

		final OAuth2Client oauth2Client = getService(IOAuth2Service.class).getOAuth2Client(clientId);
		if (oauth2Client == null) {
			logger.error(String.format("OAuth2 client not found for this client id : %s.", clientId));
			return null;
		}

		final Provider provider = userLoginInfoRequest.getProvider();
		if (provider == null) {
			logger.error("Provider is empty.");
			return null;
		}

		final String providerId = userLoginInfoRequest.getProviderId();
		if (providerId == null || providerId.isEmpty()) {
			logger.error("Provider id is empty.");
			return null;
		}

		String name = userLoginInfoRequest.getName();
		if (name == null || name.isEmpty()) {
			name = "";
		}

		String email = userLoginInfoRequest.getEmail();
		if (email == null || email.isEmpty()) {
			email = "";
		}

		final User user = getService(IOAuth2Service.class).getUser(provider, providerId, name, email);
		if (user == null) {
			logger.error("User not found.");
			return null;
		}

		return getAccessTokenResponse(clientId, user, issuer, accessTokenDuration);
	}

	@GetMapping("/google/redirect_url")
	@Override
	public RedirectUrlResponse googleRedirectUrl(IRequest request) {
		final String clientId = request.getParameter("client_id");
		if (clientId == null || clientId.isEmpty()) {
			logger.error("Client id is empty.");
			return null;
		}

		final IOAuth2Service oauth2Service = getService(IOAuth2Service.class);

		final OAuth2Client oauth2Client = oauth2Service.getOAuth2Client(clientId);
		if (oauth2Client == null) {
			logger.error(String.format("OAuth2 client not found for this client id : %s.", clientId));
			return null;
		}

		final String redirectUri = request.getParameter("redirect_uri");
		if (redirectUri == null || redirectUri.isEmpty()) {
			logger.error("Redirect uri is empty.");
			return null;
		}

		if (!oauth2Client.getRedirectUri().equals(redirectUri)) {
			logger.error("Redirect uri doesn't match.");
			return null;
		}

		final String state = request.getParameter("state");
		if (state == null || state.isEmpty()) {
			logger.error("State is empty.");
			return null;
		}

		final RedirectUrlResponse redirectUrl = oauth2Service.getRedirectUrl(Provider.PROVIDER_GOOGLE, state);
		if (redirectUrl == null) {
			logger.error("Redirect url is empty.");
			return null;
		}

		return redirectUrl;
	}

	@PostMapping("/google/token")
	@Override
	public AccessTokenResponse googleToken(AuthorizationCodeRequest authorizationCodeRequest, IRequest request) {
		final String issuer = request.getIssuer();
		if (issuer == null || issuer.isEmpty()) {
			logger.error("Issuer is empty.");
			return null;
		}

		final String clientId = authorizationCodeRequest.getClientId();
		if (clientId == null || clientId.isEmpty()) {
			logger.error("Client id is empty.");
			return null;
		}

		final IOAuth2Service oauth2Service = getService(IOAuth2Service.class);

		final OAuth2Client oauth2Client = oauth2Service.getOAuth2Client(clientId);
		if (oauth2Client == null) {
			logger.error(String.format("OAuth2 client not found for this client id : %s.", clientId));
			return null;
		}

		final String redirectUri = authorizationCodeRequest.getRedirectUri();
		if (redirectUri == null || redirectUri.isEmpty()) {
			logger.error("Redirect uri is empty.");
			return null;
		}

		final String code = authorizationCodeRequest.getCode();
		if (code == null || code.isEmpty()) {
			logger.error("Code is empty.");
			return null;
		}

		final User user = oauth2Service.getUser(Provider.PROVIDER_GOOGLE, code);
		if (user == null) {
			logger.error("User not found.");
			return null;
		}

		return getAccessTokenResponse(clientId, user, issuer, accessTokenDuration);
	}

	@PostMapping("/google/user")
	@Override
	public AccessTokenResponse googleUser(UserLoginInfoRequest userLoginInfoRequest, IRequest request) {
		final String issuer = request.getIssuer();
		if (issuer == null || issuer.isEmpty()) {
			logger.error("Issuer is empty.");
			return null;
		}

		final String clientId = userLoginInfoRequest.getClientId();
		if (clientId == null || clientId.isEmpty()) {
			logger.error("Client id is empty.");
			return null;
		}

		final OAuth2Client oauth2Client = getService(IOAuth2Service.class).getOAuth2Client(clientId);
		if (oauth2Client == null) {
			logger.error(String.format("OAuth2 client not found for this client id : %s.", clientId));
			return null;
		}

		final Provider provider = userLoginInfoRequest.getProvider();
		if (provider == null) {
			logger.error("Provider is empty.");
			return null;
		}

		final String providerId = userLoginInfoRequest.getProviderId();
		if (providerId == null || providerId.isEmpty()) {
			logger.error("Provider id is empty.");
			return null;
		}

		String name = userLoginInfoRequest.getName();
		if (name == null || name.isEmpty()) {
			name = "";
		}

		String email = userLoginInfoRequest.getEmail();
		if (email == null || email.isEmpty()) {
			email = "";
		}

		final User user = getService(IOAuth2Service.class).getUser(provider, providerId, name, email);
		if (user == null) {
			logger.error("User not found.");
			return null;
		}

		return getAccessTokenResponse(clientId, user, issuer, accessTokenDuration);
	}

	@GetMapping("/apple/redirect_url")
	@Override
	public RedirectUrlResponse appleRedirectUrl(IRequest request) {
		final String clientId = request.getParameter("client_id");
		if (clientId == null || clientId.isEmpty()) {
			logger.error("Client id is empty.");
			return null;
		}

		final IOAuth2Service oauth2Service = getService(IOAuth2Service.class);

		final OAuth2Client oauth2Client = oauth2Service.getOAuth2Client(clientId);
		if (oauth2Client == null) {
			logger.error(String.format("OAuth2 client not found for this client id : %s.", clientId));
			return null;
		}

		final String redirectUri = request.getParameter("redirect_uri");
		if (redirectUri == null || redirectUri.isEmpty()) {
			logger.error("Redirect uri is empty.");
			return null;
		}

		if (!oauth2Client.getRedirectUri().equals(redirectUri)) {
			logger.error("Redirect uri doesn't match.");
			return null;
		}

		final String state = request.getParameter("state");
		if (state == null || state.isEmpty()) {
			logger.error("State is empty.");
			return null;
		}

		final RedirectUrlResponse redirectUrl = oauth2Service.getRedirectUrl(Provider.PROVIDER_APPLE, state);
		if (redirectUrl == null) {
			logger.error("Redirect url is empty.");
			return null;
		}

		return redirectUrl;
	}

	@PostMapping("/apple/token")
	@Override
	public AccessTokenResponse appleToken(AuthorizationCodeRequest authorizationCodeRequest, IRequest request) {
		final String issuer = request.getIssuer();
		if (issuer == null || issuer.isEmpty()) {
			logger.error("Issuer is empty.");
			return null;
		}

		final String clientId = authorizationCodeRequest.getClientId();
		if (clientId == null || clientId.isEmpty()) {
			logger.error("Client id is empty.");
			return null;
		}

		final IOAuth2Service oauth2Service = getService(IOAuth2Service.class);

		final OAuth2Client oauth2Client = oauth2Service.getOAuth2Client(clientId);
		if (oauth2Client == null) {
			logger.error(String.format("OAuth2 client not found for this client id : %s.", clientId));
			return null;
		}

		final String redirectUri = authorizationCodeRequest.getRedirectUri();
		if (redirectUri == null || redirectUri.isEmpty()) {
			logger.error("Redirect uri is empty.");
			return null;
		}

		final String code = authorizationCodeRequest.getCode();
		if (code == null || code.isEmpty()) {
			logger.error("Code is empty.");
			return null;
		}

		final User user = oauth2Service.getUser(Provider.PROVIDER_APPLE, code);
		if (user == null) {
			logger.error("User not found.");
			return null;
		}

		return getAccessTokenResponse(clientId, user, issuer, accessTokenDuration);
	}

	@PostMapping("/apple/user")
	@Override
	public AccessTokenResponse appleUser(UserLoginInfoRequest userLoginInfoRequest, IRequest request) {
		final String issuer = request.getIssuer();
		if (issuer == null || issuer.isEmpty()) {
			logger.error("Issuer is empty.");
			return null;
		}

		final String clientId = userLoginInfoRequest.getClientId();
		if (clientId == null || clientId.isEmpty()) {
			logger.error("Client id is empty.");
			return null;
		}

		final OAuth2Client oauth2Client = getService(IOAuth2Service.class).getOAuth2Client(clientId);
		if (oauth2Client == null) {
			logger.error(String.format("OAuth2 client not found for this client id : %s.", clientId));
			return null;
		}

		final Provider provider = userLoginInfoRequest.getProvider();
		if (provider == null) {
			logger.error("Provider is empty.");
			return null;
		}

		final String providerId = userLoginInfoRequest.getProviderId();
		if (providerId == null || providerId.isEmpty()) {
			logger.error("Provider id is empty.");
			return null;
		}

		String name = userLoginInfoRequest.getName();
		if (name == null || name.isEmpty()) {
			name = "";
		}

		String email = userLoginInfoRequest.getEmail();
		if (email == null || email.isEmpty()) {
			email = "";
		}

		final User user = getService(IOAuth2Service.class).getUser(provider, providerId, name, email);
		if (user == null) {
			logger.error("User not found.");
			return null;
		}

		return getAccessTokenResponse(clientId, user, issuer, accessTokenDuration);
	}

	@PostMapping("/refresh_token")
	@Override
	public AccessTokenResponse refreshToken(ResfreshTokenRequest resfreshTokenRequest, IRequest request) {
		final String issuer = request.getIssuer();
		if (issuer == null || issuer.isEmpty()) {
			logger.error("Issuer is empty.");
			return null;
		}

		final String clientId = resfreshTokenRequest.getClientId();
		if (clientId == null || clientId.isEmpty()) {
			logger.error("Client id is empty.");
			return null;
		}

		final OAuth2Client oauth2Client = getService(IOAuth2Service.class).getOAuth2Client(clientId);
		if (oauth2Client == null) {
			logger.error(String.format("OAuth2 client not found for this client id : %s.", clientId));
			return null;
		}

		final String refreshToken = resfreshTokenRequest.getRefreshToken();
		if (refreshToken == null || refreshToken.isEmpty()) {
			logger.error("Refresh token is empty.");
			return null;
		}

		final IJWTService jwtService = getService(IJWTService.class);

		final boolean isRefreshToken = jwtService.isRefreshToken(refreshToken);
		if (!isRefreshToken) {
			logger.error("This refresh token is not a valid refresh token.");
			return null;
		}

		final User userFromRefreshToken = jwtService.getUser(refreshToken);
		if (userFromRefreshToken == null) {
			logger.error("Refresh token doesn't contain any user.");
			return null;
		}

		final User user = getService(IUserService.class).findById(userFromRefreshToken.getId());
		if (user == null) {
			logger.error("User not found.");
			return null;
		}

		return getAccessTokenResponse(clientId, user, issuer, accessTokenDuration);
	}

	@PostMapping("/logout")
	@Override
	public IResponse logout() {
		return ResponseFactory.ok();
	}

	/**
	 * getAccessTokenResponse
	 *
	 * @param clientId String
	 * @param user User
	 * @param issuer String
	 * @param accessTokenDuration long
	 * @return AccessTokenResponse
	 */
	private AccessTokenResponse getAccessTokenResponse(final String clientId, final User user, final String issuer, final long accessTokenDuration) {
		final IJWTService jwtService = getService(IJWTService.class);

		final Date issuedAt = new Date();
		final UUID userUUID = UUID.randomUUID();

		final String accessToken = jwtService.createAccessToken(clientId, user, issuer, issuedAt, userUUID);
		final String refreshToken = jwtService.createRefreshToken(clientId, user, issuer, issuedAt);

		final AccessTokenResponse accessTokenResponse = new AccessTokenResponse();
		accessTokenResponse.setUserUUID(userUUID);
		accessTokenResponse.setAccessToken(accessToken);
		accessTokenResponse.setRefreshToken(refreshToken);
		accessTokenResponse.setAccessTokenDuration(accessTokenDuration);

		return accessTokenResponse;
	}
}
