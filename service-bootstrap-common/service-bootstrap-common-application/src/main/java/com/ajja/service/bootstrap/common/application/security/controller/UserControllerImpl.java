package com.ajja.service.bootstrap.common.application.security.controller;

import com.ajja.service.bootstrap.common.application.annotation.GetMapping;
import com.ajja.service.bootstrap.common.application.annotation.RequestMapping;
import com.ajja.service.bootstrap.common.application.controller.AbstractController;
import com.ajja.service.bootstrap.common.application.controller.request.IRequest;
import com.ajja.service.bootstrap.common.application.controller.response.IResponse;
import com.ajja.service.bootstrap.common.application.controller.security.IIdentity;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.service.IUserService;

@RequestMapping("/users")
public class UserControllerImpl extends AbstractController implements IUserController<IRequest, IResponse> {

	@GetMapping("/me")
	@Override
	public User me(IRequest request) {
		final IIdentity identity = request.getIdentity();
		if (identity == null) {
			return null;
		}

		final Long userId = identity.getUserId();
		if (userId == null) {
			return null;
		}

		final User user = this.getService(IUserService.class).findById(userId);
		if (user == null) {
			return null;
		}

		user.setPassword(null);

		return user;
	}
}
