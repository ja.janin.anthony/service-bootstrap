package com.ajja.service.bootstrap.common.application.controller.builder;

import java.net.URI;

/**
 * IURIBuilder
 */
public interface IURIBuilder {

    /**
     * append
     *
     * @param value String
     * @return IURIBuilder
     */
    IURIBuilder append(String value);

    /**
     * append
     *
     * @param value Long
     * @return IURIBuilder
     */
    IURIBuilder append(Long value);

    /**
     * build
     *
     * @return URI
     */
    URI build();
}
