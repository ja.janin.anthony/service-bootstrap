package com.ajja.service.bootstrap.common.application.controller.request;

import com.ajja.service.bootstrap.common.application.controller.builder.IURIBuilder;
import com.ajja.service.bootstrap.common.application.controller.security.IIdentity;

/**
 * IRequest
 */
public interface IRequest {

    /**
     * getCurrent
     *
     * @return IURIBuilder
     */
    IURIBuilder getCurrent();

    /**
     * getParameter
     *
     * @param name String
     * @return String
     */
    String getParameter(String name);

    /**
     * getIssuer
     *
     * @return String
     */
    String getIssuer();

    /**
     * getIdentity
     *
     * @return IIdentity
     */
    IIdentity getIdentity();
}
