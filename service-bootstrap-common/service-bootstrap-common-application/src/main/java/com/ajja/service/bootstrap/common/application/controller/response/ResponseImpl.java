package com.ajja.service.bootstrap.common.application.controller.response;

import java.net.URI;

/**
 * ResponseImpl
 */
public final class ResponseImpl implements IResponse {

    private final int status;

    private URI location;

    /**
     * ResponseImpl
     *
     * @param status int
     */
    public ResponseImpl(final int status) {
        this.status = status;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public URI getLocation() {
        return location;
    }

    @Override
    public void setLocation(final URI location) {
        this.location = location;
    }
}
