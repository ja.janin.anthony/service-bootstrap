package com.ajja.service.bootstrap.common.impl.spring.boot.security.proxy.service;

import com.ajja.service.bootstrap.common.business.repository.IRepositoryLocator;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.OAuth2Client;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.RedirectUrlResponse;
import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;
import com.ajja.service.bootstrap.common.business.security.service.IOAuth2Service;
import com.ajja.service.bootstrap.common.business.security.service.OAuth2ServiceImpl;
import com.ajja.service.bootstrap.common.business.service.IServiceLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("IOAuth2Service")
public class OAuth2ServiceProxy implements IOAuth2Service {

    private OAuth2ServiceImpl service = new OAuth2ServiceImpl();

    @Autowired
    public void setRepositoryLocator(IRepositoryLocator repositoryLocator) {
        service.setRepositoryLocator(repositoryLocator);
    }

    @Autowired
    public void setServiceLocator(IServiceLocator serviceLocator) {
        service.setServiceLocator(serviceLocator);
    }

    @Override
    public OAuth2Client getOAuth2Client(String clientId) {
        return service.getOAuth2Client(clientId);
    }

    @Override
    public RedirectUrlResponse getRedirectUrl(Provider provider, String state) {
        return service.getRedirectUrl(provider, state);
    }

    @Override
    public User getUser(Provider provider, String code) {
        return service.getUser(provider, code);
    }

    @Override
    public User getUser(Provider provider, String providerId, String name, String email) {
        return service.getUser(provider, providerId, name, email);
    }
}
