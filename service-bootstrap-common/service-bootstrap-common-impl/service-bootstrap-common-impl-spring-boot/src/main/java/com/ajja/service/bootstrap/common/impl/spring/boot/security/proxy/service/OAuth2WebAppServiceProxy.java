package com.ajja.service.bootstrap.common.impl.spring.boot.security.proxy.service;

import com.ajja.service.bootstrap.common.business.security.entity.oauth2.OAuth2Client;
import com.ajja.service.bootstrap.common.business.security.service.IOAuth2WebAppService;
import com.ajja.service.bootstrap.common.business.security.service.OAuth2WebAppServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("IOAuth2WebAppService")
public class OAuth2WebAppServiceProxy implements IOAuth2WebAppService {

    private OAuth2WebAppServiceImpl service = new OAuth2WebAppServiceImpl();

    @Autowired
    public void setClientId(@Value("${spring.security.oauth2.client.app.web.client-id:null}") String clientId) {
        service.setClientId(clientId);
    }

    @Autowired
    public void setRedirectUri(@Value("${spring.security.oauth2.client.app.web.redirect-uri:null}") String redirectUri) {
        service.setRedirectUri(redirectUri);
    }

    @Override
    public OAuth2Client getOAuth2Client() {
        return service.getOAuth2Client();
    }
}
