package com.ajja.service.bootstrap.common.impl.spring.boot.security.proxy.controller;

import com.ajja.service.bootstrap.common.application.security.controller.AuthControllerImpl;
import com.ajja.service.bootstrap.common.application.security.controller.IAuthController;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.AuthorizationCodeRequest;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.RedirectUrlResponse;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.UserLoginInfoRequest;
import com.ajja.service.bootstrap.common.business.security.entity.token.AccessTokenResponse;
import com.ajja.service.bootstrap.common.business.security.entity.token.ResfreshTokenRequest;
import com.ajja.service.bootstrap.common.business.service.IServiceLocator;
import com.ajja.service.bootstrap.common.impl.spring.boot.controller.request.RequestImpl;
import com.ajja.service.bootstrap.common.impl.spring.boot.controller.response.ResponseEntityFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;

/**
 * AbstractAuthControllerProxy
 */
public abstract class AbstractAuthControllerProxy implements IAuthController<HttpServletRequest, ResponseEntity> {

	private AuthControllerImpl controller = new AuthControllerImpl();

	@Autowired
	public void setServiceLocator(IServiceLocator serviceLocator) {
		controller.setServiceLocator(serviceLocator);
	}

	@Autowired
	public void setAccessTokenDuration(@Value("${spring.security.jwt.access-token-duration:0}") long accessTokenDuration) {
		controller.setAccessTokenDuration(accessTokenDuration);
	}

	@GetMapping("/facebook/redirect_url")
	@Override
	public RedirectUrlResponse facebookRedirectUrl(HttpServletRequest request) {
		return controller.facebookRedirectUrl(new RequestImpl(request));
	}

	@PostMapping("/facebook/token")
	@Override
	public AccessTokenResponse facebookToken(@RequestBody AuthorizationCodeRequest authorizationCodeRequest, HttpServletRequest request) {
		return controller.facebookToken(authorizationCodeRequest, new RequestImpl(request));
	}

	@PostMapping("/facebook/user")
	@Override
	public AccessTokenResponse facebookUser(@RequestBody UserLoginInfoRequest userLoginInfoRequest, HttpServletRequest request) {
		return controller.facebookUser(userLoginInfoRequest, new RequestImpl(request));
	}

	@GetMapping("/google/redirect_url")
	@Override
	public RedirectUrlResponse googleRedirectUrl(HttpServletRequest request) {
		return controller.googleRedirectUrl(new RequestImpl(request));
	}

	@PostMapping("/google/token")
	@Override
	public AccessTokenResponse googleToken(@RequestBody AuthorizationCodeRequest authorizationCodeRequest, HttpServletRequest request) {
		return controller.googleToken(authorizationCodeRequest, new RequestImpl(request));
	}

	@PostMapping("/google/user")
	@Override
	public AccessTokenResponse googleUser(@RequestBody UserLoginInfoRequest userLoginInfoRequest, HttpServletRequest request) {
		return controller.googleUser(userLoginInfoRequest, new RequestImpl(request));
	}

	@GetMapping("/apple/redirect_url")
	@Override
	public RedirectUrlResponse appleRedirectUrl(HttpServletRequest request) {
		return controller.appleRedirectUrl(new RequestImpl(request));
	}

	@PostMapping("/apple/token")
	@Override
	public AccessTokenResponse appleToken(@RequestBody AuthorizationCodeRequest authorizationCodeRequest, HttpServletRequest request) {
		return controller.appleToken(authorizationCodeRequest, new RequestImpl(request));
	}

	@PostMapping("/apple/user")
	@Override
	public AccessTokenResponse appleUser(@RequestBody UserLoginInfoRequest userLoginInfoRequest, HttpServletRequest request) {
		return controller.appleUser(userLoginInfoRequest, new RequestImpl(request));
	}

	@PostMapping("/refresh_token")
	@Override
	public AccessTokenResponse refreshToken(@RequestBody ResfreshTokenRequest resfreshTokenRequest, HttpServletRequest request) {
		return controller.refreshToken(resfreshTokenRequest, new RequestImpl(request));
	}

	@PostMapping("/logout")
	@Override
	public ResponseEntity logout() {
		return ResponseEntityFactory.build(controller.logout());
	}
}
