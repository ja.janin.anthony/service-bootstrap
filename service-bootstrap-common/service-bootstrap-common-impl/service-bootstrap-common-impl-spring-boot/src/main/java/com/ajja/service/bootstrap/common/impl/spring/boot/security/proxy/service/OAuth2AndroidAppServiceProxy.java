package com.ajja.service.bootstrap.common.impl.spring.boot.security.proxy.service;

import com.ajja.service.bootstrap.common.business.security.entity.oauth2.OAuth2Client;
import com.ajja.service.bootstrap.common.business.security.service.IOAuth2AndroidAppService;
import com.ajja.service.bootstrap.common.business.security.service.OAuth2IOSAppServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("IOAuth2AndroidAppService")
public class OAuth2AndroidAppServiceProxy implements IOAuth2AndroidAppService {

    private OAuth2IOSAppServiceImpl service = new OAuth2IOSAppServiceImpl();

    @Autowired
    public void setClientId(@Value("${spring.security.oauth2.client.app.ios.client-id:null}") String clientId) {
        service.setClientId(clientId);
    }

    @Autowired
    public void setRedirectUri(@Value("${spring.security.oauth2.client.app.ios.redirect-uri:null}") String redirectUri) {
        service.setRedirectUri(redirectUri);
    }

    @Override
    public OAuth2Client getOAuth2Client() {
        return service.getOAuth2Client();
    }
}
