package com.ajja.service.bootstrap.common.impl.spring.boot.controller.request;

import com.ajja.service.bootstrap.common.application.controller.builder.IURIBuilder;
import com.ajja.service.bootstrap.common.application.controller.builder.URIBuilderImpl;
import com.ajja.service.bootstrap.common.application.controller.request.IRequest;
import com.ajja.service.bootstrap.common.application.controller.security.IIdentity;
import com.ajja.service.bootstrap.common.application.controller.security.IdentityImpl;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * RequestImpl
 */
public final class RequestImpl implements IRequest {

    private final HttpServletRequest request;

    /**
     * RequestImpl
     *
     * @param request HttpServletRequest
     */
    public RequestImpl(final HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public IURIBuilder getCurrent() {
        return new URIBuilderImpl(request.getRequestURL());
    }

    @Override
    public String getParameter(String name) {
        return request.getParameter(name);
    }

    @Override
    public String getIssuer() {
        return request.getHeader("Origin");
    }

    @Override
    public IIdentity getIdentity() {
        final String authorization = request.getHeader("Authorization");
        if (StringUtils.isEmpty(authorization) || !authorization.startsWith("Bearer ")) {
            return null;
        }

        final String token = authorization.substring("Bearer ".length());
        if (StringUtils.isEmpty(token)) {
            return null;
        }

        return new IdentityImpl(token);
    }
}
