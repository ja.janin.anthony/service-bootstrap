package com.ajja.service.bootstrap.common.impl.spring.boot.security.proxy.controller;

import com.ajja.service.bootstrap.common.application.security.controller.IUserController;
import com.ajja.service.bootstrap.common.application.security.controller.UserControllerImpl;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.service.IServiceLocator;
import com.ajja.service.bootstrap.common.impl.spring.boot.controller.request.RequestImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * AbstractUserControllerProxy
 */
public abstract class AbstractUserControllerProxy implements IUserController<HttpServletRequest, ResponseEntity> {

	private UserControllerImpl controller = new UserControllerImpl();

	@Autowired
	public void setServiceLocator(IServiceLocator serviceLocator) {
		controller.setServiceLocator(serviceLocator);
	}

	@GetMapping("/me")
	@Override
	public User me(HttpServletRequest request) {
		return controller.me(new RequestImpl(request));
	}
}
