package com.ajja.service.bootstrap.common.impl.spring.boot.security.proxy.service;

import com.ajja.service.bootstrap.common.business.security.entity.oauth2.OAuth2Client;
import com.ajja.service.bootstrap.common.business.security.service.IOAuth2AndroidAppService;
import com.ajja.service.bootstrap.common.business.security.service.IOAuth2IOSAppService;
import com.ajja.service.bootstrap.common.business.security.service.OAuth2AndroidAppServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("IOAuth2IOSAppService")
public class OAuth2IOSAppServiceProxy implements IOAuth2IOSAppService {

    private OAuth2AndroidAppServiceImpl service = new OAuth2AndroidAppServiceImpl();

    @Autowired
    public void setClientId(@Value("${spring.security.oauth2.client.app.android.client-id:null}") String clientId) {
        service.setClientId(clientId);
    }

    @Autowired
    public void setRedirectUri(@Value("${spring.security.oauth2.client.app.android.redirect-uri:null}") String redirectUri) {
        service.setRedirectUri(redirectUri);
    }

    @Override
    public OAuth2Client getOAuth2Client() {
        return service.getOAuth2Client();
    }
}
