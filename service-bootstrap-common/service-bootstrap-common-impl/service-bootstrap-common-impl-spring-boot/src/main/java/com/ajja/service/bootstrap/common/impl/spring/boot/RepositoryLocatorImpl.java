package com.ajja.service.bootstrap.common.impl.spring.boot;

import com.ajja.service.bootstrap.common.business.repository.IRepository;
import com.ajja.service.bootstrap.common.business.repository.IRepositoryLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Component
public class RepositoryLocatorImpl implements IRepositoryLocator {

    private static final Map<String, IRepository> CACHE = new HashMap<>();

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public <T extends IRepository> T get(Class<T> aClass) {
        final String simpleName = aClass.getSimpleName();
        if (simpleName.charAt(0) != 'I') {
            throw new RuntimeException(String.format("%s must begin by letter 'I'.", simpleName));
        }

        IRepository repository = CACHE.get(simpleName);
        if (repository == null) {
            final String packageName = aClass.getPackageName();

            final Class<IRepository> repositoryProxyClass = getRepositoryProxyClass(packageName, simpleName);
            final Repository repositoryAnnotation = repositoryProxyClass.getAnnotation(Repository.class);

            if (repositoryAnnotation == null || !repositoryAnnotation.value().equals(simpleName)) {
                throw new RuntimeException(String.format("%s must have a proxy with 'Repository' annotation.", simpleName));
            }

            repository = applicationContext.getBean(repositoryProxyClass);
            CACHE.put(simpleName, repository);
        }

        return (T) repository;
    }

    /**
     * getRepositoryProxyClass
     *
     * @param packageName String
     * @param simpleName String
     * @return Class<IRepository>
     */
    private Class<IRepository> getRepositoryProxyClass(final String packageName, final String simpleName) {
        final String proxyName = simpleName.substring(1) + "Proxy";
        final String className = (packageName.replace(".business.", ".impl.spring.boot.") + "." + proxyName).replace(".repository." + proxyName, ".proxy.repository." + proxyName);
        try {
            return (Class<IRepository>) Thread.currentThread().getContextClassLoader().loadClass(className);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(String.format("Proxy '%s' not found for this interface '%s'.", className, simpleName));
        }
    }
}
