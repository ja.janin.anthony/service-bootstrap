package com.ajja.service.bootstrap.common.impl.spring.boot.security.proxy.repository;

import com.ajja.service.bootstrap.common.application.security.repository.JWTRepositoryImpl;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.repository.IJWTRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.UUID;

@Repository("IJWTRepository")
public class JWTRepositoryProxy implements IJWTRepository {

    private JWTRepositoryImpl repository = new JWTRepositoryImpl();

    @Autowired
    public void setSecret(@Value("${spring.security.jwt.secret:null}") String secret) {
        repository.setSecret(secret);
    }

    @Autowired
    public void setAccessTokenDuration(@Value("${spring.security.jwt.access-token-duration:0}") long accessTokenDuration) {
        repository.setAccessTokenDuration(accessTokenDuration);
    }

    @Autowired
    public void setRefreshTokenDuration(@Value("${spring.security.jwt.refresh-token-duration:0}") long refreshTokenDuration) {
        repository.setRefreshTokenDuration(refreshTokenDuration);
    }

    @Override
    public String createAccessToken(String clientId, User user, String issuer, Date issuedAt, UUID userUUID) {
        return repository.createAccessToken(clientId, user, issuer, issuedAt, userUUID);
    }

    @Override
    public String createRefreshToken(String clientId, User user, String issuer, Date issuedAt) {
        return repository.createRefreshToken(clientId, user, issuer, issuedAt);
    }

    @Override
    public boolean isRefreshToken(String token) {
        return repository.isRefreshToken(token);
    }

    @Override
    public User getUser(String token) {
        return repository.getUser(token);
    }
}
