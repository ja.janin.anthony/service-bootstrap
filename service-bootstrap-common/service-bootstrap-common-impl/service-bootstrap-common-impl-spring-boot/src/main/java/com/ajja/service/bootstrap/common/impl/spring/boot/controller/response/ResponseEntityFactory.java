package com.ajja.service.bootstrap.common.impl.spring.boot.controller.response;

import com.ajja.service.bootstrap.common.application.controller.response.IResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * ResponseEntityFactory
 */
public final class ResponseEntityFactory {

    /**
     * ResponseEntityFactory
     */
    private ResponseEntityFactory() {}

    /**
     * build
     *
     * @param response IResponse
     */
    public static ResponseEntity build(final IResponse response) {
        switch (response.getStatus()) {
            case 200:
                return ResponseEntity.ok().build();

            case 201:
                return ResponseEntity.created(response.getLocation()).build();

            case 204:
                return ResponseEntity.noContent().build();

            case 404:
                return ResponseEntity.notFound().build();

            default:
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
