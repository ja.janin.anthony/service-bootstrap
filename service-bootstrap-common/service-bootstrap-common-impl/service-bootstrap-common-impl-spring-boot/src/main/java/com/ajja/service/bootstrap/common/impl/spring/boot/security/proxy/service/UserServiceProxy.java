package com.ajja.service.bootstrap.common.impl.spring.boot.security.proxy.service;

import com.ajja.service.bootstrap.common.business.repository.IRepositoryLocator;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;
import com.ajja.service.bootstrap.common.business.security.service.IUserService;
import com.ajja.service.bootstrap.common.business.security.service.UserServiceImpl;
import com.ajja.service.bootstrap.common.business.service.IServiceLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("IUserService")
public class UserServiceProxy implements IUserService {

    private UserServiceImpl service = new UserServiceImpl();

    @Autowired
    public void setRepositoryLocator(IRepositoryLocator repositoryLocator) {
        service.setRepositoryLocator(repositoryLocator);
    }

    @Autowired
    public void setServiceLocator(IServiceLocator serviceLocator) {
        service.setServiceLocator(serviceLocator);
    }

    @Override
    public User create(User user) {
        return service.create(user);
    }

    @Override
    public User findById(Long id) {
        return service.findById(id);
    }

    @Override
    public User findByProvider(Provider provider, String providerId) {
        return service.findByProvider(provider, providerId);
    }
}
