package com.ajja.service.bootstrap.common.impl.spring.boot.security.proxy.service;

import com.ajja.service.bootstrap.common.business.repository.IRepositoryLocator;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.service.IJWTService;
import com.ajja.service.bootstrap.common.business.security.service.JWTServiceImpl;
import com.ajja.service.bootstrap.common.business.service.IServiceLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service("IJWTService")
public class JWTServiceProxy implements IJWTService {

    private JWTServiceImpl service = new JWTServiceImpl();

    @Autowired
    public void setRepositoryLocator(IRepositoryLocator repositoryLocator) {
        service.setRepositoryLocator(repositoryLocator);
    }

    @Autowired
    public void setServiceLocator(IServiceLocator serviceLocator) {
        service.setServiceLocator(serviceLocator);
    }

    @Override
    public String createAccessToken(String clientId, User user, String issuer, Date issuedAt, UUID userUUID) {
        return service.createAccessToken(clientId, user, issuer, issuedAt, userUUID);
    }

    @Override
    public String createRefreshToken(String clientId, User user, String issuer, Date issuedAt) {
        return service.createRefreshToken(clientId, user, issuer, issuedAt);
    }

    @Override
    public boolean isRefreshToken(String token) {
        return service.isRefreshToken(token);
    }

    @Override
    public User getUser(String token) {
        return service.getUser(token);
    }
}
