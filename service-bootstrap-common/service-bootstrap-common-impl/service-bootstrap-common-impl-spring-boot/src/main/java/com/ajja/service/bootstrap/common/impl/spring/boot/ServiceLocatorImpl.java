package com.ajja.service.bootstrap.common.impl.spring.boot;

import com.ajja.service.bootstrap.common.business.service.IService;
import com.ajja.service.bootstrap.common.business.service.IServiceLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Component
public class ServiceLocatorImpl implements IServiceLocator {

    private static final Map<String, IService> CACHE = new HashMap<>();

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public <T extends IService> T get(Class<T> aClass) {
        final String simpleName = aClass.getSimpleName();
        if (simpleName.charAt(0) != 'I') {
            throw new RuntimeException(String.format("%s must begin by letter 'I'.", simpleName));
        }

        IService service = CACHE.get(simpleName);
        if (service == null) {
            final String packageName = aClass.getPackageName();

            final Class<IService> repositoryProxyClass = getServiceProxyClass(packageName, simpleName);
            final Service serviceAnnotation = repositoryProxyClass.getAnnotation(Service.class);

            if (serviceAnnotation == null || !serviceAnnotation.value().equals(simpleName)) {
                throw new RuntimeException(String.format("%s must have a proxy with 'Service' annotation.", simpleName));
            }

            service = applicationContext.getBean(repositoryProxyClass);
            CACHE.put(simpleName, service);
        }

        return (T) service;
    }

    /**
     * getServiceProxyClass
     *
     * @param packageName String
     * @param simpleName String
     * @return Class<IService>
     */
    private Class<IService> getServiceProxyClass(final String packageName, final String simpleName) {
        final String proxyName = simpleName.substring(1) + "Proxy";
        final String className = (packageName.replace(".business.", ".impl.spring.boot.") + "." + simpleName.substring(1) + "Proxy").replace(".service." + proxyName, ".proxy.service." + proxyName);
        try {
            return (Class<IService>) Thread.currentThread().getContextClassLoader().loadClass(className);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(String.format("Proxy '%s' not found for this interface '%s'.", className, simpleName));
        }
    }
}
