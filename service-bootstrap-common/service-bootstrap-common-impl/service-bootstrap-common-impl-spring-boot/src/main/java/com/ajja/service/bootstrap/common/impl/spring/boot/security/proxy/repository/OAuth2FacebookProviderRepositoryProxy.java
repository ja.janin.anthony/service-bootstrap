package com.ajja.service.bootstrap.common.impl.spring.boot.security.proxy.repository;

import com.ajja.service.bootstrap.common.application.security.repository.OAuth2FacebookProviderRepositoryImpl;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.RedirectUrlResponse;
import com.ajja.service.bootstrap.common.business.security.repository.IOAuth2FacebookProviderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository("IOAuth2FacebookProviderRepository")
public class OAuth2FacebookProviderRepositoryProxy implements IOAuth2FacebookProviderRepository {

    private OAuth2FacebookProviderRepositoryImpl repository = new OAuth2FacebookProviderRepositoryImpl();

    @Autowired
    public void setClientId(@Value("${spring.security.oauth2.client.registration.facebook.client-id:null}") String clientId) {
        repository.setClientId(clientId);
    }

    @Autowired
    public void setClientSecret(@Value("${spring.security.oauth2.client.registration.facebook.client-secret:null}") String clientSecret) {
        repository.setClientSecret(clientSecret);
    }

    @Autowired
    public void setRedirectUri(@Value("${spring.security.oauth2.client.registration.facebook.redirect-uri:null}") String redirectUri) {
        repository.setRedirectUri(redirectUri);
    }

    @Autowired
    public void setAuthorizationUri(@Value("${spring.security.oauth2.client.provider.facebook.authorization-uri:null}") String authorizationUri) {
        repository.setAuthorizationUri(authorizationUri);
    }

    @Autowired
    public void setTokenUri(@Value("${spring.security.oauth2.client.provider.facebook.token-uri:null}") String tokenUri) {
        repository.setTokenUri(tokenUri);
    }

    @Autowired
    public void setUserInfoUri(@Value("${spring.security.oauth2.client.provider.facebook.user-info-uri:null}") String userInfoUri) {
        repository.setUserInfoUri(userInfoUri);
    }

    @Override
    public RedirectUrlResponse getRedirectUrl(String state) {
        return repository.getRedirectUrl(state);
    }

    @Override
    public User getUser(String code) {
        return repository.getUser(code);
    }
}
