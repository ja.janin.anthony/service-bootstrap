package com.ajja.service.bootstrap.common.impl.spring.boot.security.jwt;

import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.type.Role;
import com.ajja.service.bootstrap.common.business.security.service.IJWTService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * JWTAuthorizationFilter
 */
public final class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private IJWTService jwtService;

    /**
     * JWTAuthorizationFilter
     *
     * @param authenticationManager AuthenticationManager
     * @param jwtService IJWTService
     */
    public JWTAuthorizationFilter(final AuthenticationManager authenticationManager, final IJWTService jwtService) {
        super(authenticationManager);
        this.jwtService = jwtService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        final UsernamePasswordAuthenticationToken authentication = getAuthentication(request);
        if (authentication != null) {
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        chain.doFilter(request, response);
    }

    /**
     * getAuthentication
     *
     * @param request HttpServletRequest
     * @return UsernamePasswordAuthenticationToken
     */
    private UsernamePasswordAuthenticationToken getAuthentication(final HttpServletRequest request) {
        final String authorization = request.getHeader("Authorization");
        if (StringUtils.isEmpty(authorization) || !authorization.startsWith("Bearer ")) {
            return null;
        }

        final String token = authorization.substring("Bearer ".length());
        if (StringUtils.isEmpty(token)) {
            return null;
        }

        final User user = jwtService.getUser(token);
        if (user == null) {
            return null;
        }

        final String username = user.getEmail();
        if (username == null || username.isEmpty()) {
            return null;
        }

        final Set<Role> roles = user.getRoles();
        if (roles == null || roles.isEmpty()) {
            return null;
        }

        return new UsernamePasswordAuthenticationToken(username, null, roles.stream().map(authority -> new SimpleGrantedAuthority(authority.name())).collect(Collectors.toList()));
    }
}
