package com.ajja.service.bootstrap.common.impl.quarkus.security.proxy.controller;

import com.ajja.service.bootstrap.common.application.security.controller.AuthControllerImpl;
import com.ajja.service.bootstrap.common.application.security.controller.IAuthController;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.AuthorizationCodeRequest;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.RedirectUrlResponse;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.UserLoginInfoRequest;
import com.ajja.service.bootstrap.common.business.security.entity.token.AccessTokenResponse;
import com.ajja.service.bootstrap.common.business.security.entity.token.ResfreshTokenRequest;
import com.ajja.service.bootstrap.common.business.service.IServiceLocator;
import com.ajja.service.bootstrap.common.impl.quarkus.controller.request.RequestImpl;
import com.ajja.service.bootstrap.common.impl.quarkus.controller.response.ResponseFactory;
import io.vertx.core.http.HttpServerRequest;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

/**
 * AbstractAuthControllerProxy
 */
public abstract class AbstractAuthControllerProxy implements IAuthController<HttpServerRequest, Response> {

	private AuthControllerImpl controller = new AuthControllerImpl();

	@Inject
	public void setServiceLocator(IServiceLocator serviceLocator) {
		controller.setServiceLocator(serviceLocator);
	}

	@Inject
	public void setAccessTokenDuration(@ConfigProperty(name = "quarkus.security.jwt.access-token-duration", defaultValue = "0") long accessTokenDuration) {
		controller.setAccessTokenDuration(accessTokenDuration);
	}

	@GET
	@Path("/facebook/redirect_url")
	@Override
	public RedirectUrlResponse facebookRedirectUrl(@Context HttpServerRequest request) {
		return controller.facebookRedirectUrl(new RequestImpl(request));
	}

	@POST
	@Path("/facebook/token")
	@Override
	public AccessTokenResponse facebookToken(AuthorizationCodeRequest authorizationCodeRequest, @Context HttpServerRequest request) {
		return controller.facebookToken(authorizationCodeRequest, new RequestImpl(request));
	}

	@POST
	@Path("/facebook/user")
	@Override
	public AccessTokenResponse facebookUser(UserLoginInfoRequest userLoginInfoRequest, @Context HttpServerRequest request) {
		return controller.facebookUser(userLoginInfoRequest, new RequestImpl(request));
	}

	@GET
	@Path("/google/redirect_url")
	@Override
	public RedirectUrlResponse googleRedirectUrl(@Context HttpServerRequest request) {
		return controller.googleRedirectUrl(new RequestImpl(request));
	}

	@POST
	@Path("/google/token")
	@Override
	public AccessTokenResponse googleToken(AuthorizationCodeRequest authorizationCodeRequest, @Context HttpServerRequest request) {
		return controller.googleToken(authorizationCodeRequest, new RequestImpl(request));
	}

	@POST
	@Path("/google/user")
	@Override
	public AccessTokenResponse googleUser(UserLoginInfoRequest userLoginInfoRequest, @Context HttpServerRequest request) {
		return controller.googleUser(userLoginInfoRequest, new RequestImpl(request));
	}

	@GET
	@Path("/apple/redirect_url")
	@Override
	public RedirectUrlResponse appleRedirectUrl(@Context HttpServerRequest request) {
		return controller.appleRedirectUrl(new RequestImpl(request));
	}

	@POST
	@Path("/apple/token")
	@Override
	public AccessTokenResponse appleToken(AuthorizationCodeRequest authorizationCodeRequest, @Context HttpServerRequest request) {
		return controller.appleToken(authorizationCodeRequest, new RequestImpl(request));
	}

	@POST
	@Path("/apple/user")
	@Override
	public AccessTokenResponse appleUser(UserLoginInfoRequest userLoginInfoRequest, @Context HttpServerRequest request) {
		return controller.appleUser(userLoginInfoRequest, new RequestImpl(request));
	}

	@POST
	@Path("/refresh_token")
	@Override
	public AccessTokenResponse refreshToken(ResfreshTokenRequest resfreshTokenRequest, @Context HttpServerRequest request) {
		return controller.refreshToken(resfreshTokenRequest, new RequestImpl(request));
	}

	@POST
	@Path("/logout")
	@Override
	public Response logout() {
		return ResponseFactory.build(controller.logout());
	}
}
