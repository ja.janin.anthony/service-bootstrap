package com.ajja.service.bootstrap.common.impl.quarkus.security.filter;

import javax.ws.rs.container.ContainerRequestContext;

/**
 * IFilter
 */
public interface IFilter {

    /**
     * doFilter
     *
     * @param context ContainerRequestContext
     */
    void doFilter(ContainerRequestContext context);
}
