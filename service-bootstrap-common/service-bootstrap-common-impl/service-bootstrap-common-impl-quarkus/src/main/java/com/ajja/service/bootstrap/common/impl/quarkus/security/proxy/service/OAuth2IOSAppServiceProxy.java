package com.ajja.service.bootstrap.common.impl.quarkus.security.proxy.service;

import com.ajja.service.bootstrap.common.business.security.entity.oauth2.OAuth2Client;
import com.ajja.service.bootstrap.common.business.security.service.IOAuth2IOSAppService;
import com.ajja.service.bootstrap.common.business.security.service.OAuth2IOSAppServiceImpl;
import io.quarkus.arc.Unremovable;
import io.quarkus.runtime.annotations.RegisterForReflection;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
@Unremovable
@RegisterForReflection
@Named("IOAuth2IOSAppService")
public class OAuth2IOSAppServiceProxy implements IOAuth2IOSAppService {

    private OAuth2IOSAppServiceImpl service = new OAuth2IOSAppServiceImpl();

    @Inject
    public void setClientId(@ConfigProperty(name = "quarkus.security.oauth2.client.app.ios.client-id", defaultValue = "") String clientId) {
        service.setClientId(clientId);
    }

    @Inject
    public void setRedirectUri(@ConfigProperty(name = "quarkus.security.oauth2.client.app.ios.redirect-uri", defaultValue = "") String redirectUri) {
        service.setRedirectUri(redirectUri);
    }

    @Override
    public OAuth2Client getOAuth2Client() {
        return service.getOAuth2Client();
    }
}
