package com.ajja.service.bootstrap.common.impl.quarkus.security.proxy.repository;

import com.ajja.service.bootstrap.common.application.security.repository.UserRepositoryImpl;
import com.ajja.service.bootstrap.common.application.sql.ISQLConnection;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;
import com.ajja.service.bootstrap.common.business.security.repository.IUserRepository;
import io.quarkus.arc.Unremovable;
import io.quarkus.runtime.annotations.RegisterForReflection;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
@Unremovable
@RegisterForReflection
@Named("IUserRepository")
public class UserRepositoryProxy implements IUserRepository {

    private UserRepositoryImpl repository = new UserRepositoryImpl();

    @Inject
    public void setSqlConnection(ISQLConnection sqlConnection) {
        repository.setSQLConnection(sqlConnection);
    }

    @Override
    public User create(User user) {
        return repository.create(user);
    }

    @Override
    public User findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public User findByProvider(Provider provider, String providerId) {
        return repository.findByProvider(provider, providerId);
    }
}
