package com.ajja.service.bootstrap.common.impl.quarkus.security.proxy.service;

import com.ajja.service.bootstrap.common.business.repository.IRepositoryLocator;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;
import com.ajja.service.bootstrap.common.business.security.service.IUserService;
import com.ajja.service.bootstrap.common.business.security.service.UserServiceImpl;
import com.ajja.service.bootstrap.common.business.service.IServiceLocator;
import io.quarkus.arc.Unremovable;
import io.quarkus.runtime.annotations.RegisterForReflection;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
@Unremovable
@RegisterForReflection
@Named("IUserService")
public class UserServiceProxy implements IUserService {

    private UserServiceImpl service = new UserServiceImpl();

    @Inject
    public void setRepositoryLocator(IRepositoryLocator repositoryLocator) {
        service.setRepositoryLocator(repositoryLocator);
    }

    @Inject
    public void setServiceLocator(IServiceLocator serviceLocator) {
        service.setServiceLocator(serviceLocator);
    }

    @Override
    public User create(User user) {
        return service.create(user);
    }

    @Override
    public User findById(Long id) {
        return service.findById(id);
    }

    @Override
    public User findByProvider(Provider provider, String providerId) {
        return service.findByProvider(provider, providerId);
    }
}
