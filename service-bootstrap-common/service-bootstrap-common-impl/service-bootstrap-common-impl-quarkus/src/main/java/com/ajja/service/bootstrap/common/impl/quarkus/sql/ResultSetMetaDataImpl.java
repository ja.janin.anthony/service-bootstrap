package com.ajja.service.bootstrap.common.impl.quarkus.sql;

import com.ajja.service.bootstrap.common.application.sql.IResultSetMetaData;
import com.ajja.service.bootstrap.common.application.sql.exception.SQLException;

import java.sql.ResultSetMetaData;

/**
 * ResultSetMetaDataImpl
 */
public final class ResultSetMetaDataImpl implements IResultSetMetaData {

    private final ResultSetMetaData resultSetMetaData;

    /**
     * ResultSetMetaDataImpl
     *
     * @param resultSetMetaData ResultSetMetaData
     */
    public ResultSetMetaDataImpl(final ResultSetMetaData resultSetMetaData) {
        this.resultSetMetaData = resultSetMetaData;
    }

    @Override
    public int getColumnCount() throws SQLException {
        try {
            return resultSetMetaData.getColumnCount();
        } catch (java.sql.SQLException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public String getColumnName(int column) throws SQLException {
        try {
            return resultSetMetaData.getColumnLabel(column);
        } catch (java.sql.SQLException e) {
            throw new SQLException(e);
        }
    }
}
