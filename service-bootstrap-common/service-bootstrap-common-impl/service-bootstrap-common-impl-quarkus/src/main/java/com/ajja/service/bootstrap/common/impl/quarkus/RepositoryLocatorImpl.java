package com.ajja.service.bootstrap.common.impl.quarkus;

import com.ajja.service.bootstrap.common.business.repository.IRepository;
import com.ajja.service.bootstrap.common.business.repository.IRepositoryLocator;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class RepositoryLocatorImpl implements IRepositoryLocator {

    private static final Map<String, IRepository> CACHE = new HashMap<>();

    @Inject
    BeanManager beanManager;

    @Override
    public <T extends IRepository> T get(Class<T> aClass) {
        final String simpleName = aClass.getSimpleName();
        if (simpleName.charAt(0) != 'I') {
            throw new RuntimeException(String.format("%s must begin by letter 'I'.", simpleName));
        }

        IRepository repository = CACHE.get(simpleName);
        if (repository == null) {
            final String packageName = aClass.getPackageName();

            final Class<IRepository> repositoryProxyClass = getRepositoryProxyClass(packageName, simpleName);
            final Bean<IRepository> bean = (Bean<IRepository>) beanManager.getBeans(repositoryProxyClass).stream().findFirst().get();

            final CreationalContext<IRepository> creationalContext = beanManager.createCreationalContext(bean);
            repository = (IRepository) beanManager.getReference(bean, aClass, creationalContext);

            final Named namedAnnotation = repository.getClass().getAnnotation(Named.class);
            if (namedAnnotation == null || !namedAnnotation.value().equals(simpleName)) {
                throw new RuntimeException(String.format("%s must have a proxy with 'Named' annotation.", simpleName));
            }

            CACHE.put(simpleName, repository);
        }

        return (T) repository;
    }

    /**
     * getRepositoryProxyClass
     *
     * @param packageName String
     * @param simpleName String
     * @return Class<IRepository>
     */
    private Class<IRepository> getRepositoryProxyClass(final String packageName, final String simpleName) {
        final String proxyName = simpleName.substring(1) + "Proxy";
        final String className = (packageName.replace(".business.", ".impl.quarkus.") + "." + proxyName).replace(".repository." + proxyName, ".proxy.repository." + proxyName);
        try {
            return (Class<IRepository>) Thread.currentThread().getContextClassLoader().loadClass(className);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(String.format("Proxy '%s' not found for this interface '%s'.", className, simpleName));
        }
    }
}
