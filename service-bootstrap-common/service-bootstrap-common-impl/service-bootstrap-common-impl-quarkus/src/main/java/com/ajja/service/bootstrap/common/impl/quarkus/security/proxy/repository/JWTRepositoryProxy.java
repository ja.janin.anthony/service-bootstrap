package com.ajja.service.bootstrap.common.impl.quarkus.security.proxy.repository;

import com.ajja.service.bootstrap.common.application.security.repository.JWTRepositoryImpl;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.repository.IJWTRepository;
import io.quarkus.arc.Unremovable;
import io.quarkus.runtime.annotations.RegisterForReflection;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Date;
import java.util.UUID;

@Singleton
@Unremovable
@RegisterForReflection
@Named("IJWTRepository")
public class JWTRepositoryProxy implements IJWTRepository {

    private JWTRepositoryImpl repository = new JWTRepositoryImpl();

    @Inject
    public void setSecret(@ConfigProperty(name = "quarkus.security.jwt.secret", defaultValue = "") String secret) {
        repository.setSecret(secret);
    }

    @Inject
    public void setAccessTokenDuration(@ConfigProperty(name = "quarkus.security.jwt.access-token-duration", defaultValue = "0") long accessTokenDuration) {
        repository.setAccessTokenDuration(accessTokenDuration);
    }

    @Inject
    public void setRefreshTokenDuration(@ConfigProperty(name = "quarkus.security.jwt.refresh-token-duration", defaultValue = "0") long refreshTokenDuration) {
        repository.setRefreshTokenDuration(refreshTokenDuration);
    }


    @Override
    public String createAccessToken(String clientId, User user, String issuer, Date issuedAt, UUID userUUID) {
        return repository.createAccessToken(clientId, user, issuer, issuedAt, userUUID);
    }

    @Override
    public String createRefreshToken(String clientId, User user, String issuer, Date issuedAt) {
        return repository.createRefreshToken(clientId, user, issuer, issuedAt);
    }

    @Override
    public boolean isRefreshToken(String token) {
        return repository.isRefreshToken(token);
    }

    @Override
    public User getUser(String token) {
        return repository.getUser(token);
    }
}
