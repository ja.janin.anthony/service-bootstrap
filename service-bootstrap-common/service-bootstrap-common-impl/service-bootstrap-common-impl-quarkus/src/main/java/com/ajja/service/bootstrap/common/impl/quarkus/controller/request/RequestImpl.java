package com.ajja.service.bootstrap.common.impl.quarkus.controller.request;

import com.ajja.service.bootstrap.common.application.controller.builder.IURIBuilder;
import com.ajja.service.bootstrap.common.application.controller.builder.URIBuilderImpl;
import com.ajja.service.bootstrap.common.application.controller.request.IRequest;
import com.ajja.service.bootstrap.common.application.controller.security.IIdentity;
import com.ajja.service.bootstrap.common.application.controller.security.IdentityImpl;
import io.vertx.core.http.HttpServerRequest;

/**
 * RequestImpl
 */
public final class RequestImpl implements IRequest {

    private final HttpServerRequest request;

    /**
     * RequestImpl
     *
     * @param request HttpServerRequest
     */
    public RequestImpl(final HttpServerRequest request) {
        this.request = request;
    }

    @Override
    public IURIBuilder getCurrent() {
        return new URIBuilderImpl(new StringBuffer(request.absoluteURI()));
    }

    @Override
    public String getParameter(String name) {
        return request.getParam(name);
    }

    @Override
    public String getIssuer() {
        return request.getHeader("Origin");
    }

    @Override
    public IIdentity getIdentity() {
        final String authorization = request.getHeader("Authorization");
        if (authorization == null || authorization.isEmpty() || !authorization.startsWith("Bearer ")) {
            return null;
        }

        final String token = authorization.substring("Bearer ".length());
        if (token == null || token.isEmpty()) {
            return null;
        }

        return new IdentityImpl(token);
    }
}
