package com.ajja.service.bootstrap.common.impl.quarkus.security.proxy.service;

import com.ajja.service.bootstrap.common.business.security.entity.oauth2.OAuth2Client;
import com.ajja.service.bootstrap.common.business.security.service.IOAuth2WebAppService;
import com.ajja.service.bootstrap.common.business.security.service.OAuth2WebAppServiceImpl;
import io.quarkus.arc.Unremovable;
import io.quarkus.runtime.annotations.RegisterForReflection;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
@Unremovable
@RegisterForReflection
@Named("IOAuth2WebAppService")
public class OAuth2WebAppServiceProxy implements IOAuth2WebAppService {

    private OAuth2WebAppServiceImpl service = new OAuth2WebAppServiceImpl();

    @Inject
    public void setClientId(@ConfigProperty(name = "quarkus.security.oauth2.client.app.web.client-id", defaultValue = "") String clientId) {
        service.setClientId(clientId);
    }

    @Inject
    public void setRedirectUri(@ConfigProperty(name = "quarkus.security.oauth2.client.app.web.redirect-uri", defaultValue = "") String redirectUri) {
        service.setRedirectUri(redirectUri);
    }

    @Override
    public OAuth2Client getOAuth2Client() {
        return service.getOAuth2Client();
    }
}
