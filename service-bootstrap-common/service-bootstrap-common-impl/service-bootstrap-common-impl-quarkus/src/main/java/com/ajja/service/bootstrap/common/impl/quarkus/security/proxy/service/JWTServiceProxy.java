package com.ajja.service.bootstrap.common.impl.quarkus.security.proxy.service;

import com.ajja.service.bootstrap.common.business.repository.IRepositoryLocator;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.service.IJWTService;
import com.ajja.service.bootstrap.common.business.security.service.JWTServiceImpl;
import com.ajja.service.bootstrap.common.business.service.IServiceLocator;
import io.quarkus.arc.Unremovable;
import io.quarkus.runtime.annotations.RegisterForReflection;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Date;
import java.util.UUID;

@Singleton
@Unremovable
@RegisterForReflection
@Named("IJWTService")
public class JWTServiceProxy implements IJWTService {

    private JWTServiceImpl service = new JWTServiceImpl();

    @Inject
    public void setRepositoryLocator(IRepositoryLocator repositoryLocator) {
        service.setRepositoryLocator(repositoryLocator);
    }

    @Inject
    public void setServiceLocator(IServiceLocator serviceLocator) {
        service.setServiceLocator(serviceLocator);
    }

    @Override
    public String createAccessToken(String clientId, User user, String issuer, Date issuedAt, UUID userUUID) {
        return service.createAccessToken(clientId, user, issuer, issuedAt, userUUID);
    }

    @Override
    public String createRefreshToken(String clientId, User user, String issuer, Date issuedAt) {
        return service.createRefreshToken(clientId, user, issuer, issuedAt);
    }

    @Override
    public boolean isRefreshToken(String token) {
        return service.isRefreshToken(token);
    }

    @Override
    public User getUser(String token) {
        return service.getUser(token);
    }
}
