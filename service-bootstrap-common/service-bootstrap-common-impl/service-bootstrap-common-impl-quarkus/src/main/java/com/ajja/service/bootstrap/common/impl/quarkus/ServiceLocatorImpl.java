package com.ajja.service.bootstrap.common.impl.quarkus;

import com.ajja.service.bootstrap.common.business.service.IService;
import com.ajja.service.bootstrap.common.business.service.IServiceLocator;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class ServiceLocatorImpl implements IServiceLocator {

    private static final Map<String, IService> CACHE = new HashMap<>();

    @Inject
    BeanManager beanManager;

    @Override
    public <T extends IService> T get(Class<T> aClass) {
        final String simpleName = aClass.getSimpleName();
        if (simpleName.charAt(0) != 'I') {
            throw new RuntimeException(String.format("%s must begin by letter 'I'.", simpleName));
        }

        IService service = CACHE.get(simpleName);
        if (service == null) {
            final String packageName = aClass.getPackageName();

            final Class<IService> serviceProxyClass = getServiceProxyClass(packageName, simpleName);
            final Bean<IService> bean = (Bean<IService>) beanManager.getBeans(serviceProxyClass).stream().findFirst().get();

            final CreationalContext<IService> creationalContext = beanManager.createCreationalContext(bean);
            service = (IService) beanManager.getReference(bean, aClass, creationalContext);

            final Named namedAnnotation = service.getClass().getAnnotation(Named.class);
            if (namedAnnotation == null || !namedAnnotation.value().equals(simpleName)) {
                throw new RuntimeException(String.format("%s must have a proxy with 'Named' annotation.", simpleName));
            }

            CACHE.put(simpleName, service);
        }

        return (T) service;
    }

    /**
     * getServiceProxyClass
     *
     * @param packageName String
     * @param simpleName String
     * @return Class<IService>
     */
    private Class<IService> getServiceProxyClass(final String packageName, final String simpleName) {
        final String proxyName = simpleName.substring(1) + "Proxy";
        final String className = (packageName.replace(".business.", ".impl.quarkus.") + "." + proxyName).replace(".service." + proxyName, ".proxy.service." + proxyName);
        try {
            return (Class<IService>) Thread.currentThread().getContextClassLoader().loadClass(className);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(String.format("Proxy '%s' not found for this interface '%s'.", className, simpleName));
        }
    }
}
