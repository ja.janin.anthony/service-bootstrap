package com.ajja.service.bootstrap.common.impl.quarkus.security.proxy.controller;

import com.ajja.service.bootstrap.common.application.security.controller.IUserController;
import com.ajja.service.bootstrap.common.application.security.controller.UserControllerImpl;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.service.IServiceLocator;
import com.ajja.service.bootstrap.common.impl.quarkus.controller.request.RequestImpl;
import io.vertx.core.http.HttpServerRequest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

/**
 * AbstractUserControllerProxy
 */
public abstract class AbstractUserControllerProxy implements IUserController<HttpServerRequest, Response> {

	private UserControllerImpl controller = new UserControllerImpl();

	@Inject
	public void setServiceLocator(IServiceLocator serviceLocator) {
		controller.setServiceLocator(serviceLocator);
	}

	@GET
	@Path("/me")
	@Override
	public User me(@Context HttpServerRequest request) {
		return controller.me(new RequestImpl(request));
	}
}
