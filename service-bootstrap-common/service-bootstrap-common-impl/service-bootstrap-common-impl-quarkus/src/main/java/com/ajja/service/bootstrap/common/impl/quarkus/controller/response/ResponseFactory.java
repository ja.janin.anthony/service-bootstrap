package com.ajja.service.bootstrap.common.impl.quarkus.controller.response;

import com.ajja.service.bootstrap.common.application.controller.response.IResponse;

/**
 * ResponseFactory
 */
public final class ResponseFactory {

    /**
     * ResponseFactory
     */
    private ResponseFactory() {}

    /**
     * build
     *
     * @param response IResponse
     */
    public static javax.ws.rs.core.Response build(final IResponse response) {
        switch (response.getStatus()) {
            case 200:
                return javax.ws.rs.core.Response.ok().build();

            case 201:
                return javax.ws.rs.core.Response.created(response.getLocation()).build();

            case 204:
                return javax.ws.rs.core.Response.noContent().build();

            case 404:
                return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();

            default:
                return javax.ws.rs.core.Response.serverError().build();
        }
    }
}
