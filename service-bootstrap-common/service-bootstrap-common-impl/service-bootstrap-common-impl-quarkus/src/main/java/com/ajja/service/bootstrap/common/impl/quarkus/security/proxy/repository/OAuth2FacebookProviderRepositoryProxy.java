package com.ajja.service.bootstrap.common.impl.quarkus.security.proxy.repository;

import com.ajja.service.bootstrap.common.application.security.repository.OAuth2FacebookProviderRepositoryImpl;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.RedirectUrlResponse;
import com.ajja.service.bootstrap.common.business.security.repository.IOAuth2FacebookProviderRepository;
import io.quarkus.arc.Unremovable;
import io.quarkus.runtime.annotations.RegisterForReflection;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
@Unremovable
@RegisterForReflection
@Named("IOAuth2FacebookProviderRepository")
public class OAuth2FacebookProviderRepositoryProxy implements IOAuth2FacebookProviderRepository {

    private OAuth2FacebookProviderRepositoryImpl repository = new OAuth2FacebookProviderRepositoryImpl();

    @Inject
    public void setClientId(@ConfigProperty(name = "quarkus.security.oauth2.client.registration.facebook.client-id", defaultValue = "") String clientId) {
        repository.setClientId(clientId);
    }

    @Inject
    public void setClientSecret(@ConfigProperty(name = "quarkus.security.oauth2.client.registration.facebook.client-secret", defaultValue = "") String clientSecret) {
        repository.setClientSecret(clientSecret);
    }

    @Inject
    public void setRedirectUri(@ConfigProperty(name = "quarkus.security.oauth2.client.registration.facebook.redirect-uri", defaultValue = "") String redirectUri) {
        repository.setRedirectUri(redirectUri);
    }

    @Inject
    public void setAuthorizationUri(@ConfigProperty(name = "quarkus.security.oauth2.client.provider.facebook.authorization-uri", defaultValue = "") String authorizationUri) {
        repository.setAuthorizationUri(authorizationUri);
    }

    @Inject
    public void setTokenUri(@ConfigProperty(name = "quarkus.security.oauth2.client.provider.facebook.token-uri", defaultValue = "") String tokenUri) {
        repository.setTokenUri(tokenUri);
    }

    @Inject
    public void setUserInfoUri(@ConfigProperty(name = "quarkus.security.oauth2.client.provider.facebook.user-info-uri", defaultValue = "") String userInfoUri) {
        repository.setUserInfoUri(userInfoUri);
    }

    @Override
    public RedirectUrlResponse getRedirectUrl(String state) {
        return repository.getRedirectUrl(state);
    }

    @Override
    public User getUser(String code) {
        return repository.getUser(code);
    }
}
