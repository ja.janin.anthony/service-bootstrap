package com.ajja.service.bootstrap.common.impl.quarkus.security.proxy.service;

import com.ajja.service.bootstrap.common.business.repository.IRepositoryLocator;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.OAuth2Client;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.RedirectUrlResponse;
import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;
import com.ajja.service.bootstrap.common.business.security.service.IOAuth2Service;
import com.ajja.service.bootstrap.common.business.security.service.OAuth2ServiceImpl;
import com.ajja.service.bootstrap.common.business.service.IServiceLocator;
import io.quarkus.arc.Unremovable;
import io.quarkus.runtime.annotations.RegisterForReflection;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
@Unremovable
@RegisterForReflection
@Named("IOAuth2Service")
public class OAuth2ServiceProxy implements IOAuth2Service {

    private OAuth2ServiceImpl service = new OAuth2ServiceImpl();

    @Inject
    public void setRepositoryLocator(IRepositoryLocator repositoryLocator) {
        service.setRepositoryLocator(repositoryLocator);
    }

    @Inject
    public void setServiceLocator(IServiceLocator serviceLocator) {
        service.setServiceLocator(serviceLocator);
    }

    @Override
    public OAuth2Client getOAuth2Client(String clientId) {
        return service.getOAuth2Client(clientId);
    }

    @Override
    public RedirectUrlResponse getRedirectUrl(Provider provider, String state) {
        return service.getRedirectUrl(provider, state);
    }

    @Override
    public User getUser(Provider provider, String code) {
        return service.getUser(provider, code);
    }

    @Override
    public User getUser(Provider provider, String providerId, String name, String email) {
        return service.getUser(provider, providerId, name, email);
    }
}
