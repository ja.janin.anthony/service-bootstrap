package com.ajja.service.bootstrap.common.impl.quarkus.security.filter;

import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.type.Role;
import com.ajja.service.bootstrap.common.business.security.service.IJWTService;
import org.jboss.resteasy.core.ResourceMethodInvoker;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Response;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Set;

@ApplicationScoped
public class JWTSecurityFilter implements IFilter {

    @Inject
    IJWTService jwtService;

    @Override
    public void doFilter(ContainerRequestContext context) {
        final ResourceMethodInvoker resourceMethodInvoker = (ResourceMethodInvoker) context.getProperty("org.jboss.resteasy.core.ResourceMethodInvoker");
        final Method method = resourceMethodInvoker.getMethod();

        if (method.isAnnotationPresent(PermitAll.class)) {
            return;
        }

        if (method.isAnnotationPresent(DenyAll.class)) {
            context.abortWith(Response.status(Response.Status.FORBIDDEN).build());
            return;
        }

        if (method.isAnnotationPresent(RolesAllowed.class)) {
            final String authorization = context.getHeaderString("Authorization");
            if (authorization == null || authorization.isEmpty() || !authorization.startsWith("Bearer ")) {
                context.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
                return;
            }

            final String token = authorization.substring("Bearer ".length());
            if (authorization == null || authorization.isEmpty()) {
                context.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
                return;
            }

            final User user = jwtService.getUser(token);
            if (user == null) {
                context.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
                return;
            }

            final Set<Role> roles = user.getRoles();
            if (roles == null || roles.isEmpty()) {
                context.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
                return;
            }

            final boolean isAllowed = Arrays.stream(method.getAnnotation(RolesAllowed.class).value())
                .filter(roleAllowed -> {
                    try {
                        return roles.contains(Role.valueOf(roleAllowed));
                    } catch (Exception e) {}
                    return false;
                })
                .findFirst()
                .isPresent();

            if (!isAllowed) {
                context.abortWith(Response.status(Response.Status.FORBIDDEN).build());
            }
        }
    }
}
