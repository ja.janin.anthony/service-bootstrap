package com.ajja.service.bootstrap.common.impl.quarkus.sql;

import com.ajja.service.bootstrap.common.application.sql.IResultSet;
import com.ajja.service.bootstrap.common.application.sql.IResultSetMetaData;
import com.ajja.service.bootstrap.common.application.sql.exception.SQLException;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;

/**
 * ResultSetImpl
 */
public final class ResultSetImpl implements IResultSet {

    private final ResultSet resultSet;

    /**
     * ResultSetImpl
     *
     * @param resultSet
     */
    public ResultSetImpl(final ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    @Override
    public boolean next() throws SQLException {
        try {
            return resultSet.next();
        } catch (java.sql.SQLException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public IResultSetMetaData getMetaData() throws SQLException {
        try {
            return new ResultSetMetaDataImpl(resultSet.getMetaData());
        } catch (java.sql.SQLException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public String getString(String columnLabel) throws SQLException {
        try {
            return resultSet.getString(columnLabel);
        } catch (java.sql.SQLException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public int getInt(String columnLabel) throws SQLException {
        try {
            return resultSet.getInt(columnLabel);
        } catch (java.sql.SQLException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public long getLong(int columnIndex) throws SQLException {
        try {
            return resultSet.getLong(columnIndex);
        } catch (java.sql.SQLException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public long getLong(String columnLabel) throws SQLException {
        try {
            return resultSet.getLong(columnLabel);
        } catch (java.sql.SQLException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public float getFloat(String columnLabel) throws SQLException {
        try {
            return resultSet.getFloat(columnLabel);
        } catch (java.sql.SQLException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public double getDouble(String columnLabel) throws SQLException {
        try {
            return resultSet.getDouble(columnLabel);
        } catch (java.sql.SQLException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public boolean getBoolean(String columnLabel) throws SQLException {
        try {
            return resultSet.getBoolean(columnLabel);
        } catch (java.sql.SQLException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public Date getDate(String columnLabel) throws SQLException {
        try {
            final Timestamp timestamp = resultSet.getTimestamp(columnLabel);
            if (timestamp != null) {
                return new Date(timestamp.getTime());
            }
        } catch (java.sql.SQLException e) {
            throw new SQLException(e);
        }

        return null;
    }

    @Override
    public Object getObject(String columnLabel) throws SQLException {
        try {
            return resultSet.getObject(columnLabel);
        } catch (java.sql.SQLException e) {
            throw new SQLException(e);
        }
    }
}
