package com.ajja.service.bootstrap.common.business.security.service;

import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.repository.IJWTRepository;
import com.ajja.service.bootstrap.common.business.service.AbstractService;

import java.util.Date;
import java.util.UUID;

/**
 * JWTServiceImpl
 */
public class JWTServiceImpl extends AbstractService implements IJWTService {

    @Override
    public String createAccessToken(String clientId, User user, String issuer, Date issuedAt, UUID userUUID) {
        return getRepository(IJWTRepository.class).createAccessToken(clientId, user, issuer, issuedAt, userUUID);
    }

    @Override
    public String createRefreshToken(String clientId, User user, String issuer, Date issuedAt) {
        return getRepository(IJWTRepository.class).createRefreshToken(clientId, user, issuer, issuedAt);
    }

    @Override
    public boolean isRefreshToken(String token) {
        return getRepository(IJWTRepository.class).isRefreshToken(token);
    }

    @Override
    public User getUser(String token) {
        return getRepository(IJWTRepository.class).getUser(token);
    }
}
