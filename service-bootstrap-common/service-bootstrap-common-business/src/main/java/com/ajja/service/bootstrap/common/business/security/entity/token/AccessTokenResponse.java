package com.ajja.service.bootstrap.common.business.security.entity.token;

import java.util.UUID;

/**
 * AccessTokenResponse
 */
public class AccessTokenResponse {

    private UUID userUUID;

    private String accessToken;

    private String refreshToken;

    private Long accessTokenDuration;

    public UUID getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(UUID userUUID) {
        this.userUUID = userUUID;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Long getAccessTokenDuration() {
        return accessTokenDuration;
    }

    public void setAccessTokenDuration(Long accessTokenDuration) {
        this.accessTokenDuration = accessTokenDuration;
    }
}
