package com.ajja.service.bootstrap.common.business.security.service;

import com.ajja.service.bootstrap.common.business.annotation.Service;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.OAuth2Client;
import com.ajja.service.bootstrap.common.business.service.IService;

@Service
public interface IOAuth2WebAppService extends IService {

    /**
     * getOAuth2Client
     *
     * @return OAuth2Client
     */
    OAuth2Client getOAuth2Client();
}
