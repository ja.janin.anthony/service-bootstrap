package com.ajja.service.bootstrap.common.business.security.repository;

import com.ajja.service.bootstrap.common.business.annotation.Repository;
import com.ajja.service.bootstrap.common.business.repository.IRepository;
import com.ajja.service.bootstrap.common.business.security.entity.User;

import java.util.Date;
import java.util.UUID;

@Repository
public interface IJWTRepository extends IRepository {

    /**
     * createAccessToken
     *
     *
     * @param clientId String
     * @param user User
     * @param issuer String
     * @param issuedAt Date
     * @param userUUID UUID
     * @return String
     */
    String createAccessToken(String clientId, User user, String issuer, Date issuedAt, UUID userUUID);

    /**
     * createRefreshToken
     *
     *
     * @param clientId String
     * @param user User
     * @param issuer String
     * @param issuedAt Date
     * @return String
     */
    String createRefreshToken(String clientId, User user, String issuer, Date issuedAt);

    /**
     * isRefreshToken
     *
     * @param token String
     * @return boolean
     */
    boolean isRefreshToken(String token);

    /**
     * getUser
     *
     * @param token String
     * @return User
     */
    User getUser(String token);
}
