package com.ajja.service.bootstrap.common.business.security.entity.type;

/**
 * Provider
 */
public enum Provider {

    PROVIDER_LOCAL, PROVIDER_FACEBOOK, PROVIDER_GOOGLE, PROVIDER_APPLE
}
