package com.ajja.service.bootstrap.common.business.security.service;

import com.ajja.service.bootstrap.common.business.annotation.Service;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.OAuth2Client;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.RedirectUrlResponse;
import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;
import com.ajja.service.bootstrap.common.business.service.IService;

@Service
public interface IOAuth2Service extends IService {

    /**
     * getOAuth2Client
     *
     * @return OAuth2Client
     */
    OAuth2Client getOAuth2Client(String clientId);

    /**
     * getRedirectUrl
     *
     * @param provider Provider
     * @param state String
     * @return RedirectUrlResponse
     */
    RedirectUrlResponse getRedirectUrl(Provider provider, String state);

    /**
     * getUser
     *
     * @param provider Provider
     * @param code String
     * @return User
     */
    User getUser(Provider provider, String code);

    /**
     * getUser
     *
     * @param provider Provider
     * @param providerId String
     * @param name String
     * @param email String
     * @return User
     */
    User getUser(Provider provider, String providerId, String name, String email);
}
