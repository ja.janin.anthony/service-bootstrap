package com.ajja.service.bootstrap.common.business.security.service;

import com.ajja.service.bootstrap.common.business.annotation.Service;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;
import com.ajja.service.bootstrap.common.business.service.IService;

@Service
public interface IUserService extends IService {

    /**
     * create
     *
     * @param user User
     * @return User
     */
    User create(User user);

    /**
     * findById
     *
     * @param id
     * @return
     */
    User findById(Long id);

    /**
     * findByProvider
     *
     * @param provider Provider
     * @param providerId String
     * @return User
     */
    User findByProvider(Provider provider, String providerId);
}
