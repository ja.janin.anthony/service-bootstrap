package com.ajja.service.bootstrap.common.business.service;

import com.ajja.service.bootstrap.common.business.repository.IRepository;
import com.ajja.service.bootstrap.common.business.repository.IRepositoryLocator;

/**
 * AbstractService
 */
public abstract class AbstractService {

    private IRepositoryLocator repositoryLocator;

    private IServiceLocator serviceLocator;

    /**
     * setRepositoryLocator
     *
     * @param repositoryLocator IRepositoryLocator
     */
    public void setRepositoryLocator(final IRepositoryLocator repositoryLocator) {
        this.repositoryLocator = repositoryLocator;
    }

    /**
     * setServiceLocator
     *
     * @param serviceLocator IServiceLocator
     */
    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    /**
     * getRepository
     *
     * @param aClass aClass
     * @param <T extends IRepository>
     * @return T
     */
    protected <T extends IRepository> T getRepository(final Class<T> aClass) {
        return repositoryLocator.get(aClass);
    }

    /**
     * getService
     *
     * @param aClass Class<T>
     * @param <T extends IService
     * @return T
     */
    protected <T extends IService> T getService(final Class<T> aClass) {
        return serviceLocator.get(aClass);
    }
}
