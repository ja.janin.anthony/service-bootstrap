package com.ajja.service.bootstrap.common.business.security.service;

import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;
import com.ajja.service.bootstrap.common.business.security.repository.IUserRepository;
import com.ajja.service.bootstrap.common.business.service.AbstractService;

/**
 * UserServiceImpl
 */
public class UserServiceImpl extends AbstractService implements IUserService {

    @Override
    public User create(User user) {
        return getRepository(IUserRepository.class).create(user);
    }

    @Override
    public User findById(Long id) {
        return getRepository(IUserRepository.class).findById(id);
    }

    @Override
    public User findByProvider(Provider provider, String providerId) {
        return getRepository(IUserRepository.class).findByProvider(provider, providerId);
    }
}
