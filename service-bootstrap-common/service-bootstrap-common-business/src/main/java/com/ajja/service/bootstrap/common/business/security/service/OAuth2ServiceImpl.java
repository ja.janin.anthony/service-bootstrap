package com.ajja.service.bootstrap.common.business.security.service;

import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.OAuth2Client;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.RedirectUrlResponse;
import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;
import com.ajja.service.bootstrap.common.business.security.entity.type.Role;
import com.ajja.service.bootstrap.common.business.security.repository.IOAuth2AppleProviderRepository;
import com.ajja.service.bootstrap.common.business.security.repository.IOAuth2FacebookProviderRepository;
import com.ajja.service.bootstrap.common.business.security.repository.IOAuth2GoogleProviderRepository;
import com.ajja.service.bootstrap.common.business.service.AbstractService;
import com.ajja.service.bootstrap.common.business.util.PasswordUtils;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * OAuth2ServiceImpl
 */
public class OAuth2ServiceImpl extends AbstractService implements IOAuth2Service {

    private final Logger logger = Logger.getLogger(OAuth2ServiceImpl.class);

    private Map<String, OAuth2Client> oauth2Clients = new HashMap<>();

    @Override
    public OAuth2Client getOAuth2Client(String clientId) {
        if (oauth2Clients.isEmpty()) {
            // WebApp
            final OAuth2Client webAppOAuth2Client = getService(IOAuth2WebAppService.class).getOAuth2Client();
            oauth2Clients.put(webAppOAuth2Client.getClientId(), webAppOAuth2Client);

            // AndroidApp
            final OAuth2Client androidAppOAuth2Client = getService(IOAuth2AndroidAppService.class).getOAuth2Client();
            oauth2Clients.put(androidAppOAuth2Client.getClientId(), androidAppOAuth2Client);

            // IOSApp
            final OAuth2Client iosAppOAuth2Client = getService(IOAuth2IOSAppService.class).getOAuth2Client();
            oauth2Clients.put(iosAppOAuth2Client.getClientId(), iosAppOAuth2Client);
        }

        return oauth2Clients.get(clientId);
    }

    @Override
    public RedirectUrlResponse getRedirectUrl(Provider provider, String state) {
        switch (provider) {
            case PROVIDER_LOCAL:
                logger.info("Retrieve local redirect url.");
                // TODO
                return null;

            case PROVIDER_FACEBOOK:
                logger.info("Retrieve Facebook redirect url.");
                return getRepository(IOAuth2FacebookProviderRepository.class).getRedirectUrl(state);

            case PROVIDER_GOOGLE:
                logger.info("Retrieve Google redirect url.");
                return getRepository(IOAuth2GoogleProviderRepository.class).getRedirectUrl(state);

            case PROVIDER_APPLE:
                logger.info("Retrieve Apple redirect url.");
                return getRepository(IOAuth2AppleProviderRepository.class).getRedirectUrl(state);
        }

        return null;
    }

    @Override
    public User getUser(Provider provider, String code) {
        User user = null;

        switch (provider) {
            case PROVIDER_LOCAL:
                logger.info("Retrieve local user.");
                // TODO
                break;

            case PROVIDER_FACEBOOK:
                logger.info("Retrieve Facebook user.");
                user = getRepository(IOAuth2FacebookProviderRepository.class).getUser(code);
                break;

            case PROVIDER_GOOGLE:
                logger.info("Retrieve Google user.");
                user = getRepository(IOAuth2GoogleProviderRepository.class).getUser(code);
                break;

            case PROVIDER_APPLE:
                logger.info("Retrieve Apple user.");
                user = getRepository(IOAuth2AppleProviderRepository.class).getUser(code);
                break;
        }

        if (user == null) {
            logger.error("Can't retrieve user.");
            return null;
        }

        final IUserService userService = getService(IUserService.class);

        final User userExisting = userService.findByProvider(user.getProvider(), user.getProviderId());
        if (userExisting != null) {
            return userExisting;
        }

        return userService.create(user);
    }

    @Override
    public User getUser(Provider provider, String providerId, String name, String email) {
        final IUserService userService = getService(IUserService.class);

        final User userExisting = userService.findByProvider(provider, providerId);
        if (userExisting != null) {
            return userExisting;
        }

        final User user = new User();
        user.setName(name);
        user.setEmail(email);
        user.setEmailVerified(!email.isEmpty());
        user.setPassword(PasswordUtils.generate(8));
        user.setProvider(provider);
        user.setProviderId(providerId);
        user.setRoles(Set.of(Role.ROLE_USER));

        return userService.create(user);
    }
}
