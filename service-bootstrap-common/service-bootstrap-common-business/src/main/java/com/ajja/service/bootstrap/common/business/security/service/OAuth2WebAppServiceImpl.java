package com.ajja.service.bootstrap.common.business.security.service;

import com.ajja.service.bootstrap.common.business.security.entity.oauth2.OAuth2Client;
import com.ajja.service.bootstrap.common.business.service.AbstractService;

/**
 * OAuth2WebAppServiceImpl
 */
public class OAuth2WebAppServiceImpl extends AbstractService implements IOAuth2WebAppService {

    private String clientId;

    private String redirectUri;

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    @Override
    public OAuth2Client getOAuth2Client() {
        final OAuth2Client oauth2Client = new OAuth2Client();
        oauth2Client.setClientId(clientId);
        oauth2Client.setRedirectUri(redirectUri);
        return oauth2Client;
    }
}
