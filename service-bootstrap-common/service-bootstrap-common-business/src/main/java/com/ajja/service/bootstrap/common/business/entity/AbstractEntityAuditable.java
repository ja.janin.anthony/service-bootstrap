package com.ajja.service.bootstrap.common.business.entity;

import java.util.Date;

/**
 * AbstractEntityAuditable
 */
public abstract class AbstractEntityAuditable<ID> extends AbstractEntity<ID> {

    private Date creationDate;

    private Date updatingDate;

    private Long version;

    /**
     * getCreationDate
     *
     * @return Date
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * setCreationDate
     *
     * @param creationDate Date
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * getUpdatingDate
     *
     * @return Date
     */
    public Date getUpdatingDate() {
        return updatingDate;
    }

    /**
     * setUpdatingDate
     *
     * @param updatingDate Date
     */
    public void setUpdatingDate(Date updatingDate) {
        this.updatingDate = updatingDate;
    }

    /**
     * getVersion
     *
     * @return Long
     */
    public Long getVersion() {
        return version;
    }

    /**
     * setVersion
     *
     * @param version Long
     */
    public void setVersion(Long version) {
        this.version = version;
    }
}
