package com.ajja.service.bootstrap.common.business.security.entity.type;

/**
 * Role
 */
public enum Role {

    ROLE_USER, ROLE_ADMIN, ROLE_REFRESH_TOKEN
}
