package com.ajja.service.bootstrap.common.business.security.entity.oauth2;

/**
 * RedirectUrlResponse
 */
public class RedirectUrlResponse {

    private String redirectUrl;

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
