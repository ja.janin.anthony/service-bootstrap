package com.ajja.service.bootstrap.common.business.security.repository;

import com.ajja.service.bootstrap.common.business.annotation.Repository;
import com.ajja.service.bootstrap.common.business.annotation.Service;
import com.ajja.service.bootstrap.common.business.repository.IRepository;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.oauth2.RedirectUrlResponse;
import com.ajja.service.bootstrap.common.business.service.IService;

@Repository
public interface IOAuth2FacebookProviderRepository extends IRepository {

    /**
     * getRedirectUrl
     *
     * @param state String
     * @return RedirectUrlResponse
     */
    RedirectUrlResponse getRedirectUrl(String state);

    /**
     * getUser
     *
     * @param code String
     * @return User
     */
    User getUser(String code);
}
