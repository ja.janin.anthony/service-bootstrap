package com.ajja.service.bootstrap.common.business.security.entity.oauth2;

import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;

/**
 * UserLoginInfoRequest
 */
public class UserLoginInfoRequest {

    private String clientId;

    private Provider provider;

    private String providerId;

    private String name;

    private String email;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
