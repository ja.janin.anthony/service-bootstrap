package com.ajja.service.bootstrap.common.business.service;

/**
 * IServiceLocator
 */
public interface IServiceLocator {

    /**
     * get
     *
     * @param aClass Class<T>
     * @param <T extends IService>
     * @return T
     */
    <T extends IService> T get(Class<T> aClass);
}
