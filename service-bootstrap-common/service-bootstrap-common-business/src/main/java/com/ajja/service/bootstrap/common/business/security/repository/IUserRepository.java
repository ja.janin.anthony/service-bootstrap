package com.ajja.service.bootstrap.common.business.security.repository;

import com.ajja.service.bootstrap.common.business.annotation.Repository;
import com.ajja.service.bootstrap.common.business.repository.IRepository;
import com.ajja.service.bootstrap.common.business.security.entity.User;
import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;

@Repository
public interface IUserRepository extends IRepository {

    /**
     * create
     *
     * @param user User
     * @return User
     */
    User create(User user);

    /**
     *
     * @param id Long
     * @return User
     */
    User findById(Long id);

    /**
     * findByProvider
     *
     * @param provider Provider
     * @param providerId providerId
     * @return User
     */
    User findByProvider(Provider provider, String providerId);
}
