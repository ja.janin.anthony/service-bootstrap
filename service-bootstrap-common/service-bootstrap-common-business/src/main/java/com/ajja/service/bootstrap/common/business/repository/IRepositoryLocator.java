package com.ajja.service.bootstrap.common.business.repository;

/**
 * IRepositoryLocator
 */
public interface IRepositoryLocator {

    /**
     * get
     *
     * @param aClass Class<T>
     * @param <T extends IRepository>
     * @return T
     */
    <T extends IRepository> T get(Class<T> aClass);
}
