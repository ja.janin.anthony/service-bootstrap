package com.ajja.service.bootstrap.common.business.security.entity;


import com.ajja.service.bootstrap.common.business.annotation.Entity;
import com.ajja.service.bootstrap.common.business.entity.AbstractEntityAuditable;
import com.ajja.service.bootstrap.common.business.entity.IEntity;
import com.ajja.service.bootstrap.common.business.security.entity.type.Provider;
import com.ajja.service.bootstrap.common.business.security.entity.type.Role;

import java.util.Set;

@Entity
public class User extends AbstractEntityAuditable<Long> implements IEntity {

    private Provider provider;

    private String providerId;

    private String email;

    private Boolean emailVerified;

    private String name;

    private String password;

    private Set<Role> roles;

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
