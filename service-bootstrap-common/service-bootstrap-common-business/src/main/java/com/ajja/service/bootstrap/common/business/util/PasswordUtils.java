package com.ajja.service.bootstrap.common.business.util;

import java.util.Random;

/**
 * PasswordUtils
 */
public final class PasswordUtils {

    /**
     * PasswordUtils
     */
    private PasswordUtils() {}

    /**
     * generate
     *
     * @param length
     * @return String
     */
    public static String generate(final int length) {
        final StringBuilder sb = new StringBuilder(length);

        final String alphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            + "0123456789"
            + "abcdefghijklmnopqrstuvxyz";

        for (int i = 0; i < length; i++) {
            sb.append(alphaNumericString.charAt(new Random().nextInt(alphaNumericString.length())));
        }

        return sb.toString();
    }
}
