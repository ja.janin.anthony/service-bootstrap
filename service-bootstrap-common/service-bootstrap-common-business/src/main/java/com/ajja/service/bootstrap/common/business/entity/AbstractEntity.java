package com.ajja.service.bootstrap.common.business.entity;

/**
 * AbstractEntity
 */
public abstract class AbstractEntity<ID> {

    private ID id;

    /**
     * getId
     *
     * @return T
     */
    public ID getId() {
        return id;
    }

    /**
     * setId
     *
     * @param id T
     */
    public void setId(ID id) {
        this.id = id;
    }

    @Override
    public final int hashCode() {
        return 31 + ((id == null) ? 0 : id.hashCode());
    }

    @Override
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof AbstractEntity)) {
            return false;
        }

        final AbstractEntity entity = (AbstractEntity) obj;

        return id != null && id.equals(entity.id);
    }

    @Override
    public String toString() {
        return getClass().getName() + "[id=" + (id != null ? id.toString() : null) + "]";
    }
}
