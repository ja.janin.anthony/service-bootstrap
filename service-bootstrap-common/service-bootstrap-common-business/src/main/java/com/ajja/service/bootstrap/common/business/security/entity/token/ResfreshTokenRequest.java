package com.ajja.service.bootstrap.common.business.security.entity.token;

/**
 * ResfreshTokenRequest
 */
public class ResfreshTokenRequest {

    private String clientId;

    private String refreshToken;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
