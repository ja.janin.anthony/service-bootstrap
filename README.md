# Service bootstrap & Skeleton service (microservice)

## Goal

> Have to the same source code for 5 deliveries (by hexagonal architecture).

- [x] Quarkus (jar)
- [x] Spring-boot (jar)
- [x] Quarkus (exe)
- [x] Quarkus (image with exe)
- [x] Spring-boot (image with jar)

## Service bootstrap

```
mvn clean install
```

## Skeleton service

```
mvn clean install
```

- [x] Quarkus (jar)
- [x] Spring-boot (jar)

### Quarkus

###### Module 'skeleton-service-impl-quarkus'

`Run`

```
java -jar target/*-runner.jar
```

`Run / Debug`

```
mvn quarkus:dev 
```

Image native (GraalVm):

```
mvn package -Pnative
```

- [x] Quarkus (exe)

### Spring boot

###### Module 'skeleton-service-impl-spring-boot'

`Run / Debug`

```
SkeletonApplication.java
```

## Docker

- [x] Quarkus (image with exe)
- [x] Spring-boot (image with jar)

## Generator plugin

Generate `proxies` implementations for Quarkus and Spring-boot (`mvn compile`).
