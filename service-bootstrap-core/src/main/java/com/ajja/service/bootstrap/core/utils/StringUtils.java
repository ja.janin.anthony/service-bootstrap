package com.ajja.service.bootstrap.core.utils;

/**
 * StringUtils
 */
public class StringUtils {

    /**
     * StringUtils
     */
    private StringUtils() {}

    /**
     * toCamelCase
     *
     * @param str String
     * @return String
     */
    public static String toCamelCase(final String str) {
        final StringBuffer sb = new StringBuffer();

        for (final String s : str.split("_")) {
            sb.append(Character.toUpperCase(s.charAt(0)));
            if (s.length() > 1) {
                sb.append(s.substring(1).toLowerCase());
            }
        }

        return sb.toString();
    }

    /**
     * toSnakeCase
     *
     * @param str String
     * @return String
     */
    public static String toSnakeCase(final String str) {
        final StringBuffer sb = new StringBuffer();

        for (int i = 0; i < str.length(); i++) {
            final char c = str.charAt(i);
            if (i > 0 && Character.isUpperCase(c)) {
                sb.append('_').append(Character.toLowerCase(c));
            } else {
                sb.append(Character.toLowerCase(c));
            }
        }

        return sb.toString();
    }
}
