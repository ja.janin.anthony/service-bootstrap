package com.ajja.service.bootstrap.core.utils;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * ReflectionUtils
 */
public final class ReflectionUtils {

    /**
     * ReflectionUtils
     */
    private ReflectionUtils() {}

    /**
     * getColumns
     *
     * @param entityClass Class<?>
     * @return Collection<String>
     */
    public static Collection<String> getColumns(final Class<?> entityClass) {
        final Collection<String> columns = new ArrayList<>();

        final Method[] methods = entityClass.getMethods();
        for (final Method method : methods) {
            if (!Modifier.isPublic(method.getModifiers())) {
                continue;
            }

            final String methodName = method.getName();
            if (!methodName.startsWith("get")) {
                continue;
            }

            if ("getClass".equals(methodName)) {
                continue;
            }

            final String column = StringUtils.toSnakeCase(methodName.substring(3));
            columns.add(column);
        }

        return columns;
    }

    /**
     * getType
     *
     * @param method Method
     * @param entityClass Class<?>
     * @return Class<?>
     */
    public static Class<?> getType(final Method method, final Class<?> entityClass) {
        final Class<?> type = method.getReturnType();

        // If type is an <Object> then check if it's a generic type <T> and return the real type
        if (Object.class.equals(type)) {
            final Type genericReturnType = method.getGenericReturnType();
            if (genericReturnType instanceof TypeVariable) {
                final List<Class<?>> superClasses = getSuperClasses(entityClass);

                final GenericDeclaration genericDeclaration = ((TypeVariable) method.getGenericReturnType()).getGenericDeclaration();
                final boolean hasGenericDeclaration = superClasses.stream().filter(s -> genericDeclaration.equals(s)).findFirst().isPresent();

                if (hasGenericDeclaration) {
                    final Type genericSuperClass = entityClass.getGenericSuperclass();
                    if (genericSuperClass instanceof ParameterizedType) {
                        final Class<?> genericType = (Class<?>) ((ParameterizedType) genericSuperClass).getActualTypeArguments()[0];
                        return genericType;
                    }
                }
            }
        }

        return type;
    }

    /**
     * getSuperClasses
     *
     * @param entityClass Class<?>
     * @return List<Class<?>>
     */
    private static List<Class<?>> getSuperClasses(final Class<?> entityClass) {
        final List<Class<?>> superClasses = new ArrayList<>();

        Class<?> superClass = null;
        do {
            if (superClass != null) {
                superClasses.add(superClass);
            }
            superClass = ((superClass == null) ? entityClass : superClass).getSuperclass();
        } while (superClass != null);

        return superClasses;
    }
}
