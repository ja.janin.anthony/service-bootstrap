#!/bin/bash

set -e

. .build/build.conf

echo "=========================================================================================="
echo "Begin cleaning_job_images.sh"

docker rmi -f $(docker images ${IMAGE_NAME}-quarkus -aq) &> /dev/null || true
docker rmi -f $(docker images ${IMAGE_NAME}-spring-boot -aq) &> /dev/null || true

docker rmi -f $(docker images registry.access.redhat.com/ubi8/ubi-minimal -aq) || true
docker rmi -f $(docker images openjdk -aq) || true

docker rmi -f $(docker images | grep none | awk '{print $3}') &> /dev/null || true

docker rmi -f $(docker images quay.io/quarkus/centos-quarkus-maven -aq) || true
docker rmi -f $(docker images maven -aq) || true

docker ps -a
docker images
docker network ls
docker volume ls

echo "End cleaning_job_images.sh"
echo "=========================================================================================="
