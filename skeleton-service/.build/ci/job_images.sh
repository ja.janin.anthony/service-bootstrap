#!/bin/bash

set -e

sh .build/ci/scripts/build_images.sh
sh .build/ci/scripts/push_images.sh
