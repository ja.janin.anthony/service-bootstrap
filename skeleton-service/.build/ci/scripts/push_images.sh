#!/bin/bash

set -e

. .build/build.conf

echo "=========================================================================================="
echo "Begin push_image.sh"

VERSION=`cat pom.xml | grep "^    <version>.*</version>$" | awk -F'[><]' '{print $3}'`
echo "Version : ${VERSION}"

IS_SNAPSHOT="yes"
echo ${VERSION} | grep "SNAPSHOT" &> /dev/null || IS_SNAPSHOT="no"
echo "Is snapshot : ${IS_SNAPSHOT}"

#### QUARKUS ####
IS_NEW_VERSION="no"
docker search --no-trunc ${CI_REGISTRY_PULL}/${IMAGE_NAME}-quarkus:${VERSION} | grep "${VERSION}" &> /dev/null || IS_NEW_VERSION="yes"
echo "Is new version : ${IS_NEW_VERSION}"

if [[ "${IS_NEW_VERSION}" == "yes" ]]
then
  if [[ "${IS_SNAPSHOT}" == "yes" ]]
  then
    echo "Pushing to registry (only snapshot)"
    docker tag ${IMAGE_NAME}-quarkus:${VERSION} ${CI_REGISTRY_PUSH}/${IMAGE_NAME}-quarkus:${VERSION}
    docker push ${CI_REGISTRY_PUSH}/${IMAGE_NAME}-quarkus:${VERSION} &> /dev/null
  else
    echo "Pushing to registry"
    docker tag ${IMAGE_NAME}-quarkus:${VERSION} ${CI_REGISTRY_PUSH}/${IMAGE_NAME}-quarkus:${VERSION}
    docker tag ${IMAGE_NAME}-quarkus:${VERSION} ${CI_REGISTRY_PUSH}/${IMAGE_NAME}-quarkus:latest
    docker push ${CI_REGISTRY_PUSH}/${IMAGE_NAME}-quarkus:${VERSION} &> /dev/null
    docker push ${CI_REGISTRY_PUSH}/${IMAGE_NAME}-quarkus:latest &> /dev/null
  fi
elif [[ "${IS_SNAPSHOT}" == "yes" ]]
then
  echo "Pushing to registry (only snapshot)"
  docker tag ${IMAGE_NAME}-quarkus:${VERSION} ${CI_REGISTRY_PUSH}/${IMAGE_NAME}-quarkus:${VERSION}
  docker push ${CI_REGISTRY_PUSH}/${IMAGE_NAME}-quarkus:${VERSION} &> /dev/null
fi
#### QUARKUS ####

#### SPRING BOOT ####
IS_NEW_VERSION="no"
docker search --no-trunc ${CI_REGISTRY_PULL}/${IMAGE_NAME}-spring-boot:${VERSION} | grep "${VERSION}" &> /dev/null || IS_NEW_VERSION="yes"
echo "Is new version : ${IS_NEW_VERSION}"

if [[ "${IS_NEW_VERSION}" == "yes" ]]
then
  if [[ "${IS_SNAPSHOT}" == "yes" ]]
  then
    echo "Pushing to registry (only snapshot)"
    docker tag ${IMAGE_NAME}-spring-boot:${VERSION} ${CI_REGISTRY_PUSH}/${IMAGE_NAME}-spring-boot:${VERSION}
    docker push ${CI_REGISTRY_PUSH}/${IMAGE_NAME}-spring-boot:${VERSION} &> /dev/null
  else
    echo "Pushing to registry"
    docker tag ${IMAGE_NAME}-spring-boot:${VERSION} ${CI_REGISTRY_PUSH}/${IMAGE_NAME}-spring-boot:${VERSION}
    docker tag ${IMAGE_NAME}-spring-boot:${VERSION} ${CI_REGISTRY_PUSH}/${IMAGE_NAME}-spring-boot:latest
    docker push ${CI_REGISTRY_PUSH}/${IMAGE_NAME}-spring-boot:${VERSION} &> /dev/null
    docker push ${CI_REGISTRY_PUSH}/${IMAGE_NAME}-spring-boot:latest &> /dev/null
  fi
elif [[ "${IS_SNAPSHOT}" == "yes" ]]
then
  echo "Pushing to registry (only snapshot)"
  docker tag ${IMAGE_NAME}-spring-boot:${VERSION} ${CI_REGISTRY_PUSH}/${IMAGE_NAME}-spring-boot:${VERSION}
  docker push ${CI_REGISTRY_PUSH}/${IMAGE_NAME}-spring-boot:${VERSION} &> /dev/null
fi
#### SPRING BOOT ####

echo "End push_image.sh"
echo "=========================================================================================="
