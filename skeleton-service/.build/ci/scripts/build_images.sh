#!/bin/bash

set -e

. .build/build.conf

echo "=========================================================================================="
echo "Begin build_image.sh"

docker ps -a
docker images
docker network ls
docker volume ls

VERSION=`cat pom.xml | grep "^    <version>.*</version>$" | awk -F'[><]' '{print $3}'`
echo "Version : ${VERSION}"

sed -i "s|{{CI_REGISTRY_USER}}|${CI_REGISTRY_USER}|g" .build/maven/settings.xml
sed -i "s|{{CI_REGISTRY_PASSWORD}}|${CI_REGISTRY_PASSWORD}|g" .build/maven/settings.xml

docker build \
    -f .build/images/quarkus/Dockerfile \
    -t ${IMAGE_NAME}-quarkus:${VERSION} .

docker build \
    -f .build/images/spring-boot/Dockerfile \
    -t ${IMAGE_NAME}-spring-boot:${VERSION} .

docker ps -a
docker images
docker network ls
docker volume ls

echo "End build_image.sh"
echo "=========================================================================================="
