package com.ajja.skeleton.service.application.repository;

import com.ajja.service.bootstrap.common.application.sql.AbstractSQLRepository;
import com.ajja.skeleton.service.business.entity.Foo;
import com.ajja.skeleton.service.business.repository.IFooRepository;

import java.util.List;

/**
 * FooRepositoryImpl
 */
public final class FooRepositoryImpl extends AbstractSQLRepository implements IFooRepository {

    @Override
    public Foo create(Foo foo) {
        return getSQLQueryFactory()
            .insert(foo);
    }

    @Override
    public List<Foo> findAll() {
        return getSQLQueryFactory()
            .select(Foo.class);
    }

    @Override
    public Foo findById(Long id) {
        return getSQLQueryFactory()
            .where("id = ?")
            .setParam(id)
            .selectOne(Foo.class);
    }

    @Override
    public int update(Foo foo) {
        return getSQLQueryFactory()
            .where("id = ?")
            .setParam(foo.getId())
            .update(foo);
    }

    @Override
    public int deleteById(Long id) {
        return getSQLQueryFactory()
            .where("id = ?")
            .setParam(id)
            .delete(Foo.class);
    }
}
