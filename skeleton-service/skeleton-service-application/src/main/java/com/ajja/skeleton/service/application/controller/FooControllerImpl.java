package com.ajja.skeleton.service.application.controller;

import com.ajja.service.bootstrap.common.application.annotation.*;
import com.ajja.service.bootstrap.common.application.controller.AbstractController;
import com.ajja.service.bootstrap.common.application.controller.factory.ResponseFactory;
import com.ajja.service.bootstrap.common.application.controller.request.IRequest;
import com.ajja.service.bootstrap.common.application.controller.response.IResponse;
import com.ajja.skeleton.service.business.entity.Foo;
import com.ajja.skeleton.service.business.service.IFooService;

import java.net.URI;
import java.util.List;

@RequestMapping("/foo")
public final class FooControllerImpl extends AbstractController implements IFooController<IRequest, IResponse> {

    @PostMapping
    @Override
    public IResponse create(@RequestBody Foo foo, IRequest request) {
        final Long id = getService(IFooService.class).create(foo).getId();
        final URI location = request.getCurrent().append("/").append(id).build();

        return ResponseFactory.created(location);
    }

    @GetMapping
    @Override
    public List<Foo> findAll(IRequest request) {
        return getService(IFooService.class).findAll();
    }

    @GetMapping("/{id}")
    @Override
    public Foo findById(@PathVariable("id") Long id, IRequest request) {
        return getService(IFooService.class).findById(id);
    }

    @PutMapping("/{id}")
    @Override
    public IResponse update(@PathVariable("id") Long id, @RequestBody Foo foo, IRequest request) {
        final IFooService fooService = getService(IFooService.class);

        if (fooService.findById(id) == null) {
            return ResponseFactory.notFound();
        }

        foo.setId(id);
        fooService.update(foo);

        return ResponseFactory.ok();
    }

    @DeleteMapping("/{id}")
    @Override
    public IResponse deleteById(@PathVariable("id") Long id, IRequest request) {
        getService(IFooService.class).deleteById(id);

        return ResponseFactory.noContent();
    }
}
