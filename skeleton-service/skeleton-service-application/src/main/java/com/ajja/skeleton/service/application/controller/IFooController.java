package com.ajja.skeleton.service.application.controller;

import com.ajja.service.bootstrap.common.application.annotation.Controller;
import com.ajja.service.bootstrap.common.application.controller.IController;
import com.ajja.skeleton.service.business.entity.Foo;

import java.util.List;

@Controller
public interface IFooController<Request, Response> extends IController<Request, Response> {

    /**
     * create
     *
     * @param foo Foo
     * @param request Request
     * @return Response
     */
    Response create(Foo foo, Request request);

    /**
     * findAll
     *
     * @param request request
     * @return List<Foo>
     */
    List<Foo> findAll(Request request);

    /**
     * findById
     *
     * @param id Long
     * @param request request
     * @return Foo
     */
    Foo findById(Long id, Request request);

    /**
     * update
     *
     * @param id Long
     * @param foo Foo
     * @param request request
     * @return Response
     */
    Response update(Long id, Foo foo, Request request);

    /**
     * deleteById
     *
     * @param id Long
     * @param request request
     * @return Response
     */
    Response deleteById(Long id, Request request);
}
