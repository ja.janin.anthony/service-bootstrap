package com.ajja.skeleton.service.business.repository;

import com.ajja.service.bootstrap.common.business.annotation.Repository;
import com.ajja.service.bootstrap.common.business.repository.IRepository;
import com.ajja.skeleton.service.business.entity.Foo;

import java.util.List;

@Repository
public interface IFooRepository extends IRepository {

    /**
     * create
     *
     * @param foo Foo
     * @return Foo
     */
    Foo create(Foo foo);

    /**
     * findAll
     *
     * @return List<Foo>
     */
    List<Foo> findAll();

    /**
     * findById
     *
     * @param id Long
     * @return Foo
     */
    Foo findById(Long id);

    /**
     * update
     *
     * @param foo Foo
     * @return int
     */
    int update(Foo foo);

    /**
     * deleteById
     *
     * @param id Long
     * @return int
     */
    int deleteById(Long id);
}
