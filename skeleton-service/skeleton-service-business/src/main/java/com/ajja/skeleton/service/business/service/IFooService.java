package com.ajja.skeleton.service.business.service;

import com.ajja.service.bootstrap.common.business.annotation.Service;
import com.ajja.service.bootstrap.common.business.service.IService;
import com.ajja.skeleton.service.business.entity.Foo;

import java.util.List;

@Service
public interface IFooService extends IService {

    /**
     * create
     *
     * @param foo Foo
     * @return Foo
     */
    Foo create(Foo foo);

    /**
     * findAll
     *
     * @return List<Foo>
     */
    List<Foo> findAll();

    /**
     * findById
     *
     * @param id Long
     * @return Foo
     */
    Foo findById(Long id);

    /**
     * update
     *
     * @param foo Foo
     * @return boolean
     */
    boolean update(Foo foo);

    /**
     * deleteById
     *
     * @param id Long
     * @return boolean
     */
    boolean deleteById(Long id);
}
