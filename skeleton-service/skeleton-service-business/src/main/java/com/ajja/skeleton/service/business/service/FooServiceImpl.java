package com.ajja.skeleton.service.business.service;

import com.ajja.service.bootstrap.common.business.service.AbstractService;
import com.ajja.skeleton.service.business.entity.Foo;
import com.ajja.skeleton.service.business.repository.IFooRepository;

import java.util.List;

/**
 * FooServiceImpl
 */
public final class FooServiceImpl extends AbstractService implements IFooService {

    @Override
    public Foo create(Foo foo) {
        return getRepository(IFooRepository.class).create(foo);
    }

    @Override
    public List<Foo> findAll() {
        return getRepository(IFooRepository.class).findAll();
    }

    @Override
    public Foo findById(Long id) {
        return getRepository(IFooRepository.class).findById(id);
    }

    @Override
    public boolean update(Foo foo) {
        return getRepository(IFooRepository.class).update(foo) > 0;
    }

    @Override
    public boolean deleteById(Long id) {
        return getRepository(IFooRepository.class).deleteById(id) > 0;
    }
}
