package com.ajja.skeleton.service.business.entity;

import com.ajja.service.bootstrap.common.business.annotation.Entity;
import com.ajja.service.bootstrap.common.business.entity.AbstractEntityAuditable;
import com.ajja.service.bootstrap.common.business.entity.IEntity;

@Entity
public class Foo extends AbstractEntityAuditable<Long> implements IEntity {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
