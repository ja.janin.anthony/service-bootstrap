package com.ajja.skeleton.service.impl.spring.boot.proxy.service;

import com.ajja.service.bootstrap.common.business.repository.IRepositoryLocator;
import com.ajja.service.bootstrap.common.business.service.IServiceLocator;
import com.ajja.skeleton.service.business.entity.Foo;
import com.ajja.skeleton.service.business.service.FooServiceImpl;
import com.ajja.skeleton.service.business.service.IFooService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("IFooService")
public class FooServiceProxy implements IFooService {

    private FooServiceImpl service = new FooServiceImpl();

    @Autowired
    public void setRepositoryLocator(IRepositoryLocator repositoryLocator) {
        service.setRepositoryLocator(repositoryLocator);
    }

    @Autowired
    public void setServiceLocator(IServiceLocator serviceLocator) {
        service.setServiceLocator(serviceLocator);
    }

	@Override
	public Foo create(Foo foo) {
		return service.create(foo);
	}

	@Override
	public List<Foo> findAll() {
		return service.findAll();
	}

	@Override
	public Foo findById(Long id) {
		return service.findById(id);
	}

	@Override
	public boolean update(Foo foo) {
		return service.update(foo);
	}

	@Override
	public boolean deleteById(Long id) {
		return service.deleteById(id);
	}
}
