package com.ajja.skeleton.service.impl.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.ajja.service.bootstrap.common.impl.spring.boot", "com.ajja.skeleton.service.impl.spring.boot" })
public class SkeletonApplication {

    /**
     * main
     *
     * @param args String[]
     */
    public static void main(String[] args) {
        SpringApplication.run(SkeletonApplication.class, args);
    }
}
