package com.ajja.skeleton.service.impl.spring.boot.security.proxy.controller;

import com.ajja.service.bootstrap.common.impl.spring.boot.security.proxy.controller.AbstractAuthControllerProxy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/* Uncomment if necessary
@RestController
@RequestMapping("/auth")
*/
public class AuthControllerProxy extends AbstractAuthControllerProxy {

}
