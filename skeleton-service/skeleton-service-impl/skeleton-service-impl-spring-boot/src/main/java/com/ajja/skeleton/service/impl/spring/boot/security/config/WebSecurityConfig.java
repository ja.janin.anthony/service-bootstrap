package com.ajja.skeleton.service.impl.spring.boot.security.config;

import com.ajja.service.bootstrap.common.business.security.service.IJWTService;
import com.ajja.service.bootstrap.common.impl.spring.boot.security.config.WebSecurityDefaultConfig;
import com.ajja.service.bootstrap.common.impl.spring.boot.security.jwt.JWTAuthenticationEntryPoint;
import com.ajja.service.bootstrap.common.impl.spring.boot.security.jwt.JWTAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;

/* Uncomment if necessary
@Order(-1)
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
*/
public class WebSecurityConfig extends WebSecurityDefaultConfig {

    @Autowired
    private IJWTService jwtService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
                .cors()
            .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS).permitAll()
                .antMatchers("/auth/**").permitAll()
                .anyRequest().authenticated()
            .and()
                .csrf().disable()
                .formLogin().disable()
                .logout().disable()
                .httpBasic().disable()
                .headers().cacheControl().disable()
             .and()
                .addFilter(new JWTAuthorizationFilter(authenticationManager(), jwtService))
                .exceptionHandling().authenticationEntryPoint(new JWTAuthenticationEntryPoint());
    }
}
