package com.ajja.skeleton.service.impl.spring.boot.proxy.controller;

import com.ajja.service.bootstrap.common.business.service.IServiceLocator;
import com.ajja.service.bootstrap.common.impl.spring.boot.controller.request.RequestImpl;
import com.ajja.service.bootstrap.common.impl.spring.boot.controller.response.ResponseEntityFactory;
import com.ajja.skeleton.service.application.controller.FooControllerImpl;
import com.ajja.skeleton.service.application.controller.IFooController;
import com.ajja.skeleton.service.business.entity.Foo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/foo")
public class FooControllerProxy implements IFooController<HttpServletRequest, ResponseEntity> {

    private FooControllerImpl controller = new FooControllerImpl();

    @Autowired
    public void setServiceLocator(IServiceLocator serviceLocator) {
        controller.setServiceLocator(serviceLocator);
    }

	@PostMapping
	@Override
	public ResponseEntity create(@RequestBody Foo foo, HttpServletRequest request) {
		return ResponseEntityFactory.build(controller.create(foo, new RequestImpl(request)));
	}

	@GetMapping
	@Override
	public List<Foo> findAll(HttpServletRequest request) {
		return controller.findAll(new RequestImpl(request));
	}

	@GetMapping("/{id}")
	@Override
	public Foo findById(@PathVariable("id") Long id, HttpServletRequest request) {
		return controller.findById(id, new RequestImpl(request));
	}

	@PutMapping("/{id}")
	@Override
	public ResponseEntity update(@PathVariable("id") Long id, @RequestBody Foo foo, HttpServletRequest request) {
		return ResponseEntityFactory.build(controller.update(id, foo, new RequestImpl(request)));
	}

	@DeleteMapping("/{id}")
	@Override
	public ResponseEntity deleteById(@PathVariable("id") Long id, HttpServletRequest request) {
		return ResponseEntityFactory.build(controller.deleteById(id, new RequestImpl(request)));
	}
}
