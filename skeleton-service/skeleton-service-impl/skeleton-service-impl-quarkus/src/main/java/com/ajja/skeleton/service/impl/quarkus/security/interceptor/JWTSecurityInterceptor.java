package com.ajja.skeleton.service.impl.quarkus.security.interceptor;

import com.ajja.service.bootstrap.common.impl.quarkus.security.filter.IFilter;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

/* Uncomment if necessary
@Provider
@Priority(Priorities.AUTHORIZATION)
*/
public class JWTSecurityInterceptor implements ContainerRequestFilter {

    @Inject
    IFilter jwtSecurityFilter;

    @Override
    public void filter(ContainerRequestContext context) {
        jwtSecurityFilter.doFilter(context);
    }
}
