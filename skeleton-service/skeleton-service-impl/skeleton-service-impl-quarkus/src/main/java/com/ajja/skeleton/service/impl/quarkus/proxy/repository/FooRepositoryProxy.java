package com.ajja.skeleton.service.impl.quarkus.proxy.repository;

import com.ajja.service.bootstrap.common.application.sql.ISQLConnection;
import com.ajja.skeleton.service.application.repository.FooRepositoryImpl;
import com.ajja.skeleton.service.business.entity.Foo;
import com.ajja.skeleton.service.business.repository.IFooRepository;

import io.quarkus.arc.Unremovable;
import io.quarkus.runtime.annotations.RegisterForReflection;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
@Unremovable
@RegisterForReflection
@Named("IFooRepository")
public class FooRepositoryProxy implements IFooRepository {

    private FooRepositoryImpl repository = new FooRepositoryImpl();

    @Inject
    public void setSQLConnection(ISQLConnection sqlConnection) {
        repository.setSQLConnection(sqlConnection);
    }

	@Override
	public Foo create(Foo foo) {
		return repository.create(foo);
	}

	@Override
	public List<Foo> findAll() {
		return repository.findAll();
	}

	@Override
	public Foo findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public int update(Foo foo) {
		return repository.update(foo);
	}

	@Override
	public int deleteById(Long id) {
		return repository.deleteById(id);
	}
}
