package com.ajja.skeleton.service.impl.quarkus.proxy.controller;

import com.ajja.service.bootstrap.common.business.service.IServiceLocator;
import com.ajja.service.bootstrap.common.impl.quarkus.controller.request.RequestImpl;
import com.ajja.service.bootstrap.common.impl.quarkus.controller.response.ResponseFactory;
import com.ajja.skeleton.service.application.controller.FooControllerImpl;
import com.ajja.skeleton.service.application.controller.IFooController;
import com.ajja.skeleton.service.business.entity.Foo;

import io.vertx.core.http.HttpServerRequest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/foo")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FooControllerProxy implements IFooController<HttpServerRequest, Response> {

    private FooControllerImpl controller = new FooControllerImpl();

    @Inject
    public void setServiceLocator(IServiceLocator serviceLocator) {
        controller.setServiceLocator(serviceLocator);
    }

	@POST
	@Override
	public Response create(Foo foo, @Context HttpServerRequest request) {
		return ResponseFactory.build(controller.create(foo, new RequestImpl(request)));
	}

	@GET
	@Override
	public List<Foo> findAll(@Context HttpServerRequest request) {
		return controller.findAll(new RequestImpl(request));
	}

	@GET
	@Path("/{id}")
	@Override
	public Foo findById(@PathParam("id") Long id, @Context HttpServerRequest request) {
		return controller.findById(id, new RequestImpl(request));
	}

	@PUT
	@Path("/{id}")
	@Override
	public Response update(@PathParam("id") Long id, Foo foo, @Context HttpServerRequest request) {
		return ResponseFactory.build(controller.update(id, foo, new RequestImpl(request)));
	}

	@DELETE
	@Path("/{id}")
	@Override
	public Response deleteById(@PathParam("id") Long id, @Context HttpServerRequest request) {
		return ResponseFactory.build(controller.deleteById(id, new RequestImpl(request)));
	}
}
