package com.ajja.skeleton.service.impl.quarkus.security.proxy.controller;

import com.ajja.service.bootstrap.common.impl.quarkus.security.proxy.controller.AbstractUserControllerProxy;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/* Uncomment if necessary
@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
*/
public class UserControllerProxy extends AbstractUserControllerProxy {

}
